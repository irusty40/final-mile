﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalMile
{
    class Envelope
    {
        public string UserGUID { get; set; }

        public Order SaveOrder { get; set; }

        private string terminalId;

        public Envelope(string guid)
        {
            UserGUID = guid;

            SaveOrder = new Order();
        } 

        public void Add(FRP001 frp001)
        {
            terminalId = frp001.fhdt.Trim();

            SaveOrder.Add(frp001);

        }

        public void Add(List<AAP030> aap030)
        {
            foreach (AAP030 tid in aap030)
            {
                if (terminalId.Equals(tid.ct1tid.Trim()))
                {
                    SaveOrder.Add(tid);

                    break;
                }
            }

        }

        public void Add(List<FRP002> frp002)
        {
            SaveOrder.Add(frp002);
        }
        
        public void Add(List<FRP003> frp003)
        {
            SaveOrder.Add(frp003);
        }

        public void Add(FRP001EX frp001ex)
        {
            SaveOrder.Add(frp001ex);
        }
        
        public string ToXML()
        {
            StringBuilder xx = new StringBuilder();

            xx.Append(@"<SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'>");

            xx.AppendFormat(Environment.NewLine);

            xx.AppendFormat(@"<SOAP:Body UserGUID = '{0}'>", UserGUID);

            xx.AppendFormat(Environment.NewLine);

            xx.Append(SaveOrder.ToXML());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</SOAP:Body>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</SOAP:Envelope>");

            return xx.ToString();
        }

    }
}

﻿using System;
using System.Net.Http;

namespace FinalMile
{
    public static class Errors
    {
        public static void ReportException(Exception ex)
        {
            System.Text.StringBuilder msg = new System.Text.StringBuilder();

            try
            {
                msg.Append("<span style='font-family: Helvetica, Consolas, Tahoma, Arial; font-size: 10pt; color: #003399;'>");

                msg.Append("<h3>Final Mile Application</h3><br />");

                if (ex != null)
                {
                    msg.AppendFormat("ex.Message:<br />{0}<br />", ex.Message);

                    msg.AppendFormat("ex.StackTrace:<br />{0}<br />", ex.StackTrace);

                    if ((ex.HelpLink != null) && (!string.IsNullOrEmpty(ex.HelpLink.Trim())))
                    {
                        msg.AppendFormat("ex.HelpLink:<br />{0}<br />", ex.HelpLink);
                    }
                }

                msg.AppendFormat("<br />Server: {0}<br />", Environment.MachineName);

                msg.AppendFormat("Server Timestamp: {0}<br />", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff"));

                msg.Append("</span>");

                using (System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(Properties.Settings.Default.fromEmail, Properties.Settings.Default.programmerEmail))
                {
                    mail.Subject = Properties.Settings.Default.mailSubject;

                    mail.IsBodyHtml = true;

                    mail.Body = msg.ToString();

                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(Properties.Settings.Default.smtpServer))
                    {
                        smtp.Send(mail);
                    }
                }
            }
            catch { }            
        }

        public static void ReportException(HttpResponseMessage ex)
        {
            System.Text.StringBuilder msg = new System.Text.StringBuilder();

            try
            {
                msg.Append("<span style='font-family: Helvetica, Consolas, Tahoma, Arial; font-size: 10pt; color: #003399;'>");

                msg.Append("<h3>Final Mile Application</h3><br />");

                if (ex != null)
                {
                    msg.AppendFormat("StatusCode:<br />{0}<br />", ex.StatusCode);

                    msg.AppendFormat("ReasonPhrase:<br />{0}<br />", ex.ReasonPhrase);

                    msg.AppendFormat("RequestMessage:<br />{0}<br />", ex.RequestMessage);
                                        
                }
                else
                {
                    msg.Append("HttpResponseMessage was null. <br />");
                }

                msg.AppendFormat("<br />Server: {0}<br />", Environment.MachineName);

                msg.AppendFormat("Server Timestamp: {0}<br />", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff"));

                msg.Append("</span>");

                using (System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(Properties.Settings.Default.fromEmail, Properties.Settings.Default.programmerEmail))
                {
                    mail.Subject = Properties.Settings.Default.mailSubject;

                    mail.IsBodyHtml = true;

                    mail.Body = msg.ToString();

                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(Properties.Settings.Default.smtpServer))
                    {
                        smtp.Send(mail);
                    }
                }
            }
            catch { }
        }

        public static void ReportException(string json)
        {
            System.Text.StringBuilder msg = new System.Text.StringBuilder();

            try
            {
                msg.Append("<span style='font-family: Helvetica, Consolas, Tahoma, Arial; font-size: 10pt; color: #003399;'>");

                msg.Append("<h3>Final Mile Application</h3><br />");

                msg.Append(json);

                msg.Append("<br /><br />");

                msg.AppendFormat("<br />Server: {0}<br />", Environment.MachineName);

                msg.AppendFormat("Server Timestamp: {0}<br />", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff"));

                msg.Append("</span>");

                using (System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(Properties.Settings.Default.fromEmail, Properties.Settings.Default.programmerEmail))
                {
                    mail.Subject = Properties.Settings.Default.mailSubject;

                    mail.IsBodyHtml = true;

                    mail.Body = msg.ToString();

                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(Properties.Settings.Default.smtpServer))
                    {
                        smtp.Send(mail);
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="faultcode"></param>
        /// <param name="faultstring"></param>
        /// <param name="faultactor"></param>
        /// <param name="xmlPath1"></param>
        /// <param name="xmlPath2"></param>
        /// <remarks>
        /// 10/04/2018 : rw
        /// function added to report eCourier rejections of pros so we can have an email notification
        /// </remarks>
        public static void ReportException(string faultcode, string faultstring, string faultactor, string xmlPath1, string xmlPath2)
        {
            System.Text.StringBuilder msg = new System.Text.StringBuilder();

            try
            {
                msg.Append("<span style='font-family: Helvetica, Consolas, Tahoma, Arial; font-size: 10pt; color: #003399;'>");

                msg.Append("<h3>Final Mile - Pro Rejected</h3><br />");

                msg.AppendFormat("<br />faultcode: {0}<br />", faultcode);

                msg.AppendFormat("<br />faultstring: {0}<br />", faultstring);

                msg.AppendFormat("<br />faultactor: {0}<br />", faultactor);

                msg.Append("<br /><br />");

                msg.Append("Attached XML are the actual documents sent to eCourier along with their SOAP response.");

                msg.Append("<br /><br />");

                msg.AppendFormat("<br />Server: {0}<br />", Environment.MachineName);

                msg.AppendFormat("Server Timestamp: {0}<br />", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff"));

                msg.Append("</span>");

                using (System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage(Properties.Settings.Default.fromEmail, Properties.Settings.Default.programmerEmail))
                {
                    mail.Priority = System.Net.Mail.MailPriority.High;

                    mail.Subject = Properties.Settings.Default.mailSubjectProRejection;

                    mail.IsBodyHtml = true;

                    mail.Body = msg.ToString();

                    if (System.IO.File.Exists(xmlPath1)) mail.Attachments.Add(new System.Net.Mail.Attachment(xmlPath1));

                    if (System.IO.File.Exists(xmlPath2)) mail.Attachments.Add(new System.Net.Mail.Attachment(xmlPath2));
                    
                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(Properties.Settings.Default.smtpServer))
                    {
                        smtp.Send(mail);
                    }
                }
            }
            catch { }
        }



    }

    public static class Logging
    {   
        public static string RUN_MODE = "";
              

        public static void LogEntry(string file, string key, string value)
        {
            using (System.IO.StreamWriter wtr = new System.IO.StreamWriter(file, true))
            {
                wtr.WriteLine(string.Format("{0} [{1}] {2}", DateTime.Today.ToString("MM/dd/yyyy HH:mm:ss.fff"), key.PadRight(12, ' '), value));
            }
        }

        public static void LogError(string file, string json)
        {
            using (System.IO.StreamWriter wtr = new System.IO.StreamWriter(file, true))
            {
                wtr.WriteLine(DateTime.Today.ToString("MM/dd/yyyy HH:mm:ss.fff"));

                wtr.WriteLine("JSON: " + Environment.NewLine + json);

                wtr.WriteLine("");
            }
        }

        public static void LogError(string file, Exception ex)
        {
            using (System.IO.StreamWriter wtr = new System.IO.StreamWriter(file, true))
            {
                wtr.WriteLine(DateTime.Today.ToString("MM/dd/yyyy HH:mm:ss.fff"));

                wtr.WriteLine("Message: " + Environment.NewLine + ex.Message);

                wtr.WriteLine("StackTrace: " + Environment.NewLine + ex.StackTrace);

                if ((ex.HelpLink != null) && (!string.IsNullOrEmpty(ex.HelpLink.Trim())))
                {
                    wtr.WriteLine("HelpLink: " + Environment.NewLine + ex.HelpLink);
                }

                wtr.WriteLine("");

            }
        }
    }
}
﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;


namespace FinalMile
{
    /// <summary>
    /// 
    /// </summary>
    public class FRP001
    {
        /// <summary>
        /// base select
        /// </summary>
        public const string SQL_SELECT = "select fhpro, fhchkd, fhsn, fhccn, fhst, fhshst, fhcs, fhstat, fhbilh, fhbilv, fhcd, fhco, fhdiv, fhozon, fhopn, fhot, fhdzon, fhdpn, fhdt, fhslsm, fhtc, fhtcin, fhrbn, fhrbns, fhtotp, fhhaz, fhdclv, fhchg, fhprod, fhfbcc, fhincc, fhfexc, fhcxfs, fhrtyp, fhtrm, fhbilt, fhpucn, fhpadt, fhpatm, fhpud, fhpudr, fhpudt, fhputm, fhput, fhpur, fhbl, fhsnum, fhpo, fhdept, fhref, fhscc, fhsq, fhcube, fhpdg, fhpdgs, fhtr, fhloap, fhocd, fhscd, fhsnm, fhsa1, fhsa2, fhsct, fhsst, fhszip, fhszp4, fhccd, fhcnm, fhca1, fhca2, fhcct, fhcst, fhczip, fhczp4, fhbtc, fhbnm, fhba1, fhba2, fhbct, fhbst, fhbzip, fhbzp4, fhft, fhxf, fhxfa, fhxfb, fhxfbd, fhtt, fhxt, fhxta, fhxtb, fhxtbd, fhfpc, fhopc, fhtpc, fhf$, fho$, fht$, fhpf$, fhpo$, fhpt$, fhfp, fhtp, fhcod, fhccc, fhcexc, fhcxcs, fhotrv, fhlhrv, fhbtrv, fhdtrv, fhsprv, fhsprc, fhallf, fhalld, fhallp, fhips, fhops, fhcn, fhrzon, fhrpn, fhrt, fhrtd, fhrtt, fhrtl, fhrmnf, fhdadt, fhdatm, fhdacd, fhapnm, fhapdt, fhaptm, fhradf, fhradt, fhmadt, fhmatm, fhdc, fhddat, fhdtim, fhbbc, fhnbb, fhdday, fhbbcl, fhdld, fhdldr, fhdlt, fhdlr, fhdrc, fhfidd, fhfidt, fhfids, fhuwgt, fhswgt, fhrwgt, fhtwgt, fhadat, fhatim, fhseq, fhpdat, fhudat, fhutim, fhuid, fhspro, fhssuf, fhclrk, fhentl, fhrate, fhaud, fhhdss, fhhdns, fhhdsc, fhhdnc, fhhdps, fhhdos, fhhdpc, fhhdoc, fhrbsa, fhrbsc, fhrbpa, fhrbpc, fhrevd, fhrevc, fhpurt, fhpurp, fhotrt, fhlhrt, fhbtrt, fhbtrp, fhdtrt, fhdtrp, fhdlrt, fhdlrp, fhsprt, fhsprp, fhpwgt, fhsuas, fhsuac, fhsua3, fhsuaf, fhsutf, fhsuat, fhsutt, fhsuao, fhpstb, fhpstc, fhpsts, fhgstb, fhgstc, fhgsts, fhvatb, fhvatc, fhvats, fhbils, fhbco, fhbdiv, fhbcur, fhbinc, fhbrvc, fhbtyp, fhbwgt, fhbdsc, fhbcod, fhbsc, fhbsa, fhbsd, fhbss, fhbsft, fhbcc, fhbca, fhbcd, fhbcs, fhbcft, fhb3c, fhb3a, fhb3d, fhb3s, fhbbft, fhbxfc, fhbxfa, fhbxfd, fhbxft, fhbxfs, fhbfft, fhbxtc, fhbxta, fhbxtd, fhbxtt, fhbxts, fhbtft, fhboc, fhboa, fhbod, fhboft, fhpstr, fhgstr, fhvatr, fhcxft, fhcxct, fhxs1, fhxs2, fhxs3, fhxs4, fhxa1, fhxa2, fhxa3, fhxa4 from FRP001 ";

        /// <summary>
        /// base insert
        /// </summary>
        //private const string SQL_INSERT = "insert into FRP001 (fhpro, fhchkd, fhsn, fhccn, fhst, fhshst, fhcs, fhstat, fhbilh, fhbilv, fhcd, fhco, fhdiv, fhozon, fhopn, fhot, fhdzon, fhdpn, fhdt, fhslsm, fhtc, fhtcin, fhrbn, fhrbns, fhtotp, fhhaz, fhdclv, fhchg, fhprod, fhfbcc, fhincc, fhfexc, fhcxfs, fhrtyp, fhtrm, fhbilt, fhpucn, fhpadt, fhpatm, fhpud, fhpudr, fhpudt, fhputm, fhput, fhpur, fhbl, fhsnum, fhpo, fhdept, fhref, fhscc, fhsq, fhcube, fhpdg, fhpdgs, fhtr, fhloap, fhocd, fhscd, fhsnm, fhsa1, fhsa2, fhsct, fhsst, fhszip, fhszp4, fhccd, fhcnm, fhca1, fhca2, fhcct, fhcst, fhczip, fhczp4, fhbtc, fhbnm, fhba1, fhba2, fhbct, fhbst, fhbzip, fhbzp4, fhft, fhxf, fhxfa, fhxfb, fhxfbd, fhtt, fhxt, fhxta, fhxtb, fhxtbd, fhfpc, fhopc, fhtpc, fhf$, fho$, fht$, fhpf$, fhpo$, fhpt$, fhfp, fhtp, fhcod, fhccc, fhcexc, fhcxcs, fhotrv, fhlhrv, fhbtrv, fhdtrv, fhsprv, fhsprc, fhallf, fhalld, fhallp, fhips, fhops, fhcn, fhrzon, fhrpn, fhrt, fhrtd, fhrtt, fhrtl, fhrmnf, fhdadt, fhdatm, fhdacd, fhapnm, fhapdt, fhaptm, fhradf, fhradt, fhmadt, fhmatm, fhdc, fhddat, fhdtim, fhbbc, fhnbb, fhdday, fhbbcl, fhdld, fhdldr, fhdlt, fhdlr, fhdrc, fhfidd, fhfidt, fhfids, fhuwgt, fhswgt, fhrwgt, fhtwgt, fhadat, fhatim, fhseq, fhpdat, fhudat, fhutim, fhuid, fhspro, fhssuf, fhclrk, fhentl, fhrate, fhaud, fhhdss, fhhdns, fhhdsc, fhhdnc, fhhdps, fhhdos, fhhdpc, fhhdoc, fhrbsa, fhrbsc, fhrbpa, fhrbpc, fhrevd, fhrevc, fhpurt, fhpurp, fhotrt, fhlhrt, fhbtrt, fhbtrp, fhdtrt, fhdtrp, fhdlrt, fhdlrp, fhsprt, fhsprp, fhpwgt, fhsuas, fhsuac, fhsua3, fhsuaf, fhsutf, fhsuat, fhsutt, fhsuao, fhpstb, fhpstc, fhpsts, fhgstb, fhgstc, fhgsts, fhvatb, fhvatc, fhvats, fhbils, fhbco, fhbdiv, fhbcur, fhbinc, fhbrvc, fhbtyp, fhbwgt, fhbdsc, fhbcod, fhbsc, fhbsa, fhbsd, fhbss, fhbsft, fhbcc, fhbca, fhbcd, fhbcs, fhbcft, fhb3c, fhb3a, fhb3d, fhb3s, fhbbft, fhbxfc, fhbxfa, fhbxfd, fhbxft, fhbxfs, fhbfft, fhbxtc, fhbxta, fhbxtd, fhbxtt, fhbxts, fhbtft, fhboc, fhboa, fhbod, fhboft, fhpstr, fhgstr, fhvatr, fhcxft, fhcxct, fhxs1, fhxs2, fhxs3, fhxs4, fhxa1, fhxa2, fhxa3, fhxa4) values ({0}, {1}, {2}, '{3}', {4}, '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', {11}, {12}, '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', {19}, '{20}', '{21}', {22}, '{23}', {24}, '{25}', {26}, {27}, {28}, '{29}', '{30}', {31}, {32}, '{33}', '{34}', '{35}', {36}, {37}, {38}, {39}, {40}, {41}, {42}, '{43}', {44}, '{45}', '{46}', '{47}', '{48}', '{49}', '{50}', {51}, {52}, '{53}', {54}, '{55}', {56}, '{57}', '{58}', '{59}', '{60}', '{61}', '{62}', '{63}', '{64}', '{65}', '{66}', '{67}', '{68}', '{69}', '{70}', '{71}', '{72}', '{73}', '{74}', '{75}', '{76}', '{77}', '{78}', '{79}', '{80}', '{81}', '{82}', '{83}', '{84}', '{85}', {86}, '{87}', '{88}', '{89}', '{90}', {91}, {92}, {93}, {94}, {95}, {96}, {97}, {98}, {99}, {100}, '{101}', '{102}', {103}, '{104}', {105}, {106}, {107}, {108}, {109}, {110}, {111}, '{112}', '{113}', {114}, {115}, '{116}', '{117}', {118}, '{119}', '{120}', '{121}', {122}, {123}, '{124}', {125}, {126}, {127}, '{128}', '{129}', {130}, {131}, {132}, {133}, {134}, {135}, '{136}', {137}, {138}, '{139}', {140}, {141}, '{142}', {143}, {144}, '{145}', {146}, {147}, {148}, {149}, '{150}', '{151}', {152}, {153}, {154}, {155}, {156}, {157}, {158}, {159}, {160}, '{161}', {162}, '{163}', '{164}', '{165}', '{166}', '{167}', {168}, '{169}', {170}, '{171}', {172}, '{173}', {174}, '{175}', {176}, '{177}', {178}, '{179}', {180}, '{181}', {182}, {183}, {184}, {185}, {186}, '{187}', {188}, '{189}', {190}, {191}, {192}, '{193}', {194}, {195}, {196}, {197}, {198}, '{199}', {200}, '{201}', {202}, {203}, '{204}', {205}, {206}, '{207}', {208}, {209}, '{210}', {211}, '{212}', {213}, {214}, '{215}', '{216}', '{217}', '{218}', {219}, {220}, {221}, '{222}', {223}, {224}, '{225}', '{226}', '{227}', {228}, {229}, '{230}', '{231}', '{232}', {233}, {234}, '{235}', '{236}', '{237}', {238}, {239}, '{240}', '{241}', '{242}', '{243}', {244}, {245}, '{246}', '{247}', '{248}', '{249}', {250}, {251}, '{252}', {253}, {254}, {255}, {256}, {257}, '{258}', '{259}', '{260}', '{261}', {262}, {263}, {264}, {265}) ";

        /// <summary>
        /// base update
        /// </summary>
        //private const string SQL_UPDATE = "update FRP001 set fhpro = {0}, fhchkd = {1}, fhsn = {2}, fhccn = '{3}', fhst = {4}, fhshst = '{5}', fhcs = '{6}', fhstat = '{7}', fhbilh = '{8}', fhbilv = '{9}', fhcd = '{10}', fhco = {11}, fhdiv = {12}, fhozon = '{13}', fhopn = '{14}', fhot = '{15}', fhdzon = '{16}', fhdpn = '{17}', fhdt = '{18}', fhslsm = {19}, fhtc = '{20}', fhtcin = '{21}', fhrbn = {22}, fhrbns = '{23}', fhtotp = {24}, fhhaz = '{25}', fhdclv = {26}, fhchg = {27}, fhprod = {28}, fhfbcc = '{29}', fhincc = '{30}', fhfexc = {31}, fhcxfs = {32}, fhrtyp = '{33}', fhtrm = '{34}', fhbilt = '{35}', fhpucn = {36}, fhpadt = {37}, fhpatm = {38}, fhpud = {39}, fhpudr = {40}, fhpudt = {41}, fhputm = {42}, fhput = '{43}', fhpur = {44}, fhbl = '{45}', fhsnum = '{46}', fhpo = '{47}', fhdept = '{48}', fhref = '{49}', fhscc = '{50}', fhsq = {51}, fhcube = {52}, fhpdg = '{53}', fhpdgs = {54}, fhtr = '{55}', fhloap = {56}, fhocd = '{57}', fhscd = '{58}', fhsnm = '{59}', fhsa1 = '{60}', fhsa2 = '{61}', fhsct = '{62}', fhsst = '{63}', fhszip = '{64}', fhszp4 = '{65}', fhccd = '{66}', fhcnm = '{67}', fhca1 = '{68}', fhca2 = '{69}', fhcct = '{70}', fhcst = '{71}', fhczip = '{72}', fhczp4 = '{73}', fhbtc = '{74}', fhbnm = '{75}', fhba1 = '{76}', fhba2 = '{77}', fhbct = '{78}', fhbst = '{79}', fhbzip = '{80}', fhbzp4 = '{81}', fhft = '{82}', fhxf = '{83}', fhxfa = '{84}', fhxfb = '{85}', fhxfbd = {86}, fhtt = '{87}', fhxt = '{88}', fhxta = '{89}', fhxtb = '{90}', fhxtbd = {91}, fhfpc = {92}, fhopc = {93}, fhtpc = {94}, fhf$ = {95}, fho$ = {96}, fht$ = {97}, fhpf$ = {98}, fhpo$ = {99}, fhpt$ = {100}, fhfp = '{101}', fhtp = '{102}', fhcod = {103}, fhccc = '{104}', fhcexc = {105}, fhcxcs = {106}, fhotrv = {107}, fhlhrv = {108}, fhbtrv = {109}, fhdtrv = {110}, fhsprv = {111}, fhsprc = '{112}', fhallf = '{113}', fhalld = {114}, fhallp = {115}, fhips = '{116}', fhops = '{117}', fhcn = {118}, fhrzon = '{119}', fhrpn = '{120}', fhrt = '{121}', fhrtd = {122}, fhrtt = {123}, fhrtl = '{124}', fhrmnf = {125}, fhdadt = {126}, fhdatm = {127}, fhdacd = '{128}', fhapnm = '{129}', fhapdt = {130}, fhaptm = {131}, fhradf = {132}, fhradt = {133}, fhmadt = {134}, fhmatm = {135}, fhdc = '{136}', fhddat = {137}, fhdtim = {138}, fhbbc = '{139}', fhnbb = {140}, fhdday = {141}, fhbbcl = '{142}', fhdld = {143}, fhdldr = {144}, fhdlt = '{145}', fhdlr = {146}, fhdrc = {147}, fhfidd = {148}, fhfidt = {149}, fhfids = '{150}', fhuwgt = '{151}', fhswgt = {152}, fhrwgt = {153}, fhtwgt = {154}, fhadat = {155}, fhatim = {156}, fhseq = {157}, fhpdat = {158}, fhudat = {159}, fhutim = {160}, fhuid = '{161}', fhspro = {162}, fhssuf = '{163}', fhclrk = '{164}', fhentl = '{165}', fhrate = '{166}', fhaud = '{167}', fhhdss = {168}, fhhdns = '{169}', fhhdsc = {170}, fhhdnc = '{171}', fhhdps = {172}, fhhdos = '{173}', fhhdpc = {174}, fhhdoc = '{175}', fhrbsa = {176}, fhrbsc = '{177}', fhrbpa = {178}, fhrbpc = '{179}', fhrevd = {180}, fhrevc = '{181}', fhpurt = {182}, fhpurp = {183}, fhotrt = {184}, fhlhrt = {185}, fhbtrt = {186}, fhbtrp = '{187}', fhdtrt = {188}, fhdtrp = '{189}', fhdlrt = {190}, fhdlrp = {191}, fhsprt = {192}, fhsprp = '{193}', fhpwgt = {194}, fhsuas = {195}, fhsuac = {196}, fhsua3 = {197}, fhsuaf = {198}, fhsutf = '{199}', fhsuat = {200}, fhsutt = '{201}', fhsuao = {202}, fhpstb = {203}, fhpstc = '{204}', fhpsts = {205}, fhgstb = {206}, fhgstc = '{207}', fhgsts = {208}, fhvatb = {209}, fhvatc = '{210}', fhvats = {211}, fhbils = '{212}', fhbco = {213}, fhbdiv = {214}, fhbcur = '{215}', fhbinc = '{216}', fhbrvc = '{217}', fhbtyp = '{218}', fhbwgt = {219}, fhbdsc = {220}, fhbcod = {221}, fhbsc = '{222}', fhbsa = {223}, fhbsd = {224}, fhbss = '{225}', fhbsft = '{226}', fhbcc = '{227}', fhbca = {228}, fhbcd = {229}, fhbcs = '{230}', fhbcft = '{231}', fhb3c = '{232}', fhb3a = {233}, fhb3d = {234}, fhb3s = '{235}', fhbbft = '{236}', fhbxfc = '{237}', fhbxfa = {238}, fhbxfd = {239}, fhbxft = '{240}', fhbxfs = '{241}', fhbfft = '{242}', fhbxtc = '{243}', fhbxta = {244}, fhbxtd = {245}, fhbxtt = '{246}', fhbxts = '{247}', fhbtft = '{248}', fhboc = '{249}', fhboa = {250}, fhbod = {251}, fhboft = '{252}', fhpstr = {253}, fhgstr = {254}, fhvatr = {255}, fhcxft = {256}, fhcxct = {257}, fhxs1 = '{258}', fhxs2 = '{259}', fhxs3 = '{260}', fhxs4 = '{261}', fhxa1 = {262}, fhxa2 = {263}, fhxa3 = {264}, fhxa4 = {265} where *** ";

        /// <summary>
        /// base delete
        /// </summary>
        //private const string SQL_DELETE = "delete from FRP001 where *** ";

        /// <summary>
        /// default contructor
        /// </summary>
        public FRP001() { }

        /// <summary>
        /// preferred contructor
        /// </summary>
        /// <param name="row"></param>
        public FRP001(DataRow row)
        {

            fhpro = Convert.ToDecimal(row["fhpro"]);

            fhchkd = Convert.ToDecimal(row["fhchkd"]);

            fhsn = Convert.ToDecimal(row["fhsn"]);

            fhccn = row["fhccn"].ToString().Trim();

            fhst = Convert.ToDecimal(row["fhst"]);

            fhshst = row["fhshst"].ToString().Trim();

            fhcs = row["fhcs"].ToString().Trim();

            fhstat = row["fhstat"].ToString().Trim();

            fhbilh = row["fhbilh"].ToString().Trim();

            fhbilv = row["fhbilv"].ToString().Trim();

            fhcd = row["fhcd"].ToString().Trim();

            fhco = Convert.ToDecimal(row["fhco"]);

            fhdiv = Convert.ToDecimal(row["fhdiv"]);

            fhozon = row["fhozon"].ToString().Trim();

            fhopn = row["fhopn"].ToString().Trim();

            fhot = row["fhot"].ToString().Trim();

            fhdzon = row["fhdzon"].ToString().Trim();

            fhdpn = row["fhdpn"].ToString().Trim();

            fhdt = row["fhdt"].ToString().Trim();

            fhslsm = Convert.ToDecimal(row["fhslsm"]);

            fhtc = row["fhtc"].ToString().Trim();

            fhtcin = row["fhtcin"].ToString().Trim();

            fhrbn = Convert.ToDecimal(row["fhrbn"]);

            fhrbns = row["fhrbns"].ToString().Trim();

            fhtotp = Convert.ToDecimal(row["fhtotp"]);

            fhhaz = row["fhhaz"].ToString().Trim();

            fhdclv = Convert.ToDecimal(row["fhdclv"]);

            fhchg = Convert.ToDecimal(row["fhchg"]);

            fhprod = Convert.ToDecimal(row["fhprod"]);

            fhfbcc = row["fhfbcc"].ToString().Trim();

            fhincc = row["fhincc"].ToString().Trim();

            fhfexc = Convert.ToDecimal(row["fhfexc"]);

            fhcxfs = Convert.ToDecimal(row["fhcxfs"]);

            fhrtyp = row["fhrtyp"].ToString().Trim();

            fhtrm = row["fhtrm"].ToString().Trim();

            fhbilt = row["fhbilt"].ToString().Trim();

            fhpucn = Convert.ToDecimal(row["fhpucn"]);

            fhpadt = Convert.ToDecimal(row["fhpadt"]);

            fhpatm = Convert.ToDecimal(row["fhpatm"]);

            fhpud = Convert.ToDecimal(row["fhpud"]);

            fhpudr = Convert.ToDecimal(row["fhpudr"]);

            fhpudt = Convert.ToDecimal(row["fhpudt"]);

            fhputm = Convert.ToDecimal(row["fhputm"]);

            fhput = row["fhput"].ToString().Trim();

            fhpur = Convert.ToDecimal(row["fhpur"]);

            fhbl = row["fhbl"].ToString().Trim();

            fhsnum = row["fhsnum"].ToString().Trim();

            fhpo = row["fhpo"].ToString().Trim();

            fhdept = row["fhdept"].ToString().Trim();

            fhref = row["fhref"].ToString().Trim();

            fhscc = row["fhscc"].ToString().Trim();

            fhsq = Convert.ToDecimal(row["fhsq"]);

            fhcube = Convert.ToDecimal(row["fhcube"]);

            fhpdg = row["fhpdg"].ToString().Trim();

            fhpdgs = Convert.ToDecimal(row["fhpdgs"]);

            fhtr = row["fhtr"].ToString().Trim();

            fhloap = Convert.ToDecimal(row["fhloap"]);

            fhocd = row["fhocd"].ToString().Trim();

            fhscd = row["fhscd"].ToString().Trim();

            fhsnm = row["fhsnm"].ToString().Trim();

            fhsa1 = row["fhsa1"].ToString().Trim();

            fhsa2 = row["fhsa2"].ToString().Trim();

            fhsct = row["fhsct"].ToString().Trim();

            fhsst = row["fhsst"].ToString().Trim();

            fhszip = row["fhszip"].ToString().Trim();

            fhszp4 = row["fhszp4"].ToString().Trim();

            fhccd = row["fhccd"].ToString().Trim();

            fhcnm = row["fhcnm"].ToString().Trim();

            fhca1 = row["fhca1"].ToString().Trim();

            fhca2 = row["fhca2"].ToString().Trim();

            fhcct = row["fhcct"].ToString().Trim();

            fhcst = row["fhcst"].ToString().Trim();

            fhczip = row["fhczip"].ToString().Trim();

            fhczp4 = row["fhczp4"].ToString().Trim();

            fhbtc = row["fhbtc"].ToString().Trim();

            fhbnm = row["fhbnm"].ToString().Trim();

            fhba1 = row["fhba1"].ToString().Trim();

            fhba2 = row["fhba2"].ToString().Trim();

            fhbct = row["fhbct"].ToString().Trim();

            fhbst = row["fhbst"].ToString().Trim();

            fhbzip = row["fhbzip"].ToString().Trim();

            fhbzp4 = row["fhbzp4"].ToString().Trim();

            fhft = row["fhft"].ToString().Trim();

            fhxf = row["fhxf"].ToString().Trim();

            fhxfa = row["fhxfa"].ToString().Trim();

            fhxfb = row["fhxfb"].ToString().Trim();

            fhxfbd = Convert.ToDecimal(row["fhxfbd"]);

            fhtt = row["fhtt"].ToString().Trim();

            fhxt = row["fhxt"].ToString().Trim();

            fhxta = row["fhxta"].ToString().Trim();

            fhxtb = row["fhxtb"].ToString().Trim();

            fhxtbd = Convert.ToDecimal(row["fhxtbd"]);

            fhfpc = Convert.ToDecimal(row["fhfpc"]);

            fhopc = Convert.ToDecimal(row["fhopc"]);

            fhtpc = Convert.ToDecimal(row["fhtpc"]);

            fhf1 = Convert.ToDecimal(row["fhf$"]);

            fho1 = Convert.ToDecimal(row["fho$"]);

            fht1 = Convert.ToDecimal(row["fht$"]);

            fhpf1 = Convert.ToDecimal(row["fhpf$"]);

            fhpo1 = Convert.ToDecimal(row["fhpo$"]);

            fhpt1 = Convert.ToDecimal(row["fhpt$"]);

            fhfp = row["fhfp"].ToString().Trim();

            fhtp = row["fhtp"].ToString().Trim();

            fhcod = Convert.ToDecimal(row["fhcod"]);

            fhccc = row["fhccc"].ToString().Trim();

            fhcexc = Convert.ToDecimal(row["fhcexc"]);

            fhcxcs = Convert.ToDecimal(row["fhcxcs"]);

            fhotrv = Convert.ToDecimal(row["fhotrv"]);

            fhlhrv = Convert.ToDecimal(row["fhlhrv"]);

            fhbtrv = Convert.ToDecimal(row["fhbtrv"]);

            fhdtrv = Convert.ToDecimal(row["fhdtrv"]);

            fhsprv = Convert.ToDecimal(row["fhsprv"]);

            fhsprc = row["fhsprc"].ToString().Trim();

            fhallf = row["fhallf"].ToString().Trim();

            fhalld = Convert.ToDecimal(row["fhalld"]);

            fhallp = Convert.ToDecimal(row["fhallp"]);

            fhips = row["fhips"].ToString().Trim();

            fhops = row["fhops"].ToString().Trim();

            fhcn = Convert.ToDecimal(row["fhcn"]);

            fhrzon = row["fhrzon"].ToString().Trim();

            fhrpn = row["fhrpn"].ToString().Trim();

            fhrt = row["fhrt"].ToString().Trim();

            fhrtd = Convert.ToDecimal(row["fhrtd"]);

            fhrtt = Convert.ToDecimal(row["fhrtt"]);

            fhrtl = row["fhrtl"].ToString().Trim();

            fhrmnf = Convert.ToDecimal(row["fhrmnf"]);

            fhdadt = Convert.ToDecimal(row["fhdadt"]);

            fhdatm = Convert.ToDecimal(row["fhdatm"]);

            fhdacd = row["fhdacd"].ToString().Trim();

            fhapnm = row["fhapnm"].ToString().Trim();

            fhapdt = Convert.ToDecimal(row["fhapdt"]);

            fhaptm = Convert.ToDecimal(row["fhaptm"]);

            fhradf = Convert.ToDecimal(row["fhradf"]);

            fhradt = Convert.ToDecimal(row["fhradt"]);

            fhmadt = Convert.ToDecimal(row["fhmadt"]);

            fhmatm = Convert.ToDecimal(row["fhmatm"]);

            fhdc = row["fhdc"].ToString().Trim();

            fhddat = Convert.ToDecimal(row["fhddat"]);

            fhdtim = Convert.ToDecimal(row["fhdtim"]);

            fhbbc = row["fhbbc"].ToString().Trim();

            fhnbb = Convert.ToDecimal(row["fhnbb"]);

            fhdday = Convert.ToDecimal(row["fhdday"]);

            fhbbcl = row["fhbbcl"].ToString().Trim();

            fhdld = Convert.ToDecimal(row["fhdld"]);

            fhdldr = Convert.ToDecimal(row["fhdldr"]);

            fhdlt = row["fhdlt"].ToString().Trim();

            fhdlr = Convert.ToDecimal(row["fhdlr"]);

            fhdrc = Convert.ToDecimal(row["fhdrc"]);

            fhfidd = Convert.ToDecimal(row["fhfidd"]);

            fhfidt = Convert.ToDecimal(row["fhfidt"]);

            fhfids = row["fhfids"].ToString().Trim();

            fhuwgt = row["fhuwgt"].ToString().Trim();

            fhswgt = Convert.ToDecimal(row["fhswgt"]);

            fhrwgt = Convert.ToDecimal(row["fhrwgt"]);

            fhtwgt = Convert.ToDecimal(row["fhtwgt"]);

            fhadat = Convert.ToDecimal(row["fhadat"]);

            fhatim = Convert.ToDecimal(row["fhatim"]);

            fhseq = Convert.ToDecimal(row["fhseq"]);

            fhpdat = Convert.ToDecimal(row["fhpdat"]);

            fhudat = Convert.ToDecimal(row["fhudat"]);

            fhutim = Convert.ToDecimal(row["fhutim"]);

            fhuid = row["fhuid"].ToString().Trim();

            fhspro = Convert.ToDecimal(row["fhspro"]);

            fhssuf = row["fhssuf"].ToString().Trim();

            fhclrk = row["fhclrk"].ToString().Trim();

            fhentl = row["fhentl"].ToString().Trim();

            fhrate = row["fhrate"].ToString().Trim();

            fhaud = row["fhaud"].ToString().Trim();

            fhhdss = Convert.ToDecimal(row["fhhdss"]);

            fhhdns = row["fhhdns"].ToString().Trim();

            fhhdsc = Convert.ToDecimal(row["fhhdsc"]);

            fhhdnc = row["fhhdnc"].ToString().Trim();

            fhhdps = Convert.ToDecimal(row["fhhdps"]);

            fhhdos = row["fhhdos"].ToString().Trim();

            fhhdpc = Convert.ToDecimal(row["fhhdpc"]);

            fhhdoc = row["fhhdoc"].ToString().Trim();

            fhrbsa = Convert.ToDecimal(row["fhrbsa"]);

            fhrbsc = row["fhrbsc"].ToString().Trim();

            fhrbpa = Convert.ToDecimal(row["fhrbpa"]);

            fhrbpc = row["fhrbpc"].ToString().Trim();

            fhrevd = Convert.ToDecimal(row["fhrevd"]);

            fhrevc = row["fhrevc"].ToString().Trim();

            fhpurt = Convert.ToDecimal(row["fhpurt"]);

            fhpurp = Convert.ToDecimal(row["fhpurp"]);

            fhotrt = Convert.ToDecimal(row["fhotrt"]);

            fhlhrt = Convert.ToDecimal(row["fhlhrt"]);

            fhbtrt = Convert.ToDecimal(row["fhbtrt"]);

            fhbtrp = row["fhbtrp"].ToString().Trim();

            fhdtrt = Convert.ToDecimal(row["fhdtrt"]);

            fhdtrp = row["fhdtrp"].ToString().Trim();

            fhdlrt = Convert.ToDecimal(row["fhdlrt"]);

            fhdlrp = Convert.ToDecimal(row["fhdlrp"]);

            fhsprt = Convert.ToDecimal(row["fhsprt"]);

            fhsprp = row["fhsprp"].ToString().Trim();

            fhpwgt = Convert.ToDecimal(row["fhpwgt"]);

            fhsuas = Convert.ToDecimal(row["fhsuas"]);

            fhsuac = Convert.ToDecimal(row["fhsuac"]);

            fhsua3 = Convert.ToDecimal(row["fhsua3"]);

            fhsuaf = Convert.ToDecimal(row["fhsuaf"]);

            fhsutf = row["fhsutf"].ToString().Trim();

            fhsuat = Convert.ToDecimal(row["fhsuat"]);

            fhsutt = row["fhsutt"].ToString().Trim();

            fhsuao = Convert.ToDecimal(row["fhsuao"]);

            fhpstb = Convert.ToDecimal(row["fhpstb"]);

            fhpstc = row["fhpstc"].ToString().Trim();

            fhpsts = Convert.ToDecimal(row["fhpsts"]);

            fhgstb = Convert.ToDecimal(row["fhgstb"]);

            fhgstc = row["fhgstc"].ToString().Trim();

            fhgsts = Convert.ToDecimal(row["fhgsts"]);

            fhvatb = Convert.ToDecimal(row["fhvatb"]);

            fhvatc = row["fhvatc"].ToString().Trim();

            fhvats = Convert.ToDecimal(row["fhvats"]);

            fhbils = row["fhbils"].ToString().Trim();

            fhbco = Convert.ToDecimal(row["fhbco"]);

            fhbdiv = Convert.ToDecimal(row["fhbdiv"]);

            fhbcur = row["fhbcur"].ToString().Trim();

            fhbinc = row["fhbinc"].ToString().Trim();

            fhbrvc = row["fhbrvc"].ToString().Trim();

            fhbtyp = row["fhbtyp"].ToString().Trim();

            fhbwgt = Convert.ToDecimal(row["fhbwgt"]);

            fhbdsc = Convert.ToDecimal(row["fhbdsc"]);

            fhbcod = Convert.ToDecimal(row["fhbcod"]);

            fhbsc = row["fhbsc"].ToString().Trim();

            fhbsa = Convert.ToDecimal(row["fhbsa"]);

            fhbsd = Convert.ToDecimal(row["fhbsd"]);

            fhbss = row["fhbss"].ToString().Trim();

            fhbsft = row["fhbsft"].ToString().Trim();

            fhbcc = row["fhbcc"].ToString().Trim();

            fhbca = Convert.ToDecimal(row["fhbca"]);

            fhbcd = Convert.ToDecimal(row["fhbcd"]);

            fhbcs = row["fhbcs"].ToString().Trim();

            fhbcft = row["fhbcft"].ToString().Trim();

            fhb3c = row["fhb3c"].ToString().Trim();

            fhb3a = Convert.ToDecimal(row["fhb3a"]);

            fhb3d = Convert.ToDecimal(row["fhb3d"]);

            fhb3s = row["fhb3s"].ToString().Trim();

            fhbbft = row["fhbbft"].ToString().Trim();

            fhbxfc = row["fhbxfc"].ToString().Trim();

            fhbxfa = Convert.ToDecimal(row["fhbxfa"]);

            fhbxfd = Convert.ToDecimal(row["fhbxfd"]);

            fhbxft = row["fhbxft"].ToString().Trim();

            fhbxfs = row["fhbxfs"].ToString().Trim();

            fhbfft = row["fhbfft"].ToString().Trim();

            fhbxtc = row["fhbxtc"].ToString().Trim();

            fhbxta = Convert.ToDecimal(row["fhbxta"]);

            fhbxtd = Convert.ToDecimal(row["fhbxtd"]);

            fhbxtt = row["fhbxtt"].ToString().Trim();

            fhbxts = row["fhbxts"].ToString().Trim();

            fhbtft = row["fhbtft"].ToString().Trim();

            fhboc = row["fhboc"].ToString().Trim();

            fhboa = Convert.ToDecimal(row["fhboa"]);

            fhbod = Convert.ToDecimal(row["fhbod"]);

            fhboft = row["fhboft"].ToString().Trim();

            fhpstr = Convert.ToDecimal(row["fhpstr"]);

            fhgstr = Convert.ToDecimal(row["fhgstr"]);

            fhvatr = Convert.ToDecimal(row["fhvatr"]);

            fhcxft = Convert.ToDecimal(row["fhcxft"]);

            fhcxct = Convert.ToDecimal(row["fhcxct"]);

            fhxs1 = row["fhxs1"].ToString().Trim();

            fhxs2 = row["fhxs2"].ToString().Trim();

            fhxs3 = row["fhxs3"].ToString().Trim();

            fhxs4 = row["fhxs4"].ToString().Trim();

            fhxa1 = Convert.ToDecimal(row["fhxa1"]);

            fhxa2 = Convert.ToDecimal(row["fhxa2"]);

            fhxa3 = Convert.ToDecimal(row["fhxa3"]);

            fhxa4 = Convert.ToDecimal(row["fhxa4"]);

        }


        /// <summary>
        /// array contructor
        /// </summary>
        /// <param name="values">construct from ToArray() of self</param>
        public FRP001(string[] values)
        {

            fhpro = Convert.ToDecimal(values[0]);

            fhchkd = Convert.ToDecimal(values[1]);

            fhsn = Convert.ToDecimal(values[2]);

            fhccn = values[3];

            fhst = Convert.ToDecimal(values[4]);

            fhshst = values[5];

            fhcs = values[6];

            fhstat = values[7];

            fhbilh = values[8];

            fhbilv = values[9];

            fhcd = values[10];

            fhco = Convert.ToDecimal(values[11]);

            fhdiv = Convert.ToDecimal(values[12]);

            fhozon = values[13];

            fhopn = values[14];

            fhot = values[15];

            fhdzon = values[16];

            fhdpn = values[17];

            fhdt = values[18];

            fhslsm = Convert.ToDecimal(values[19]);

            fhtc = values[20];

            fhtcin = values[21];

            fhrbn = Convert.ToDecimal(values[22]);

            fhrbns = values[23];

            fhtotp = Convert.ToDecimal(values[24]);

            fhhaz = values[25];

            fhdclv = Convert.ToDecimal(values[26]);

            fhchg = Convert.ToDecimal(values[27]);

            fhprod = Convert.ToDecimal(values[28]);

            fhfbcc = values[29];

            fhincc = values[30];

            fhfexc = Convert.ToDecimal(values[31]);

            fhcxfs = Convert.ToDecimal(values[32]);

            fhrtyp = values[33];

            fhtrm = values[34];

            fhbilt = values[35];

            fhpucn = Convert.ToDecimal(values[36]);

            fhpadt = Convert.ToDecimal(values[37]);

            fhpatm = Convert.ToDecimal(values[38]);

            fhpud = Convert.ToDecimal(values[39]);

            fhpudr = Convert.ToDecimal(values[40]);

            fhpudt = Convert.ToDecimal(values[41]);

            fhputm = Convert.ToDecimal(values[42]);

            fhput = values[43];

            fhpur = Convert.ToDecimal(values[44]);

            fhbl = values[45];

            fhsnum = values[46];

            fhpo = values[47];

            fhdept = values[48];

            fhref = values[49];

            fhscc = values[50];

            fhsq = Convert.ToDecimal(values[51]);

            fhcube = Convert.ToDecimal(values[52]);

            fhpdg = values[53];

            fhpdgs = Convert.ToDecimal(values[54]);

            fhtr = values[55];

            fhloap = Convert.ToDecimal(values[56]);

            fhocd = values[57];

            fhscd = values[58];

            fhsnm = values[59];

            fhsa1 = values[60];

            fhsa2 = values[61];

            fhsct = values[62];

            fhsst = values[63];

            fhszip = values[64];

            fhszp4 = values[65];

            fhccd = values[66];

            fhcnm = values[67];

            fhca1 = values[68];

            fhca2 = values[69];

            fhcct = values[70];

            fhcst = values[71];

            fhczip = values[72];

            fhczp4 = values[73];

            fhbtc = values[74];

            fhbnm = values[75];

            fhba1 = values[76];

            fhba2 = values[77];

            fhbct = values[78];

            fhbst = values[79];

            fhbzip = values[80];

            fhbzp4 = values[81];

            fhft = values[82];

            fhxf = values[83];

            fhxfa = values[84];

            fhxfb = values[85];

            fhxfbd = Convert.ToDecimal(values[86]);

            fhtt = values[87];

            fhxt = values[88];

            fhxta = values[89];

            fhxtb = values[90];

            fhxtbd = Convert.ToDecimal(values[91]);

            fhfpc = Convert.ToDecimal(values[92]);

            fhopc = Convert.ToDecimal(values[93]);

            fhtpc = Convert.ToDecimal(values[94]);

            fhf1 = Convert.ToDecimal(values[95]);

            fho1 = Convert.ToDecimal(values[96]);

            fht1 = Convert.ToDecimal(values[97]);

            fhpf1 = Convert.ToDecimal(values[98]);

            fhpo1 = Convert.ToDecimal(values[99]);

            fhpt1 = Convert.ToDecimal(values[100]);

            fhfp = values[101];

            fhtp = values[102];

            fhcod = Convert.ToDecimal(values[103]);

            fhccc = values[104];

            fhcexc = Convert.ToDecimal(values[105]);

            fhcxcs = Convert.ToDecimal(values[106]);

            fhotrv = Convert.ToDecimal(values[107]);

            fhlhrv = Convert.ToDecimal(values[108]);

            fhbtrv = Convert.ToDecimal(values[109]);

            fhdtrv = Convert.ToDecimal(values[110]);

            fhsprv = Convert.ToDecimal(values[111]);

            fhsprc = values[112];

            fhallf = values[113];

            fhalld = Convert.ToDecimal(values[114]);

            fhallp = Convert.ToDecimal(values[115]);

            fhips = values[116];

            fhops = values[117];

            fhcn = Convert.ToDecimal(values[118]);

            fhrzon = values[119];

            fhrpn = values[120];

            fhrt = values[121];

            fhrtd = Convert.ToDecimal(values[122]);

            fhrtt = Convert.ToDecimal(values[123]);

            fhrtl = values[124];

            fhrmnf = Convert.ToDecimal(values[125]);

            fhdadt = Convert.ToDecimal(values[126]);

            fhdatm = Convert.ToDecimal(values[127]);

            fhdacd = values[128];

            fhapnm = values[129];

            fhapdt = Convert.ToDecimal(values[130]);

            fhaptm = Convert.ToDecimal(values[131]);

            fhradf = Convert.ToDecimal(values[132]);

            fhradt = Convert.ToDecimal(values[133]);

            fhmadt = Convert.ToDecimal(values[134]);

            fhmatm = Convert.ToDecimal(values[135]);

            fhdc = values[136];

            fhddat = Convert.ToDecimal(values[137]);

            fhdtim = Convert.ToDecimal(values[138]);

            fhbbc = values[139];

            fhnbb = Convert.ToDecimal(values[140]);

            fhdday = Convert.ToDecimal(values[141]);

            fhbbcl = values[142];

            fhdld = Convert.ToDecimal(values[143]);

            fhdldr = Convert.ToDecimal(values[144]);

            fhdlt = values[145];

            fhdlr = Convert.ToDecimal(values[146]);

            fhdrc = Convert.ToDecimal(values[147]);

            fhfidd = Convert.ToDecimal(values[148]);

            fhfidt = Convert.ToDecimal(values[149]);

            fhfids = values[150];

            fhuwgt = values[151];

            fhswgt = Convert.ToDecimal(values[152]);

            fhrwgt = Convert.ToDecimal(values[153]);

            fhtwgt = Convert.ToDecimal(values[154]);

            fhadat = Convert.ToDecimal(values[155]);

            fhatim = Convert.ToDecimal(values[156]);

            fhseq = Convert.ToDecimal(values[157]);

            fhpdat = Convert.ToDecimal(values[158]);

            fhudat = Convert.ToDecimal(values[159]);

            fhutim = Convert.ToDecimal(values[160]);

            fhuid = values[161];

            fhspro = Convert.ToDecimal(values[162]);

            fhssuf = values[163];

            fhclrk = values[164];

            fhentl = values[165];

            fhrate = values[166];

            fhaud = values[167];

            fhhdss = Convert.ToDecimal(values[168]);

            fhhdns = values[169];

            fhhdsc = Convert.ToDecimal(values[170]);

            fhhdnc = values[171];

            fhhdps = Convert.ToDecimal(values[172]);

            fhhdos = values[173];

            fhhdpc = Convert.ToDecimal(values[174]);

            fhhdoc = values[175];

            fhrbsa = Convert.ToDecimal(values[176]);

            fhrbsc = values[177];

            fhrbpa = Convert.ToDecimal(values[178]);

            fhrbpc = values[179];

            fhrevd = Convert.ToDecimal(values[180]);

            fhrevc = values[181];

            fhpurt = Convert.ToDecimal(values[182]);

            fhpurp = Convert.ToDecimal(values[183]);

            fhotrt = Convert.ToDecimal(values[184]);

            fhlhrt = Convert.ToDecimal(values[185]);

            fhbtrt = Convert.ToDecimal(values[186]);

            fhbtrp = values[187];

            fhdtrt = Convert.ToDecimal(values[188]);

            fhdtrp = values[189];

            fhdlrt = Convert.ToDecimal(values[190]);

            fhdlrp = Convert.ToDecimal(values[191]);

            fhsprt = Convert.ToDecimal(values[192]);

            fhsprp = values[193];

            fhpwgt = Convert.ToDecimal(values[194]);

            fhsuas = Convert.ToDecimal(values[195]);

            fhsuac = Convert.ToDecimal(values[196]);

            fhsua3 = Convert.ToDecimal(values[197]);

            fhsuaf = Convert.ToDecimal(values[198]);

            fhsutf = values[199];

            fhsuat = Convert.ToDecimal(values[200]);

            fhsutt = values[201];

            fhsuao = Convert.ToDecimal(values[202]);

            fhpstb = Convert.ToDecimal(values[203]);

            fhpstc = values[204];

            fhpsts = Convert.ToDecimal(values[205]);

            fhgstb = Convert.ToDecimal(values[206]);

            fhgstc = values[207];

            fhgsts = Convert.ToDecimal(values[208]);

            fhvatb = Convert.ToDecimal(values[209]);

            fhvatc = values[210];

            fhvats = Convert.ToDecimal(values[211]);

            fhbils = values[212];

            fhbco = Convert.ToDecimal(values[213]);

            fhbdiv = Convert.ToDecimal(values[214]);

            fhbcur = values[215];

            fhbinc = values[216];

            fhbrvc = values[217];

            fhbtyp = values[218];

            fhbwgt = Convert.ToDecimal(values[219]);

            fhbdsc = Convert.ToDecimal(values[220]);

            fhbcod = Convert.ToDecimal(values[221]);

            fhbsc = values[222];

            fhbsa = Convert.ToDecimal(values[223]);

            fhbsd = Convert.ToDecimal(values[224]);

            fhbss = values[225];

            fhbsft = values[226];

            fhbcc = values[227];

            fhbca = Convert.ToDecimal(values[228]);

            fhbcd = Convert.ToDecimal(values[229]);

            fhbcs = values[230];

            fhbcft = values[231];

            fhb3c = values[232];

            fhb3a = Convert.ToDecimal(values[233]);

            fhb3d = Convert.ToDecimal(values[234]);

            fhb3s = values[235];

            fhbbft = values[236];

            fhbxfc = values[237];

            fhbxfa = Convert.ToDecimal(values[238]);

            fhbxfd = Convert.ToDecimal(values[239]);

            fhbxft = values[240];

            fhbxfs = values[241];

            fhbfft = values[242];

            fhbxtc = values[243];

            fhbxta = Convert.ToDecimal(values[244]);

            fhbxtd = Convert.ToDecimal(values[245]);

            fhbxtt = values[246];

            fhbxts = values[247];

            fhbtft = values[248];

            fhboc = values[249];

            fhboa = Convert.ToDecimal(values[250]);

            fhbod = Convert.ToDecimal(values[251]);

            fhboft = values[252];

            fhpstr = Convert.ToDecimal(values[253]);

            fhgstr = Convert.ToDecimal(values[254]);

            fhvatr = Convert.ToDecimal(values[255]);

            fhcxft = Convert.ToDecimal(values[256]);

            fhcxct = Convert.ToDecimal(values[257]);

            fhxs1 = values[258];

            fhxs2 = values[259];

            fhxs3 = values[260];

            fhxs4 = values[261];

            fhxa1 = Convert.ToDecimal(values[262]);

            fhxa2 = Convert.ToDecimal(values[263]);

            fhxa3 = Convert.ToDecimal(values[264]);

            fhxa4 = Convert.ToDecimal(values[265]);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>an array of the class properties</returns>
        public string[] ToArray()
        {
            return new string[] { fhpro.ToString(), fhchkd.ToString(), fhsn.ToString(), fhccn, fhst.ToString(), fhshst, fhcs, fhstat, fhbilh, fhbilv, fhcd, fhco.ToString(), fhdiv.ToString(), fhozon, fhopn, fhot, fhdzon, fhdpn, fhdt, fhslsm.ToString(), fhtc, fhtcin, fhrbn.ToString(), fhrbns, fhtotp.ToString(), fhhaz, fhdclv.ToString(), fhchg.ToString(), fhprod.ToString(), fhfbcc, fhincc, fhfexc.ToString(), fhcxfs.ToString(), fhrtyp, fhtrm, fhbilt, fhpucn.ToString(), fhpadt.ToString(), fhpatm.ToString(), fhpud.ToString(), fhpudr.ToString(), fhpudt.ToString(), fhputm.ToString(), fhput, fhpur.ToString(), fhbl, fhsnum, fhpo, fhdept, fhref, fhscc, fhsq.ToString(), fhcube.ToString(), fhpdg, fhpdgs.ToString(), fhtr, fhloap.ToString(), fhocd, fhscd, fhsnm, fhsa1, fhsa2, fhsct, fhsst, fhszip, fhszp4, fhccd, fhcnm, fhca1, fhca2, fhcct, fhcst, fhczip, fhczp4, fhbtc, fhbnm, fhba1, fhba2, fhbct, fhbst, fhbzip, fhbzp4, fhft, fhxf, fhxfa, fhxfb, fhxfbd.ToString(), fhtt, fhxt, fhxta, fhxtb, fhxtbd.ToString(), fhfpc.ToString(), fhopc.ToString(), fhtpc.ToString(), fhf1.ToString(), fho1.ToString(), fht1.ToString(), fhpf1.ToString(), fhpo1.ToString(), fhpt1.ToString(), fhfp, fhtp, fhcod.ToString(), fhccc, fhcexc.ToString(), fhcxcs.ToString(), fhotrv.ToString(), fhlhrv.ToString(), fhbtrv.ToString(), fhdtrv.ToString(), fhsprv.ToString(), fhsprc, fhallf, fhalld.ToString(), fhallp.ToString(), fhips, fhops, fhcn.ToString(), fhrzon, fhrpn, fhrt, fhrtd.ToString(), fhrtt.ToString(), fhrtl, fhrmnf.ToString(), fhdadt.ToString(), fhdatm.ToString(), fhdacd, fhapnm, fhapdt.ToString(), fhaptm.ToString(), fhradf.ToString(), fhradt.ToString(), fhmadt.ToString(), fhmatm.ToString(), fhdc, fhddat.ToString(), fhdtim.ToString(), fhbbc, fhnbb.ToString(), fhdday.ToString(), fhbbcl, fhdld.ToString(), fhdldr.ToString(), fhdlt, fhdlr.ToString(), fhdrc.ToString(), fhfidd.ToString(), fhfidt.ToString(), fhfids, fhuwgt, fhswgt.ToString(), fhrwgt.ToString(), fhtwgt.ToString(), fhadat.ToString(), fhatim.ToString(), fhseq.ToString(), fhpdat.ToString(), fhudat.ToString(), fhutim.ToString(), fhuid, fhspro.ToString(), fhssuf, fhclrk, fhentl, fhrate, fhaud, fhhdss.ToString(), fhhdns, fhhdsc.ToString(), fhhdnc, fhhdps.ToString(), fhhdos, fhhdpc.ToString(), fhhdoc, fhrbsa.ToString(), fhrbsc, fhrbpa.ToString(), fhrbpc, fhrevd.ToString(), fhrevc, fhpurt.ToString(), fhpurp.ToString(), fhotrt.ToString(), fhlhrt.ToString(), fhbtrt.ToString(), fhbtrp, fhdtrt.ToString(), fhdtrp, fhdlrt.ToString(), fhdlrp.ToString(), fhsprt.ToString(), fhsprp, fhpwgt.ToString(), fhsuas.ToString(), fhsuac.ToString(), fhsua3.ToString(), fhsuaf.ToString(), fhsutf, fhsuat.ToString(), fhsutt, fhsuao.ToString(), fhpstb.ToString(), fhpstc, fhpsts.ToString(), fhgstb.ToString(), fhgstc, fhgsts.ToString(), fhvatb.ToString(), fhvatc, fhvats.ToString(), fhbils, fhbco.ToString(), fhbdiv.ToString(), fhbcur, fhbinc, fhbrvc, fhbtyp, fhbwgt.ToString(), fhbdsc.ToString(), fhbcod.ToString(), fhbsc, fhbsa.ToString(), fhbsd.ToString(), fhbss, fhbsft, fhbcc, fhbca.ToString(), fhbcd.ToString(), fhbcs, fhbcft, fhb3c, fhb3a.ToString(), fhb3d.ToString(), fhb3s, fhbbft, fhbxfc, fhbxfa.ToString(), fhbxfd.ToString(), fhbxft, fhbxfs, fhbfft, fhbxtc, fhbxta.ToString(), fhbxtd.ToString(), fhbxtt, fhbxts, fhbtft, fhboc, fhboa.ToString(), fhbod.ToString(), fhboft, fhpstr.ToString(), fhgstr.ToString(), fhvatr.ToString(), fhcxft.ToString(), fhcxct.ToString(), fhxs1, fhxs2, fhxs3, fhxs4, fhxa1.ToString(), fhxa2.ToString(), fhxa3.ToString(), fhxa4.ToString() };
        }


        #region properties


        // 11.0
        /// <summary>
        /// Pro Number
        /// </summary>
        [Display(Name = "Pro Number")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal fhpro { get; set; }

        // 1.0
        /// <summary>
        /// Check Digit
        /// </summary>
        [Display(Name = "Check Digit")]
        [DefaultValue(0)]
        [Range(0, 9)]
        public decimal fhchkd { get; set; }

        // 15.0
        /// <summary>
        /// Shipment Number
        /// </summary>
        [Display(Name = "Shipment Number")]
        [DefaultValue(0)]
        [Range(0, 999999999999999)]
        public decimal fhsn { get; set; }

        // 20
        /// <summary>
        /// Cargo Control/PARS Number
        /// </summary>
        [Display(Name = "Cargo Control/PARS Number")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "Cargo Control/PARS Number cannot be longer than 20 characters.")]
        public string fhccn { get; set; }

        // 1.0
        /// <summary>
        /// Type Of Shipment
        /// </summary>
        [Display(Name = "Type Of Shipment")]
        [DefaultValue(0)]
        [Range(0, 9)]
        public decimal fhst { get; set; }

        // 2
        /// <summary>
        /// Shipment Subtype
        /// </summary>
        [Display(Name = "Shipment Subtype")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Shipment Subtype cannot be longer than 2 characters.")]
        public string fhshst { get; set; }

        // 1
        /// <summary>
        /// Coding Status For Billing
        /// </summary>
        [Display(Name = "Coding Status For Billing")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Coding Status For Billing cannot be longer than 1 characters.")]
        public string fhcs { get; set; }

        // 2
        /// <summary>
        /// Processing Status
        /// </summary>
        [Display(Name = "Processing Status")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Processing Status cannot be longer than 2 characters.")]
        public string fhstat { get; set; }

        // 1
        /// <summary>
        /// Freight Bill Hold Code
        /// </summary>
        [Display(Name = "Freight Bill Hold Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Freight Bill Hold Code cannot be longer than 1 characters.")]
        public string fhbilh { get; set; }

        // 1
        /// <summary>
        /// Freight Bill Void Code
        /// </summary>
        [Display(Name = "Freight Bill Void Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Freight Bill Void Code cannot be longer than 1 characters.")]
        public string fhbilv { get; set; }

        // 2
        /// <summary>
        /// Company/Division ID
        /// </summary>
        [Display(Name = "Company/Division ID")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Company/Division ID cannot be longer than 2 characters.")]
        public string fhcd { get; set; }

        // 3.0
        /// <summary>
        /// Company Number
        /// </summary>
        [Display(Name = "Company Number")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhco { get; set; }

        // 3.0
        /// <summary>
        /// Division Number
        /// </summary>
        [Display(Name = "Division Number")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhdiv { get; set; }

        // 3
        /// <summary>
        /// Origin Zone
        /// </summary>
        [Display(Name = "Origin Zone")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Origin Zone cannot be longer than 3 characters.")]
        public string fhozon { get; set; }

        // 7
        /// <summary>
        /// Origin Point
        /// </summary>
        [Display(Name = "Origin Point")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Origin Point cannot be longer than 7 characters.")]
        public string fhopn { get; set; }

        // 5
        /// <summary>
        /// Origin Terminal
        /// </summary>
        [Display(Name = "Origin Terminal")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Origin Terminal cannot be longer than 5 characters.")]
        public string fhot { get; set; }

        // 3
        /// <summary>
        /// Destination Zone
        /// </summary>
        [Display(Name = "Destination Zone")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Destination Zone cannot be longer than 3 characters.")]
        public string fhdzon { get; set; }

        // 7
        /// <summary>
        /// Destination Point
        /// </summary>
        [Display(Name = "Destination Point")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Destination Point cannot be longer than 7 characters.")]
        public string fhdpn { get; set; }

        // 5
        /// <summary>
        /// Destination Terminal
        /// </summary>
        [Display(Name = "Destination Terminal")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Destination Terminal cannot be longer than 5 characters.")]
        public string fhdt { get; set; }

        // 7.0
        /// <summary>
        /// Salesman Number/Territory
        /// </summary>
        [Display(Name = "Salesman Number/Territory")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhslsm { get; set; }

        // 10
        /// <summary>
        /// Tariff Code
        /// </summary>
        [Display(Name = "Tariff Code")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Tariff Code cannot be longer than 10 characters.")]
        public string fhtc { get; set; }

        // 5
        /// <summary>
        /// Tariff Item Number
        /// </summary>
        [Display(Name = "Tariff Item Number")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Tariff Item Number cannot be longer than 5 characters.")]
        public string fhtcin { get; set; }

        // 6.0
        /// <summary>
        /// Rate Basis Number
        /// </summary>
        [Display(Name = "Rate Basis Number")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal fhrbn { get; set; }

        // 2
        /// <summary>
        /// Rate Basis Number Suffix
        /// </summary>
        [Display(Name = "Rate Basis Number Suffix")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Rate Basis Number Suffix cannot be longer than 2 characters.")]
        public string fhrbns { get; set; }

        // 5.0
        /// <summary>
        /// Total Pieces
        /// </summary>
        [Display(Name = "Total Pieces")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal fhtotp { get; set; }

        // 1
        /// <summary>
        /// Hazardous Material Flag
        /// </summary>
        [Display(Name = "Hazardous Material Flag")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Hazardous Material Flag cannot be longer than 1 characters.")]
        public string fhhaz { get; set; }

        // 9.2
        /// <summary>
        /// Declared Value
        /// </summary>
        [Display(Name = "Declared Value")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal fhdclv { get; set; }

        // 7.2
        /// <summary>
        /// Total Freight Charges
        /// </summary>
        [Display(Name = "Total Freight Charges")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhchg { get; set; }

        // 7.2
        /// <summary>
        /// Total Freight Bill Charges
        /// </summary>
        [Display(Name = "Total Freight Bill Charges")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhprod { get; set; }

        // 1
        /// <summary>
        /// Freight Bill Currency Code
        /// </summary>
        [Display(Name = "Freight Bill Currency Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Freight Bill Currency Code cannot be longer than 1 characters.")]
        public string fhfbcc { get; set; }

        // 1
        /// <summary>
        /// Invoicing Currency Code
        /// </summary>
        [Display(Name = "Invoicing Currency Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Invoicing Currency Code cannot be longer than 1 characters.")]
        public string fhincc { get; set; }

        // 5.4
        /// <summary>
        /// Freight Exchange Rate
        /// </summary>
        [Display(Name = "Freight Exchange Rate")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal fhfexc { get; set; }

        // 7.2
        /// <summary>
        /// Freight Exchange Setup
        /// </summary>
        [Display(Name = "Freight Exchange Setup")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhcxfs { get; set; }

        // 1
        /// <summary>
        /// Rate Type
        /// </summary>
        [Display(Name = "Rate Type")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Rate Type cannot be longer than 1 characters.")]
        public string fhrtyp { get; set; }

        // 3
        /// <summary>
        /// Billing Terms
        /// </summary>
        [Display(Name = "Billing Terms")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Billing Terms cannot be longer than 3 characters.")]
        public string fhtrm { get; set; }

        // 1
        /// <summary>
        /// Billing Terms Type
        /// </summary>
        [Display(Name = "Billing Terms Type")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billing Terms Type cannot be longer than 1 characters.")]
        public string fhbilt { get; set; }

        // 7.0
        /// <summary>
        /// Pickup Number
        /// </summary>
        [Display(Name = "Pickup Number")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpucn { get; set; }

        // 7.0
        /// <summary>
        /// Pickup Appointment Date
        /// </summary>
        [Display(Name = "Pickup Appointment Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpadt { get; set; }

        // 4.0
        /// <summary>
        /// Pickup Appointment Time
        /// </summary>
        [Display(Name = "Pickup Appointment Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhpatm { get; set; }

        // 7.0
        /// <summary>
        /// Pickup Driver
        /// </summary>
        [Display(Name = "Pickup Driver")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpud { get; set; }

        // 1.0
        /// <summary>
        /// Pickup Driver Run
        /// </summary>
        [Display(Name = "Pickup Driver Run")]
        [DefaultValue(0)]
        [Range(0, 9)]
        public decimal fhpudr { get; set; }

        // 7.0
        /// <summary>
        /// Pickup Date
        /// </summary>
        [Display(Name = "Pickup Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpudt { get; set; }

        // 4.0
        /// <summary>
        /// Pickup Time
        /// </summary>
        [Display(Name = "Pickup Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhputm { get; set; }

        // 1
        /// <summary>
        /// Pickup Type
        /// </summary>
        [Display(Name = "Pickup Type")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Pickup Type cannot be longer than 1 characters.")]
        public string fhput { get; set; }

        // 7.2
        /// <summary>
        /// Pickup Driver Revenue
        /// </summary>
        [Display(Name = "Pickup Driver Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpur { get; set; }

        // 20
        /// <summary>
        /// Bill Of Lading
        /// </summary>
        [Display(Name = "Bill Of Lading")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "Bill Of Lading cannot be longer than 20 characters.")]
        public string fhbl { get; set; }

        // 20
        /// <summary>
        /// Shipper's Number
        /// </summary>
        [Display(Name = "Shipper's Number")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "Shipper's Number cannot be longer than 20 characters.")]
        public string fhsnum { get; set; }

        // 20
        /// <summary>
        /// Purchase Order
        /// </summary>
        [Display(Name = "Purchase Order")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "Purchase Order cannot be longer than 20 characters.")]
        public string fhpo { get; set; }

        // 10
        /// <summary>
        /// Purchase Order Department
        /// </summary>
        [Display(Name = "Purchase Order Department")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Purchase Order Department cannot be longer than 10 characters.")]
        public string fhdept { get; set; }

        // 20
        /// <summary>
        /// Reference Number
        /// </summary>
        [Display(Name = "Reference Number")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "Reference Number cannot be longer than 20 characters.")]
        public string fhref { get; set; }

        // 2
        /// <summary>
        /// Special Commodity Code
        /// </summary>
        [Display(Name = "Special Commodity Code")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Special Commodity Code cannot be longer than 2 characters.")]
        public string fhscc { get; set; }

        // 5.0
        /// <summary>
        /// Shipment Square Measure
        /// </summary>
        [Display(Name = "Shipment Square Measure")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal fhsq { get; set; }

        // 5.0
        /// <summary>
        /// Shipment Cubic Measure
        /// </summary>
        [Display(Name = "Shipment Cubic Measure")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal fhcube { get; set; }

        // 6
        /// <summary>
        /// Pickup/Delivery Grid
        /// </summary>
        [Display(Name = "Pickup/Delivery Grid")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Pickup/Delivery Grid cannot be longer than 6 characters.")]
        public string fhpdg { get; set; }

        // 3.0
        /// <summary>
        /// Pickup/Delivery Grid Sequence
        /// </summary>
        [Display(Name = "Pickup/Delivery Grid Sequence")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhpdgs { get; set; }

        // 15
        /// <summary>
        /// Trailer Number
        /// </summary>
        [Display(Name = "Trailer Number")]
        [DefaultValue("")]
        [StringLength(15, ErrorMessage = "Trailer Number cannot be longer than 15 characters.")]
        public string fhtr { get; set; }

        // 3.0
        /// <summary>
        /// Loading Position
        /// </summary>
        [Display(Name = "Loading Position")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhloap { get; set; }

        // 7
        /// <summary>
        /// Originator Code
        /// </summary>
        [Display(Name = "Originator Code")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Originator Code cannot be longer than 7 characters.")]
        public string fhocd { get; set; }

        // 7
        /// <summary>
        /// Shipper Code
        /// </summary>
        [Display(Name = "Shipper Code")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Shipper Code cannot be longer than 7 characters.")]
        public string fhscd { get; set; }

        // 30
        /// <summary>
        /// Shipper Name
        /// </summary>
        [Display(Name = "Shipper Name")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Shipper Name cannot be longer than 30 characters.")]
        public string fhsnm { get; set; }

        // 30
        /// <summary>
        /// Shipper Address 1
        /// </summary>
        [Display(Name = "Shipper Address 1")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Shipper Address 1 cannot be longer than 30 characters.")]
        public string fhsa1 { get; set; }

        // 30
        /// <summary>
        /// Shipper Address 2
        /// </summary>
        [Display(Name = "Shipper Address 2")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Shipper Address 2 cannot be longer than 30 characters.")]
        public string fhsa2 { get; set; }

        // 20
        /// <summary>
        /// Shipper City
        /// </summary>
        [Display(Name = "Shipper City")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "Shipper City cannot be longer than 20 characters.")]
        public string fhsct { get; set; }

        // 2
        /// <summary>
        /// Shipper State
        /// </summary>
        [Display(Name = "Shipper State")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Shipper State cannot be longer than 2 characters.")]
        public string fhsst { get; set; }

        // 6
        /// <summary>
        /// Shipper Zip Code
        /// </summary>
        [Display(Name = "Shipper Zip Code")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Shipper Zip Code cannot be longer than 6 characters.")]
        public string fhszip { get; set; }

        // 4
        /// <summary>
        /// Shipper Zip + 4 Code
        /// </summary>
        [Display(Name = "Shipper Zip + 4 Code")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Shipper Zip + 4 Code cannot be longer than 4 characters.")]
        public string fhszp4 { get; set; }

        // 7
        /// <summary>
        /// Consignee Code
        /// </summary>
        [Display(Name = "Consignee Code")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Consignee Code cannot be longer than 7 characters.")]
        public string fhccd { get; set; }

        // 30
        /// <summary>
        /// Consignee Name
        /// </summary>
        [Display(Name = "Consignee Name")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Consignee Name cannot be longer than 30 characters.")]
        public string fhcnm { get; set; }

        // 30
        /// <summary>
        /// Consignee Address 1
        /// </summary>
        [Display(Name = "Consignee Address 1")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Consignee Address 1 cannot be longer than 30 characters.")]
        public string fhca1 { get; set; }

        // 30
        /// <summary>
        /// Consignee Address 2
        /// </summary>
        [Display(Name = "Consignee Address 2")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Consignee Address 2 cannot be longer than 30 characters.")]
        public string fhca2 { get; set; }

        // 20
        /// <summary>
        /// Consignee City
        /// </summary>
        [Display(Name = "Consignee City")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "Consignee City cannot be longer than 20 characters.")]
        public string fhcct { get; set; }

        // 2
        /// <summary>
        /// Consignee State
        /// </summary>
        [Display(Name = "Consignee State")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Consignee State cannot be longer than 2 characters.")]
        public string fhcst { get; set; }

        // 6
        /// <summary>
        /// Consignee Zip Code
        /// </summary>
        [Display(Name = "Consignee Zip Code")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Consignee Zip Code cannot be longer than 6 characters.")]
        public string fhczip { get; set; }

        // 4
        /// <summary>
        /// Consignee Zip + 4 Code
        /// </summary>
        [Display(Name = "Consignee Zip + 4 Code")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Consignee Zip + 4 Code cannot be longer than 4 characters.")]
        public string fhczp4 { get; set; }

        // 7
        /// <summary>
        /// 3Rd Party Bill-To Code
        /// </summary>
        [Display(Name = "3Rd Party Bill-To Code")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "3Rd Party Bill-To Code cannot be longer than 7 characters.")]
        public string fhbtc { get; set; }

        // 30
        /// <summary>
        /// 3Rd Party Name
        /// </summary>
        [Display(Name = "3Rd Party Name")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "3Rd Party Name cannot be longer than 30 characters.")]
        public string fhbnm { get; set; }

        // 30
        /// <summary>
        /// 3Rd Party Address 1
        /// </summary>
        [Display(Name = "3Rd Party Address 1")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "3Rd Party Address 1 cannot be longer than 30 characters.")]
        public string fhba1 { get; set; }

        // 30
        /// <summary>
        /// 3Rd Party Address 2
        /// </summary>
        [Display(Name = "3Rd Party Address 2")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "3Rd Party Address 2 cannot be longer than 30 characters.")]
        public string fhba2 { get; set; }

        // 20
        /// <summary>
        /// 3Rd Party City
        /// </summary>
        [Display(Name = "3Rd Party City")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "3Rd Party City cannot be longer than 20 characters.")]
        public string fhbct { get; set; }

        // 2
        /// <summary>
        /// 3Rd Party State
        /// </summary>
        [Display(Name = "3Rd Party State")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "3Rd Party State cannot be longer than 2 characters.")]
        public string fhbst { get; set; }

        // 6
        /// <summary>
        /// 3Rd Party Zip Code
        /// </summary>
        [Display(Name = "3Rd Party Zip Code")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "3Rd Party Zip Code cannot be longer than 6 characters.")]
        public string fhbzip { get; set; }

        // 4
        /// <summary>
        /// 3Rd Party Zip + 4 Code
        /// </summary>
        [Display(Name = "3Rd Party Zip + 4 Code")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "3Rd Party Zip + 4 Code cannot be longer than 4 characters.")]
        public string fhbzp4 { get; set; }

        // 5
        /// <summary>
        /// From/Advance Terminal
        /// </summary>
        [Display(Name = "From/Advance Terminal")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "From/Advance Terminal cannot be longer than 5 characters.")]
        public string fhft { get; set; }

        // 6
        /// <summary>
        /// Transfer-From Carrier
        /// </summary>
        [Display(Name = "Transfer-From Carrier")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Transfer-From Carrier cannot be longer than 6 characters.")]
        public string fhxf { get; set; }

        // 15
        /// <summary>
        /// Transfer-From Carrier At
        /// </summary>
        [Display(Name = "Transfer-From Carrier At")]
        [DefaultValue("")]
        [StringLength(15, ErrorMessage = "Transfer-From Carrier At cannot be longer than 15 characters.")]
        public string fhxfa { get; set; }

        // 15
        /// <summary>
        /// Transfer-From FB #
        /// </summary>
        [Display(Name = "Transfer-From FB #")]
        [DefaultValue("")]
        [StringLength(15, ErrorMessage = "Transfer-From FB # cannot be longer than 15 characters.")]
        public string fhxfb { get; set; }

        // 7.0
        /// <summary>
        /// Transfer-From FB Date
        /// </summary>
        [Display(Name = "Transfer-From FB Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhxfbd { get; set; }

        // 5
        /// <summary>
        /// To/Beyond Terminal
        /// </summary>
        [Display(Name = "To/Beyond Terminal")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "To/Beyond Terminal cannot be longer than 5 characters.")]
        public string fhtt { get; set; }

        // 6
        /// <summary>
        /// Transfer-To Carrier
        /// </summary>
        [Display(Name = "Transfer-To Carrier")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Transfer-To Carrier cannot be longer than 6 characters.")]
        public string fhxt { get; set; }

        // 15
        /// <summary>
        /// Transfer-To Carrier At
        /// </summary>
        [Display(Name = "Transfer-To Carrier At")]
        [DefaultValue("")]
        [StringLength(15, ErrorMessage = "Transfer-To Carrier At cannot be longer than 15 characters.")]
        public string fhxta { get; set; }

        // 15
        /// <summary>
        /// Transfer-To FB #
        /// </summary>
        [Display(Name = "Transfer-To FB #")]
        [DefaultValue("")]
        [StringLength(15, ErrorMessage = "Transfer-To FB # cannot be longer than 15 characters.")]
        public string fhxtb { get; set; }

        // 7.0
        /// <summary>
        /// Transfer-To FB Date
        /// </summary>
        [Display(Name = "Transfer-To FB Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhxtbd { get; set; }

        // 3.2
        /// <summary>
        /// From Carrier Split %'Age
        /// </summary>
        [Display(Name = "From Carrier Split %'Age")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhfpc { get; set; }

        // 3.2
        /// <summary>
        /// Our Split %'Age
        /// </summary>
        [Display(Name = "Our Split %'Age")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhopc { get; set; }

        // 3.2
        /// <summary>
        /// To Carrier Split %'Age
        /// </summary>
        [Display(Name = "To Carrier Split %'Age")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhtpc { get; set; }

        // 7.2
        /// <summary>
        /// From Carrier Freight
        /// </summary>
        [Display(Name = "From Carrier Freight")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhf1 { get; set; }

        // 7.2
        /// <summary>
        /// Our Freight
        /// </summary>
        [Display(Name = "Our Freight")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fho1 { get; set; }

        // 7.2
        /// <summary>
        /// To Carrier Freight
        /// </summary>
        [Display(Name = "To Carrier Freight")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fht1 { get; set; }

        // 7.2
        /// <summary>
        /// From Carrier Revenue
        /// </summary>
        [Display(Name = "From Carrier Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpf1 { get; set; }

        // 7.2
        /// <summary>
        /// Our Revenue
        /// </summary>
        [Display(Name = "Our Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpo1 { get; set; }

        // 7.2
        /// <summary>
        /// To Carrier Revenue
        /// </summary>
        [Display(Name = "To Carrier Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpt1 { get; set; }

        // 7
        /// <summary>
        /// From/Exit Port
        /// </summary>
        [Display(Name = "From/Exit Port")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "From/Exit Port cannot be longer than 7 characters.")]
        public string fhfp { get; set; }

        // 7
        /// <summary>
        /// To/Entry Port
        /// </summary>
        [Display(Name = "To/Entry Port")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "To/Entry Port cannot be longer than 7 characters.")]
        public string fhtp { get; set; }

        // 9.2
        /// <summary>
        /// COD Amount
        /// </summary>
        [Display(Name = "COD Amount")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal fhcod { get; set; }

        // 1
        /// <summary>
        /// COD Currency Code
        /// </summary>
        [Display(Name = "COD Currency Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "COD Currency Code cannot be longer than 1 characters.")]
        public string fhccc { get; set; }

        // 5.4
        /// <summary>
        /// COD Exchange Rate
        /// </summary>
        [Display(Name = "COD Exchange Rate")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal fhcexc { get; set; }

        // 7.2
        /// <summary>
        /// COD Exchange Setup
        /// </summary>
        [Display(Name = "COD Exchange Setup")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhcxcs { get; set; }

        // 7.2
        /// <summary>
        /// Origin Terminal Revenue
        /// </summary>
        [Display(Name = "Origin Terminal Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhotrv { get; set; }

        // 7.2
        /// <summary>
        /// LH/GO Revenue
        /// </summary>
        [Display(Name = "LH/GO Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhlhrv { get; set; }

        // 7.2
        /// <summary>
        /// Break Terminal Revenue
        /// </summary>
        [Display(Name = "Break Terminal Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbtrv { get; set; }

        // 7.2
        /// <summary>
        /// Destination Terminal Revenue
        /// </summary>
        [Display(Name = "Destination Terminal Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhdtrv { get; set; }

        // 7.2
        /// <summary>
        /// Special Revenue
        /// </summary>
        [Display(Name = "Special Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhsprv { get; set; }

        // 2
        /// <summary>
        /// Special Revenue Code
        /// </summary>
        [Display(Name = "Special Revenue Code")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Special Revenue Code cannot be longer than 2 characters.")]
        public string fhsprc { get; set; }

        // 1
        /// <summary>
        /// Discount Function
        /// </summary>
        [Display(Name = "Discount Function")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Discount Function cannot be longer than 1 characters.")]
        public string fhallf { get; set; }

        // 7.2
        /// <summary>
        /// Discount Amount
        /// </summary>
        [Display(Name = "Discount Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhalld { get; set; }

        // 4.4
        /// <summary>
        /// Discount Percentage
        /// </summary>
        [Display(Name = "Discount Percentage")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhallp { get; set; }

        // 1
        /// <summary>
        /// Inbound Print Status
        /// </summary>
        [Display(Name = "Inbound Print Status")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Inbound Print Status cannot be longer than 1 characters.")]
        public string fhips { get; set; }

        // 1
        /// <summary>
        /// Outbound Print Status
        /// </summary>
        [Display(Name = "Outbound Print Status")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Outbound Print Status cannot be longer than 1 characters.")]
        public string fhops { get; set; }

        // 1.0
        /// <summary>
        /// Printed Copy Number
        /// </summary>
        [Display(Name = "Printed Copy Number")]
        [DefaultValue(0)]
        [Range(0, 9)]
        public decimal fhcn { get; set; }

        // 3
        /// <summary>
        /// Current Zone
        /// </summary>
        [Display(Name = "Current Zone")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Current Zone cannot be longer than 3 characters.")]
        public string fhrzon { get; set; }

        // 7
        /// <summary>
        /// Current Point
        /// </summary>
        [Display(Name = "Current Point")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Current Point cannot be longer than 7 characters.")]
        public string fhrpn { get; set; }

        // 5
        /// <summary>
        /// Current Terminal
        /// </summary>
        [Display(Name = "Current Terminal")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Current Terminal cannot be longer than 5 characters.")]
        public string fhrt { get; set; }

        // 7.0
        /// <summary>
        /// Current Location Date
        /// </summary>
        [Display(Name = "Current Location Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhrtd { get; set; }

        // 4.0
        /// <summary>
        /// Current Location Time
        /// </summary>
        [Display(Name = "Current Location Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhrtt { get; set; }

        // 4
        /// <summary>
        /// Current Location Bay
        /// </summary>
        [Display(Name = "Current Location Bay")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Current Location Bay cannot be longer than 4 characters.")]
        public string fhrtl { get; set; }

        // 9.0
        /// <summary>
        /// Manifest Number
        /// </summary>
        [Display(Name = "Manifest Number")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal fhrmnf { get; set; }

        // 7.0
        /// <summary>
        /// Delivery Appointment Date
        /// </summary>
        [Display(Name = "Delivery Appointment Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhdadt { get; set; }

        // 4.0
        /// <summary>
        /// Delivery Appointment Time
        /// </summary>
        [Display(Name = "Delivery Appointment Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhdatm { get; set; }

        // 3
        /// <summary>
        /// Delivery Appointment Reason
        /// </summary>
        [Display(Name = "Delivery Appointment Reason")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Delivery Appointment Reason cannot be longer than 3 characters.")]
        public string fhdacd { get; set; }

        // 10
        /// <summary>
        /// Appointment Made By
        /// </summary>
        [Display(Name = "Appointment Made By")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Appointment Made By cannot be longer than 10 characters.")]
        public string fhapnm { get; set; }

        // 7.0
        /// <summary>
        /// Appt. Established On Date
        /// </summary>
        [Display(Name = "Appt. Established On Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhapdt { get; set; }

        // 4.0
        /// <summary>
        /// Appt. Established At Time
        /// </summary>
        [Display(Name = "Appt. Established At Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhaptm { get; set; }

        // 7.0
        /// <summary>
        /// RAD From Date
        /// </summary>
        [Display(Name = "RAD From Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhradf { get; set; }

        // 7.0
        /// <summary>
        /// RAD To Date
        /// </summary>
        [Display(Name = "RAD To Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhradt { get; set; }

        // 7.0
        /// <summary>
        /// Made Available Date
        /// </summary>
        [Display(Name = "Made Available Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhmadt { get; set; }

        // 4.0
        /// <summary>
        /// Made Available Time
        /// </summary>
        [Display(Name = "Made Available Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhmatm { get; set; }

        // 1
        /// <summary>
        /// Delivered To Customer Code
        /// </summary>
        [Display(Name = "Delivered To Customer Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Delivered To Customer Code cannot be longer than 1 characters.")]
        public string fhdc { get; set; }

        // 7.0
        /// <summary>
        /// Last Delivery Date
        /// </summary>
        [Display(Name = "Last Delivery Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhddat { get; set; }

        // 4.0
        /// <summary>
        /// Last Delivery Time
        /// </summary>
        [Display(Name = "Last Delivery Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhdtim { get; set; }

        // 3
        /// <summary>
        /// Last Delivery Status
        /// </summary>
        [Display(Name = "Last Delivery Status")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Last Delivery Status cannot be longer than 3 characters.")]
        public string fhbbc { get; set; }

        // 3.0
        /// <summary>
        /// Number Of Unsuccessful Delivery Attempts
        /// </summary>
        [Display(Name = "Number Of Unsuccessful Delivery Attempts")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhnbb { get; set; }

        // 2.0
        /// <summary>
        /// Service Days
        /// </summary>
        [Display(Name = "Service Days")]
        [DefaultValue(0)]
        [Range(0, 99)]
        public decimal fhdday { get; set; }

        // 3
        /// <summary>
        /// Late Delivery Reason Code
        /// </summary>
        [Display(Name = "Late Delivery Reason Code")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Late Delivery Reason Code cannot be longer than 3 characters.")]
        public string fhbbcl { get; set; }

        // 7.0
        /// <summary>
        /// Delivery Driver
        /// </summary>
        [Display(Name = "Delivery Driver")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhdld { get; set; }

        // 1.0
        /// <summary>
        /// Delivery Driver Run
        /// </summary>
        [Display(Name = "Delivery Driver Run")]
        [DefaultValue(0)]
        [Range(0, 9)]
        public decimal fhdldr { get; set; }

        // 1
        /// <summary>
        /// Delivery Type
        /// </summary>
        [Display(Name = "Delivery Type")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Delivery Type cannot be longer than 1 characters.")]
        public string fhdlt { get; set; }

        // 7.2
        /// <summary>
        /// Delivery Driver Revenue
        /// </summary>
        [Display(Name = "Delivery Driver Revenue")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhdlr { get; set; }

        // 7.2
        /// <summary>
        /// Amount For Driver Collection
        /// </summary>
        [Display(Name = "Amount For Driver Collection")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhdrc { get; set; }

        // 7.0
        /// <summary>
        /// Final Delivery Date
        /// </summary>
        [Display(Name = "Final Delivery Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhfidd { get; set; }

        // 4.0
        /// <summary>
        /// Final Delivery Time
        /// </summary>
        [Display(Name = "Final Delivery Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fhfidt { get; set; }

        // 3
        /// <summary>
        /// Final Delivery Status
        /// </summary>
        [Display(Name = "Final Delivery Status")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Final Delivery Status cannot be longer than 3 characters.")]
        public string fhfids { get; set; }

        // 1
        /// <summary>
        /// Units Of Weight
        /// </summary>
        [Display(Name = "Units Of Weight")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Units Of Weight cannot be longer than 1 characters.")]
        public string fhuwgt { get; set; }

        // 7.0
        /// <summary>
        /// Shipping Weight
        /// </summary>
        [Display(Name = "Shipping Weight")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhswgt { get; set; }

        // 7.0
        /// <summary>
        /// Rated Weight
        /// </summary>
        [Display(Name = "Rated Weight")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhrwgt { get; set; }

        // 7.0
        /// <summary>
        /// Tare Weight
        /// </summary>
        [Display(Name = "Tare Weight")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhtwgt { get; set; }

        // 7.0
        /// <summary>
        /// FB Add Date
        /// </summary>
        [Display(Name = "FB Add Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhadat { get; set; }

        // 6.0
        /// <summary>
        /// FB Add Time
        /// </summary>
        [Display(Name = "FB Add Time")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal fhatim { get; set; }

        // 9.0
        /// <summary>
        /// Sequence Number
        /// </summary>
        [Display(Name = "Sequence Number")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal fhseq { get; set; }

        // 7.0
        /// <summary>
        /// FB Entry Date
        /// </summary>
        [Display(Name = "FB Entry Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpdat { get; set; }

        // 7.0
        /// <summary>
        /// Last Changed, Date
        /// </summary>
        [Display(Name = "Last Changed, Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhudat { get; set; }

        // 6.0
        /// <summary>
        /// Last Changed, Time
        /// </summary>
        [Display(Name = "Last Changed, Time")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal fhutim { get; set; }

        // 10
        /// <summary>
        /// Last Changed, User Profile
        /// </summary>
        [Display(Name = "Last Changed, User Profile")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Last Changed, User Profile cannot be longer than 10 characters.")]
        public string fhuid { get; set; }

        // 11.0
        /// <summary>
        /// Referenced Bill Number
        /// </summary>
        [Display(Name = "Referenced Bill Number")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal fhspro { get; set; }

        // 2
        /// <summary>
        /// Freight Bill Subtype
        /// </summary>
        [Display(Name = "Freight Bill Subtype")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Freight Bill Subtype cannot be longer than 2 characters.")]
        public string fhssuf { get; set; }

        // 10
        /// <summary>
        /// Billing Clerk
        /// </summary>
        [Display(Name = "Billing Clerk")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Billing Clerk cannot be longer than 10 characters.")]
        public string fhclrk { get; set; }

        // 5
        /// <summary>
        /// Entering Terminal ID
        /// </summary>
        [Display(Name = "Entering Terminal ID")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Entering Terminal ID cannot be longer than 5 characters.")]
        public string fhentl { get; set; }

        // 10
        /// <summary>
        /// Original Rating Clerk
        /// </summary>
        [Display(Name = "Original Rating Clerk")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Original Rating Clerk cannot be longer than 10 characters.")]
        public string fhrate { get; set; }

        // 10
        /// <summary>
        /// Auditing Rate Clerk
        /// </summary>
        [Display(Name = "Auditing Rate Clerk")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Auditing Rate Clerk cannot be longer than 10 characters.")]
        public string fhaud { get; set; }

        // 7.2
        /// <summary>
        /// Loading Allowance Setup Amount
        /// </summary>
        [Display(Name = "Loading Allowance Setup Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhhdss { get; set; }

        // 7
        /// <summary>
        /// Loading Allowance Setup Account
        /// </summary>
        [Display(Name = "Loading Allowance Setup Account")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Loading Allowance Setup Account cannot be longer than 7 characters.")]
        public string fhhdns { get; set; }

        // 7.2
        /// <summary>
        /// Unloading Allowance Setup Amount
        /// </summary>
        [Display(Name = "Unloading Allowance Setup Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhhdsc { get; set; }

        // 7
        /// <summary>
        /// Unloading Allowance Setup Account
        /// </summary>
        [Display(Name = "Unloading Allowance Setup Account")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Unloading Allowance Setup Account cannot be longer than 7 characters.")]
        public string fhhdnc { get; set; }

        // 7.2
        /// <summary>
        /// Loading Allowance Paid Amount
        /// </summary>
        [Display(Name = "Loading Allowance Paid Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhhdps { get; set; }

        // 7
        /// <summary>
        /// Loading Allowance Paid-To Account
        /// </summary>
        [Display(Name = "Loading Allowance Paid-To Account")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Loading Allowance Paid-To Account cannot be longer than 7 characters.")]
        public string fhhdos { get; set; }

        // 7.2
        /// <summary>
        /// Unloading Allowance Paid Amount
        /// </summary>
        [Display(Name = "Unloading Allowance Paid Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhhdpc { get; set; }

        // 7
        /// <summary>
        /// Unloading Allowance Paid-To Account
        /// </summary>
        [Display(Name = "Unloading Allowance Paid-To Account")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Unloading Allowance Paid-To Account cannot be longer than 7 characters.")]
        public string fhhdoc { get; set; }

        // 7.2
        /// <summary>
        /// EOM Discount Setup Amount
        /// </summary>
        [Display(Name = "EOM Discount Setup Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhrbsa { get; set; }

        // 7
        /// <summary>
        /// EOM Discount Setup Account
        /// </summary>
        [Display(Name = "EOM Discount Setup Account")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "EOM Discount Setup Account cannot be longer than 7 characters.")]
        public string fhrbsc { get; set; }

        // 7.2
        /// <summary>
        /// EOM Discount Paid Amount
        /// </summary>
        [Display(Name = "EOM Discount Paid Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhrbpa { get; set; }

        // 7
        /// <summary>
        /// EOM Discount Paid-To Account
        /// </summary>
        [Display(Name = "EOM Discount Paid-To Account")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "EOM Discount Paid-To Account cannot be longer than 7 characters.")]
        public string fhrbpc { get; set; }

        // 7.0
        /// <summary>
        /// Revenue Date
        /// </summary>
        [Display(Name = "Revenue Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhrevd { get; set; }

        // 1
        /// <summary>
        /// Revenue Currency
        /// </summary>
        [Display(Name = "Revenue Currency")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Revenue Currency cannot be longer than 1 characters.")]
        public string fhrevc { get; set; }

        // 7.2
        /// <summary>
        /// Pickup Revenue Taken
        /// </summary>
        [Display(Name = "Pickup Revenue Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpurt { get; set; }

        // 7.0
        /// <summary>
        /// Pickup Revenue Paid To
        /// </summary>
        [Display(Name = "Pickup Revenue Paid To")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpurp { get; set; }

        // 7.2
        /// <summary>
        /// OT Revenue Taken
        /// </summary>
        [Display(Name = "OT Revenue Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhotrt { get; set; }

        // 7.2
        /// <summary>
        /// LH/GO Revenue Taken
        /// </summary>
        [Display(Name = "LH/GO Revenue Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhlhrt { get; set; }

        // 7.2
        /// <summary>
        /// BT Revenue Taken
        /// </summary>
        [Display(Name = "BT Revenue Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbtrt { get; set; }

        // 5
        /// <summary>
        /// BT Revenue Paid To
        /// </summary>
        [Display(Name = "BT Revenue Paid To")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "BT Revenue Paid To cannot be longer than 5 characters.")]
        public string fhbtrp { get; set; }

        // 7.2
        /// <summary>
        /// DT Revenue Taken
        /// </summary>
        [Display(Name = "DT Revenue Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhdtrt { get; set; }

        // 5
        /// <summary>
        /// DT Revenue Paid To
        /// </summary>
        [Display(Name = "DT Revenue Paid To")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "DT Revenue Paid To cannot be longer than 5 characters.")]
        public string fhdtrp { get; set; }

        // 7.2
        /// <summary>
        /// Delivery Revenue Taken
        /// </summary>
        [Display(Name = "Delivery Revenue Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhdlrt { get; set; }

        // 7.0
        /// <summary>
        /// Delivery Revenue Paid To
        /// </summary>
        [Display(Name = "Delivery Revenue Paid To")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhdlrp { get; set; }

        // 7.2
        /// <summary>
        /// Special Revenue Taken
        /// </summary>
        [Display(Name = "Special Revenue Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhsprt { get; set; }

        // 2
        /// <summary>
        /// Special Revenue Paid To
        /// </summary>
        [Display(Name = "Special Revenue Paid To")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Special Revenue Paid To cannot be longer than 2 characters.")]
        public string fhsprp { get; set; }

        // 7.0
        /// <summary>
        /// Revenue Posting Weight
        /// </summary>
        [Display(Name = "Revenue Posting Weight")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpwgt { get; set; }

        // 7.2
        /// <summary>
        /// Setup Amount, Shipper
        /// </summary>
        [Display(Name = "Setup Amount, Shipper")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhsuas { get; set; }

        // 7.2
        /// <summary>
        /// Setup Amount, Consignee
        /// </summary>
        [Display(Name = "Setup Amount, Consignee")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhsuac { get; set; }

        // 7.2
        /// <summary>
        /// Setup Amount, 3Rd Party
        /// </summary>
        [Display(Name = "Setup Amount, 3Rd Party")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhsua3 { get; set; }

        // 7.2
        /// <summary>
        /// Setup Amount, Transfer-From Carrier
        /// </summary>
        [Display(Name = "Setup Amount, Transfer-From Carrier")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhsuaf { get; set; }

        // 1
        /// <summary>
        /// Setup Type, Transfer-From Carrier
        /// </summary>
        [Display(Name = "Setup Type, Transfer-From Carrier")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Setup Type, Transfer-From Carrier cannot be longer than 1 characters.")]
        public string fhsutf { get; set; }

        // 7.2
        /// <summary>
        /// Setup Amount, Transfer-To Carrier
        /// </summary>
        [Display(Name = "Setup Amount, Transfer-To Carrier")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhsuat { get; set; }

        // 1
        /// <summary>
        /// Setup Type, Transfer-To Carrier
        /// </summary>
        [Display(Name = "Setup Type, Transfer-To Carrier")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Setup Type, Transfer-To Carrier cannot be longer than 1 characters.")]
        public string fhsutt { get; set; }

        // 7.2
        /// <summary>
        /// Setup Amount, Originator
        /// </summary>
        [Display(Name = "Setup Amount, Originator")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhsuao { get; set; }

        // 7.2
        /// <summary>
        /// PST Basis Amount
        /// </summary>
        [Display(Name = "PST Basis Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpstb { get; set; }

        // 1
        /// <summary>
        /// PST Basis Code
        /// </summary>
        [Display(Name = "PST Basis Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "PST Basis Code cannot be longer than 1 characters.")]
        public string fhpstc { get; set; }

        // 7.2
        /// <summary>
        /// PST Setup Amount
        /// </summary>
        [Display(Name = "PST Setup Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpsts { get; set; }

        // 7.2
        /// <summary>
        /// GST Basis Amount
        /// </summary>
        [Display(Name = "GST Basis Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhgstb { get; set; }

        // 1
        /// <summary>
        /// GST Basis Code
        /// </summary>
        [Display(Name = "GST Basis Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "GST Basis Code cannot be longer than 1 characters.")]
        public string fhgstc { get; set; }

        // 7.2
        /// <summary>
        /// GST Setup Amount
        /// </summary>
        [Display(Name = "GST Setup Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhgsts { get; set; }

        // 7.2
        /// <summary>
        /// VAT Basis Amount
        /// </summary>
        [Display(Name = "VAT Basis Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhvatb { get; set; }

        // 1
        /// <summary>
        /// VAT Basis Code
        /// </summary>
        [Display(Name = "VAT Basis Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "VAT Basis Code cannot be longer than 1 characters.")]
        public string fhvatc { get; set; }

        // 7.2
        /// <summary>
        /// VAT Setup Amount
        /// </summary>
        [Display(Name = "VAT Setup Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhvats { get; set; }

        // 1
        /// <summary>
        /// Billing Status
        /// </summary>
        [Display(Name = "Billing Status")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billing Status cannot be longer than 1 characters.")]
        public string fhbils { get; set; }

        // 3.0
        /// <summary>
        /// Billed Company
        /// </summary>
        [Display(Name = "Billed Company")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhbco { get; set; }

        // 3.0
        /// <summary>
        /// Billed Division
        /// </summary>
        [Display(Name = "Billed Division")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fhbdiv { get; set; }

        // 1
        /// <summary>
        /// Billed FB Currency
        /// </summary>
        [Display(Name = "Billed FB Currency")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed FB Currency cannot be longer than 1 characters.")]
        public string fhbcur { get; set; }

        // 1
        /// <summary>
        /// Billed Inv. Currency
        /// </summary>
        [Display(Name = "Billed Inv. Currency")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Inv. Currency cannot be longer than 1 characters.")]
        public string fhbinc { get; set; }

        // 1
        /// <summary>
        /// Billed Revenue Currency
        /// </summary>
        [Display(Name = "Billed Revenue Currency")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Revenue Currency cannot be longer than 1 characters.")]
        public string fhbrvc { get; set; }

        // 1
        /// <summary>
        /// Billed Type
        /// </summary>
        [Display(Name = "Billed Type")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Type cannot be longer than 1 characters.")]
        public string fhbtyp { get; set; }

        // 7.0
        /// <summary>
        /// Billed Weight
        /// </summary>
        [Display(Name = "Billed Weight")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbwgt { get; set; }

        // 7.2
        /// <summary>
        /// Billed Discount
        /// </summary>
        [Display(Name = "Billed Discount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbdsc { get; set; }

        // 9.2
        /// <summary>
        /// Billed COD Amount
        /// </summary>
        [Display(Name = "Billed COD Amount")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal fhbcod { get; set; }

        // 7
        /// <summary>
        /// Billed Shipper
        /// </summary>
        [Display(Name = "Billed Shipper")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Billed Shipper cannot be longer than 7 characters.")]
        public string fhbsc { get; set; }

        // 7.2
        /// <summary>
        /// Billed Shipper Amount
        /// </summary>
        [Display(Name = "Billed Shipper Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbsa { get; set; }

        // 7.0
        /// <summary>
        /// Billed Shipper Date
        /// </summary>
        [Display(Name = "Billed Shipper Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbsd { get; set; }

        // 1
        /// <summary>
        /// Billed Shipper Status
        /// </summary>
        [Display(Name = "Billed Shipper Status")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Shipper Status cannot be longer than 1 characters.")]
        public string fhbss { get; set; }

        // 1
        /// <summary>
        /// Billed Shipper Funds
        /// </summary>
        [Display(Name = "Billed Shipper Funds")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Shipper Funds cannot be longer than 1 characters.")]
        public string fhbsft { get; set; }

        // 7
        /// <summary>
        /// Billed Consignee
        /// </summary>
        [Display(Name = "Billed Consignee")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Billed Consignee cannot be longer than 7 characters.")]
        public string fhbcc { get; set; }

        // 7.2
        /// <summary>
        /// Billed Consignee Amount
        /// </summary>
        [Display(Name = "Billed Consignee Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbca { get; set; }

        // 7.0
        /// <summary>
        /// Billed Consignee Date
        /// </summary>
        [Display(Name = "Billed Consignee Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbcd { get; set; }

        // 1
        /// <summary>
        /// Billed Consignee Status
        /// </summary>
        [Display(Name = "Billed Consignee Status")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Consignee Status cannot be longer than 1 characters.")]
        public string fhbcs { get; set; }

        // 1
        /// <summary>
        /// Billed Consignee Funds
        /// </summary>
        [Display(Name = "Billed Consignee Funds")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Consignee Funds cannot be longer than 1 characters.")]
        public string fhbcft { get; set; }

        // 7
        /// <summary>
        /// Billed 3Rd Party
        /// </summary>
        [Display(Name = "Billed 3Rd Party")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Billed 3Rd Party cannot be longer than 7 characters.")]
        public string fhb3c { get; set; }

        // 7.2
        /// <summary>
        /// Billed 3Rd Party Amount
        /// </summary>
        [Display(Name = "Billed 3Rd Party Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhb3a { get; set; }

        // 7.0
        /// <summary>
        /// Billed 3Rd Party Date
        /// </summary>
        [Display(Name = "Billed 3Rd Party Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhb3d { get; set; }

        // 1
        /// <summary>
        /// Billed 3Rd Party Status
        /// </summary>
        [Display(Name = "Billed 3Rd Party Status")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed 3Rd Party Status cannot be longer than 1 characters.")]
        public string fhb3s { get; set; }

        // 1
        /// <summary>
        /// Billed Bill-To Funds
        /// </summary>
        [Display(Name = "Billed Bill-To Funds")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Bill-To Funds cannot be longer than 1 characters.")]
        public string fhbbft { get; set; }

        // 7
        /// <summary>
        /// Billed Transfer-From Carrier
        /// </summary>
        [Display(Name = "Billed Transfer-From Carrier")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Billed Transfer-From Carrier cannot be longer than 7 characters.")]
        public string fhbxfc { get; set; }

        // 7.2
        /// <summary>
        /// Billed Transfer-From Amount
        /// </summary>
        [Display(Name = "Billed Transfer-From Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbxfa { get; set; }

        // 7.0
        /// <summary>
        /// Billed Transfer-From Date
        /// </summary>
        [Display(Name = "Billed Transfer-From Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbxfd { get; set; }

        // 1
        /// <summary>
        /// Billed Transfer-From Type
        /// </summary>
        [Display(Name = "Billed Transfer-From Type")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Transfer-From Type cannot be longer than 1 characters.")]
        public string fhbxft { get; set; }

        // 1
        /// <summary>
        /// Billed Transfer-From Status
        /// </summary>
        [Display(Name = "Billed Transfer-From Status")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Transfer-From Status cannot be longer than 1 characters.")]
        public string fhbxfs { get; set; }

        // 1
        /// <summary>
        /// Billed Advance Funds
        /// </summary>
        [Display(Name = "Billed Advance Funds")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Advance Funds cannot be longer than 1 characters.")]
        public string fhbfft { get; set; }

        // 7
        /// <summary>
        /// Billed Transfer-To Carrier
        /// </summary>
        [Display(Name = "Billed Transfer-To Carrier")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Billed Transfer-To Carrier cannot be longer than 7 characters.")]
        public string fhbxtc { get; set; }

        // 7.2
        /// <summary>
        /// Billed Transfer-To Amount
        /// </summary>
        [Display(Name = "Billed Transfer-To Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbxta { get; set; }

        // 7.0
        /// <summary>
        /// Billed Transfer-To Date
        /// </summary>
        [Display(Name = "Billed Transfer-To Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbxtd { get; set; }

        // 1
        /// <summary>
        /// Billed Transfer-To Type
        /// </summary>
        [Display(Name = "Billed Transfer-To Type")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Transfer-To Type cannot be longer than 1 characters.")]
        public string fhbxtt { get; set; }

        // 1
        /// <summary>
        /// Billed Transfer-To Status
        /// </summary>
        [Display(Name = "Billed Transfer-To Status")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Transfer-To Status cannot be longer than 1 characters.")]
        public string fhbxts { get; set; }

        // 1
        /// <summary>
        /// Billed Beyond Funds
        /// </summary>
        [Display(Name = "Billed Beyond Funds")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Beyond Funds cannot be longer than 1 characters.")]
        public string fhbtft { get; set; }

        // 7
        /// <summary>
        /// Billed Originator
        /// </summary>
        [Display(Name = "Billed Originator")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Billed Originator cannot be longer than 7 characters.")]
        public string fhboc { get; set; }

        // 7.2
        /// <summary>
        /// Billed Originator Amount
        /// </summary>
        [Display(Name = "Billed Originator Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhboa { get; set; }

        // 7.0
        /// <summary>
        /// Billed Originator Date
        /// </summary>
        [Display(Name = "Billed Originator Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhbod { get; set; }

        // 1
        /// <summary>
        /// Billed Originator Funds
        /// </summary>
        [Display(Name = "Billed Originator Funds")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Billed Originator Funds cannot be longer than 1 characters.")]
        public string fhboft { get; set; }

        // 7.2
        /// <summary>
        /// PST Remitted Amount
        /// </summary>
        [Display(Name = "PST Remitted Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhpstr { get; set; }

        // 7.2
        /// <summary>
        /// GST Remitted Amount
        /// </summary>
        [Display(Name = "GST Remitted Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhgstr { get; set; }

        // 7.2
        /// <summary>
        /// VAT Remitted Amount
        /// </summary>
        [Display(Name = "VAT Remitted Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhvatr { get; set; }

        // 7.2
        /// <summary>
        /// Freight Exchange Taken
        /// </summary>
        [Display(Name = "Freight Exchange Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhcxft { get; set; }

        // 7.2
        /// <summary>
        /// COD Exchange Taken
        /// </summary>
        [Display(Name = "COD Exchange Taken")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhcxct { get; set; }

        // 1
        /// <summary>
        /// Extra Status Code 1
        /// </summary>
        [Display(Name = "Extra Status Code 1")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Extra Status Code 1 cannot be longer than 1 characters.")]
        public string fhxs1 { get; set; }

        // 1
        /// <summary>
        /// Extra Status Code 2
        /// </summary>
        [Display(Name = "Extra Status Code 2")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Extra Status Code 2 cannot be longer than 1 characters.")]
        public string fhxs2 { get; set; }

        // 2
        /// <summary>
        /// Extra Status Code 3
        /// </summary>
        [Display(Name = "Extra Status Code 3")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Extra Status Code 3 cannot be longer than 2 characters.")]
        public string fhxs3 { get; set; }

        // 4
        /// <summary>
        /// Extra Status Code 4
        /// </summary>
        [Display(Name = "Extra Status Code 4")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Extra Status Code 4 cannot be longer than 4 characters.")]
        public string fhxs4 { get; set; }

        // 7.2
        /// <summary>
        /// Extra Amount 1
        /// </summary>
        [Display(Name = "Extra Amount 1")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhxa1 { get; set; }

        // 7.2
        /// <summary>
        /// Extra Amount 2
        /// </summary>
        [Display(Name = "Extra Amount 2")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhxa2 { get; set; }

        // 7.2
        /// <summary>
        /// Extra Amount 3
        /// </summary>
        [Display(Name = "Extra Amount 3")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhxa3 { get; set; }

        // 7.2
        /// <summary>
        /// Extra Amount 4
        /// </summary>
        [Display(Name = "Extra Amount 4")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fhxa4 { get; set; }



        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;


namespace FinalMile
{
    /// <summary>
    /// 
    /// </summary>
    public class FRP001EX
    {
        /// <summary>
        /// base select
        /// </summary>
        //private const string SQL_SELECT = "select f2pro, f2xaid, f2clnt, f2atyp, f2plnt, f2dlva, f2bapd, f2capd, f2argd, f2grct, f2ncrs, f2chg, f23pt, f2spud, f2xs1, f2xs2, f2xs3, f2xs4, f2xs5, f2xs6, f2xs7, f2xs8, f2xs9, f2xa1, f2xa2, f2xa3, f2xa4, f2xa5, f2xa6, f2xa7, f2xa8, f2xa9, f2xn1, f2xn2, f2xn3, f2xn4, f2xn5, f2xn6, f2xn7, f2xn8, f2xn9, f2xn10, f2xn11, f2xn12, f2xdt1, f2xdt2, f2xdt3, f2xt1, f2xt2, f2xt3, f2xt4, f2xt5, f2xt6 from FRP001EX where *** ";

        /// <summary>
        /// base insert
        /// </summary>
        //private const string SQL_INSERT = "insert into FRP001EX (f2pro, f2xaid, f2clnt, f2atyp, f2plnt, f2dlva, f2bapd, f2capd, f2argd, f2grct, f2ncrs, f2chg, f23pt, f2spud, f2xs1, f2xs2, f2xs3, f2xs4, f2xs5, f2xs6, f2xs7, f2xs8, f2xs9, f2xa1, f2xa2, f2xa3, f2xa4, f2xa5, f2xa6, f2xa7, f2xa8, f2xa9, f2xn1, f2xn2, f2xn3, f2xn4, f2xn5, f2xn6, f2xn7, f2xn8, f2xn9, f2xn10, f2xn11, f2xn12, f2xdt1, f2xdt2, f2xdt3, f2xt1, f2xt2, f2xt3, f2xt4, f2xt5, f2xt6) values ({0}, '{1}', '{2}', '{3}', '{4}', '{5}', {6}, {7}, '{8}', {9}, '{10}', '{11}', '{12}', {13}, '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}', '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', '{31}', {32}, {33}, {34}, {35}, {36}, {37}, {38}, {39}, {40}, {41}, {42}, {43}, {44}, {45}, {46}, {47}, {48}, {49}, {50}, {51}, {52}) ";

        /// <summary>
        /// base update
        /// </summary>
        //private const string SQL_UPDATE = "update FRP001EX set f2pro = {0}, f2xaid = '{1}', f2clnt = '{2}', f2atyp = '{3}', f2plnt = '{4}', f2dlva = '{5}', f2bapd = {6}, f2capd = {7}, f2argd = '{8}', f2grct = {9}, f2ncrs = '{10}', f2chg = '{11}', f23pt = '{12}', f2spud = {13}, f2xs1 = '{14}', f2xs2 = '{15}', f2xs3 = '{16}', f2xs4 = '{17}', f2xs5 = '{18}', f2xs6 = '{19}', f2xs7 = '{20}', f2xs8 = '{21}', f2xs9 = '{22}', f2xa1 = '{23}', f2xa2 = '{24}', f2xa3 = '{25}', f2xa4 = '{26}', f2xa5 = '{27}', f2xa6 = '{28}', f2xa7 = '{29}', f2xa8 = '{30}', f2xa9 = '{31}', f2xn1 = {32}, f2xn2 = {33}, f2xn3 = {34}, f2xn4 = {35}, f2xn5 = {36}, f2xn6 = {37}, f2xn7 = {38}, f2xn8 = {39}, f2xn9 = {40}, f2xn10 = {41}, f2xn11 = {42}, f2xn12 = {43}, f2xdt1 = {44}, f2xdt2 = {45}, f2xdt3 = {46}, f2xt1 = {47}, f2xt2 = {48}, f2xt3 = {49}, f2xt4 = {50}, f2xt5 = {51}, f2xt6 = {52} where *** ";

        /// <summary>
        /// base delete
        /// </summary>
        //private const string SQL_DELETE = "delete from FRP001EX where *** ";

        /// <summary>
        /// default contructor
        /// </summary>
        public FRP001EX() { }

        /// <summary>
        /// preferred contructor
        /// </summary>
        /// <param name="row"></param>
        public FRP001EX(DataRow row)
        {

            f2pro = Convert.ToDecimal(row["f2pro"]);

            f2xaid = row["f2xaid"].ToString().Trim();

            f2clnt = row["f2clnt"].ToString().Trim();

            f2atyp = row["f2atyp"].ToString().Trim();

            f2plnt = row["f2plnt"].ToString().Trim();

            f2dlva = row["f2dlva"].ToString().Trim();

            f2bapd = Convert.ToDecimal(row["f2bapd"]);

            f2capd = Convert.ToDecimal(row["f2capd"]);

            f2argd = row["f2argd"].ToString().Trim();

            f2grct = Convert.ToDecimal(row["f2grct"]);

            f2ncrs = row["f2ncrs"].ToString().Trim();

            f2chg = row["f2chg"].ToString().Trim();

            f23pt = row["f23pt"].ToString().Trim();

            f2spud = Convert.ToDecimal(row["f2spud"]);

            f2xs1 = row["f2xs1"].ToString().Trim();

            f2xs2 = row["f2xs2"].ToString().Trim();

            f2xs3 = row["f2xs3"].ToString().Trim();

            f2xs4 = row["f2xs4"].ToString().Trim();

            f2xs5 = row["f2xs5"].ToString().Trim();

            f2xs6 = row["f2xs6"].ToString().Trim();

            f2xs7 = row["f2xs7"].ToString().Trim();

            f2xs8 = row["f2xs8"].ToString().Trim();

            f2xs9 = row["f2xs9"].ToString().Trim();

            f2xa1 = row["f2xa1"].ToString().Trim();

            f2xa2 = row["f2xa2"].ToString().Trim();

            f2xa3 = row["f2xa3"].ToString().Trim();

            f2xa4 = row["f2xa4"].ToString().Trim();

            f2xa5 = row["f2xa5"].ToString().Trim();

            f2xa6 = row["f2xa6"].ToString().Trim();

            f2xa7 = row["f2xa7"].ToString().Trim();

            f2xa8 = row["f2xa8"].ToString().Trim();

            f2xa9 = row["f2xa9"].ToString().Trim();

            f2xn1 = Convert.ToDecimal(row["f2xn1"]);

            f2xn2 = Convert.ToDecimal(row["f2xn2"]);

            f2xn3 = Convert.ToDecimal(row["f2xn3"]);

            f2xn4 = Convert.ToDecimal(row["f2xn4"]);

            f2xn5 = Convert.ToDecimal(row["f2xn5"]);

            f2xn6 = Convert.ToDecimal(row["f2xn6"]);

            f2xn7 = Convert.ToDecimal(row["f2xn7"]);

            f2xn8 = Convert.ToDecimal(row["f2xn8"]);

            f2xn9 = Convert.ToDecimal(row["f2xn9"]);

            f2xn10 = Convert.ToDecimal(row["f2xn10"]);

            f2xn11 = Convert.ToDecimal(row["f2xn11"]);

            f2xn12 = Convert.ToDecimal(row["f2xn12"]);

            f2xdt1 = Convert.ToDecimal(row["f2xdt1"]);

            f2xdt2 = Convert.ToDecimal(row["f2xdt2"]);

            f2xdt3 = Convert.ToDecimal(row["f2xdt3"]);

            f2xt1 = Convert.ToDecimal(row["f2xt1"]);

            f2xt2 = Convert.ToDecimal(row["f2xt2"]);

            f2xt3 = Convert.ToDecimal(row["f2xt3"]);

            f2xt4 = Convert.ToDecimal(row["f2xt4"]);

            f2xt5 = Convert.ToDecimal(row["f2xt5"]);

            f2xt6 = Convert.ToDecimal(row["f2xt6"]);

        }


        /// <summary>
        /// array contructor
        /// </summary>
        /// <param name="values">construct from ToArray() of self</param>
        public FRP001EX(string[] values)
        {

            f2pro = Convert.ToDecimal(values[0]);

            f2xaid = values[1];

            f2clnt = values[2];

            f2atyp = values[3];

            f2plnt = values[4];

            f2dlva = values[5];

            f2bapd = Convert.ToDecimal(values[6]);

            f2capd = Convert.ToDecimal(values[7]);

            f2argd = values[8];

            f2grct = Convert.ToDecimal(values[9]);

            f2ncrs = values[10];

            f2chg = values[11];

            f23pt = values[12];

            f2spud = Convert.ToDecimal(values[13]);

            f2xs1 = values[14];

            f2xs2 = values[15];

            f2xs3 = values[16];

            f2xs4 = values[17];

            f2xs5 = values[18];

            f2xs6 = values[19];

            f2xs7 = values[20];

            f2xs8 = values[21];

            f2xs9 = values[22];

            f2xa1 = values[23];

            f2xa2 = values[24];

            f2xa3 = values[25];

            f2xa4 = values[26];

            f2xa5 = values[27];

            f2xa6 = values[28];

            f2xa7 = values[29];

            f2xa8 = values[30];

            f2xa9 = values[31];

            f2xn1 = Convert.ToDecimal(values[32]);

            f2xn2 = Convert.ToDecimal(values[33]);

            f2xn3 = Convert.ToDecimal(values[34]);

            f2xn4 = Convert.ToDecimal(values[35]);

            f2xn5 = Convert.ToDecimal(values[36]);

            f2xn6 = Convert.ToDecimal(values[37]);

            f2xn7 = Convert.ToDecimal(values[38]);

            f2xn8 = Convert.ToDecimal(values[39]);

            f2xn9 = Convert.ToDecimal(values[40]);

            f2xn10 = Convert.ToDecimal(values[41]);

            f2xn11 = Convert.ToDecimal(values[42]);

            f2xn12 = Convert.ToDecimal(values[43]);

            f2xdt1 = Convert.ToDecimal(values[44]);

            f2xdt2 = Convert.ToDecimal(values[45]);

            f2xdt3 = Convert.ToDecimal(values[46]);

            f2xt1 = Convert.ToDecimal(values[47]);

            f2xt2 = Convert.ToDecimal(values[48]);

            f2xt3 = Convert.ToDecimal(values[49]);

            f2xt4 = Convert.ToDecimal(values[50]);

            f2xt5 = Convert.ToDecimal(values[51]);

            f2xt6 = Convert.ToDecimal(values[52]);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>an array of the class properties</returns>
        public string[] ToArray()
        {
            return new string[] { f2pro.ToString(), f2xaid, f2clnt, f2atyp, f2plnt, f2dlva, f2bapd.ToString(), f2capd.ToString(), f2argd, f2grct.ToString(), f2ncrs, f2chg, f23pt, f2spud.ToString(), f2xs1, f2xs2, f2xs3, f2xs4, f2xs5, f2xs6, f2xs7, f2xs8, f2xs9, f2xa1, f2xa2, f2xa3, f2xa4, f2xa5, f2xa6, f2xa7, f2xa8, f2xa9, f2xn1.ToString(), f2xn2.ToString(), f2xn3.ToString(), f2xn4.ToString(), f2xn5.ToString(), f2xn6.ToString(), f2xn7.ToString(), f2xn8.ToString(), f2xn9.ToString(), f2xn10.ToString(), f2xn11.ToString(), f2xn12.ToString(), f2xdt1.ToString(), f2xdt2.ToString(), f2xdt3.ToString(), f2xt1.ToString(), f2xt2.ToString(), f2xt3.ToString(), f2xt4.ToString(), f2xt5.ToString(), f2xt6.ToString() };
        }


        #region properties


        // 11.0
        /// <summary>
        /// Pro Number
        /// </summary>
        [Display(Name = "Pro Number")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal f2pro { get; set; }

        // 6
        /// <summary>
        /// XA ID
        /// </summary>
        [Display(Name = "XA ID")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "XA ID cannot be longer than 6 characters.")]
        public string f2xaid { get; set; }

        // 7
        /// <summary>
        /// XA Client
        /// </summary>
        [Display(Name = "XA Client")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "XA Client cannot be longer than 7 characters.")]
        public string f2clnt { get; set; }

        // 2
        /// <summary>
        /// XA Approval Type
        /// </summary>
        [Display(Name = "XA Approval Type")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "XA Approval Type cannot be longer than 2 characters.")]
        public string f2atyp { get; set; }

        // 1
        /// <summary>
        /// Plan Type
        /// </summary>
        [Display(Name = "Plan Type")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Plan Type cannot be longer than 1 characters.")]
        public string f2plnt { get; set; }

        // 1
        /// <summary>
        /// XA Delivery Action ABOW
        /// </summary>
        [Display(Name = "XA Delivery Action ABOW")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "XA Delivery Action ABOW cannot be longer than 1 characters.")]
        public string f2dlva { get; set; }

        // 1.0
        /// <summary>
        /// Bus. Wndw Days Add To Trans
        /// </summary>
        [Display(Name = "Bus. Wndw Days Add To Trans")]
        [DefaultValue(0)]
        [Range(0, 9)]
        public decimal f2bapd { get; set; }

        // 1.0
        /// <summary>
        /// Cal. Wndw Days Add To Trans
        /// </summary>
        [Display(Name = "Cal. Wndw Days Add To Trans")]
        [DefaultValue(0)]
        [Range(0, 9)]
        public decimal f2capd { get; set; }

        // 1
        /// <summary>
        /// XA Arrive Gate Or Dock
        /// </summary>
        [Display(Name = "XA Arrive Gate Or Dock")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "XA Arrive Gate Or Dock cannot be longer than 1 characters.")]
        public string f2argd { get; set; }

        // 4.0
        /// <summary>
        /// XA Appt Grace Time
        /// </summary>
        [Display(Name = "XA Appt Grace Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal f2grct { get; set; }

        // 2
        /// <summary>
        /// XA/RA Noncompliant Reason
        /// </summary>
        [Display(Name = "XA/RA Noncompliant Reason")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "XA/RA Noncompliant Reason cannot be longer than 2 characters.")]
        public string f2ncrs { get; set; }

        // 1
        /// <summary>
        /// XA Charges Apply
        /// </summary>
        [Display(Name = "XA Charges Apply")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "XA Charges Apply cannot be longer than 1 characters.")]
        public string f2chg { get; set; }

        // 7
        /// <summary>
        /// 3Rd Party
        /// </summary>
        [Display(Name = "3Rd Party")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "3Rd Party cannot be longer than 7 characters.")]
        public string f23pt { get; set; }

        // 7.0
        /// <summary>
        /// Suggested PU Date
        /// </summary>
        [Display(Name = "Suggested PU Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal f2spud { get; set; }

        // 1
        /// <summary>
        /// Extra Status 1
        /// </summary>
        [Display(Name = "Extra Status 1")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Extra Status 1 cannot be longer than 1 characters.")]
        public string f2xs1 { get; set; }

        // 1
        /// <summary>
        /// Extra Status 2
        /// </summary>
        [Display(Name = "Extra Status 2")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Extra Status 2 cannot be longer than 1 characters.")]
        public string f2xs2 { get; set; }

        // 1
        /// <summary>
        /// Extra Status 3
        /// </summary>
        [Display(Name = "Extra Status 3")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Extra Status 3 cannot be longer than 1 characters.")]
        public string f2xs3 { get; set; }

        // 1
        /// <summary>
        /// Extra Status 4
        /// </summary>
        [Display(Name = "Extra Status 4")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Extra Status 4 cannot be longer than 1 characters.")]
        public string f2xs4 { get; set; }

        // 1
        /// <summary>
        /// Extra Status 5
        /// </summary>
        [Display(Name = "Extra Status 5")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Extra Status 5 cannot be longer than 1 characters.")]
        public string f2xs5 { get; set; }

        // 1
        /// <summary>
        /// Extra Status 6
        /// </summary>
        [Display(Name = "Extra Status 6")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Extra Status 6 cannot be longer than 1 characters.")]
        public string f2xs6 { get; set; }

        // 3
        /// <summary>
        /// Extra Status 7
        /// </summary>
        [Display(Name = "Extra Status 7")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Extra Status 7 cannot be longer than 3 characters.")]
        public string f2xs7 { get; set; }

        // 3
        /// <summary>
        /// Extra Status 8
        /// </summary>
        [Display(Name = "Extra Status 8")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Extra Status 8 cannot be longer than 3 characters.")]
        public string f2xs8 { get; set; }

        // 3
        /// <summary>
        /// Extra Status 9
        /// </summary>
        [Display(Name = "Extra Status 9")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Extra Status 9 cannot be longer than 3 characters.")]
        public string f2xs9 { get; set; }

        // 7
        /// <summary>
        /// Extra Alpha 1
        /// </summary>
        [Display(Name = "Extra Alpha 1")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Extra Alpha 1 cannot be longer than 7 characters.")]
        public string f2xa1 { get; set; }

        // 7
        /// <summary>
        /// Extra Alpha 2
        /// </summary>
        [Display(Name = "Extra Alpha 2")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Extra Alpha 2 cannot be longer than 7 characters.")]
        public string f2xa2 { get; set; }

        // 7
        /// <summary>
        /// Extra Alpha 3
        /// </summary>
        [Display(Name = "Extra Alpha 3")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Extra Alpha 3 cannot be longer than 7 characters.")]
        public string f2xa3 { get; set; }

        // 10
        /// <summary>
        /// Extra Alpha 4
        /// </summary>
        [Display(Name = "Extra Alpha 4")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Extra Alpha 4 cannot be longer than 10 characters.")]
        public string f2xa4 { get; set; }

        // 10
        /// <summary>
        /// Extra Alpha 5
        /// </summary>
        [Display(Name = "Extra Alpha 5")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Extra Alpha 5 cannot be longer than 10 characters.")]
        public string f2xa5 { get; set; }

        // 15
        /// <summary>
        /// Extra Alpha 6
        /// </summary>
        [Display(Name = "Extra Alpha 6")]
        [DefaultValue("")]
        [StringLength(15, ErrorMessage = "Extra Alpha 6 cannot be longer than 15 characters.")]
        public string f2xa6 { get; set; }

        // 15
        /// <summary>
        /// Extra Alpha 7
        /// </summary>
        [Display(Name = "Extra Alpha 7")]
        [DefaultValue("")]
        [StringLength(15, ErrorMessage = "Extra Alpha 7 cannot be longer than 15 characters.")]
        public string f2xa7 { get; set; }

        // 40
        /// <summary>
        /// Extra Alpha 8
        /// </summary>
        [Display(Name = "Extra Alpha 8")]
        [DefaultValue("")]
        [StringLength(40, ErrorMessage = "Extra Alpha 8 cannot be longer than 40 characters.")]
        public string f2xa8 { get; set; }

        // 40
        /// <summary>
        /// Extra Alpha 9
        /// </summary>
        [Display(Name = "Extra Alpha 9")]
        [DefaultValue("")]
        [StringLength(40, ErrorMessage = "Extra Alpha 9 cannot be longer than 40 characters.")]
        public string f2xa9 { get; set; }

        // 3.0
        /// <summary>
        /// Extra Numeric 1
        /// </summary>
        [Display(Name = "Extra Numeric 1")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal f2xn1 { get; set; }

        // 3.0
        /// <summary>
        /// Extra Numeric 2
        /// </summary>
        [Display(Name = "Extra Numeric 2")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal f2xn2 { get; set; }

        // 3.0
        /// <summary>
        /// Extra Numeric 3
        /// </summary>
        [Display(Name = "Extra Numeric 3")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal f2xn3 { get; set; }

        // 7.0
        /// <summary>
        /// Extra Numeric 4
        /// </summary>
        [Display(Name = "Extra Numeric 4")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal f2xn4 { get; set; }

        // 7.0
        /// <summary>
        /// Extra Numeric 5
        /// </summary>
        [Display(Name = "Extra Numeric 5")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal f2xn5 { get; set; }

        // 9.2
        /// <summary>
        /// Extra Numeric 6
        /// </summary>
        [Display(Name = "Extra Numeric 6")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal f2xn6 { get; set; }

        // 9.2
        /// <summary>
        /// Extra Numeric 7
        /// </summary>
        [Display(Name = "Extra Numeric 7")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal f2xn7 { get; set; }

        // 11.0
        /// <summary>
        /// Extra Numeric 8
        /// </summary>
        [Display(Name = "Extra Numeric 8")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal f2xn8 { get; set; }

        // 11.0
        /// <summary>
        /// Extra Numeric 9
        /// </summary>
        [Display(Name = "Extra Numeric 9")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal f2xn9 { get; set; }

        // 11.2
        /// <summary>
        /// Extra Amount 1
        /// </summary>
        [Display(Name = "Extra Amount 1")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal f2xn10 { get; set; }

        // 11.2
        /// <summary>
        /// Extra Amount 2
        /// </summary>
        [Display(Name = "Extra Amount 2")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal f2xn11 { get; set; }

        // 11.2
        /// <summary>
        /// Extra Amount 3
        /// </summary>
        [Display(Name = "Extra Amount 3")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal f2xn12 { get; set; }

        // 7.0
        /// <summary>
        /// Extra Date 1
        /// </summary>
        [Display(Name = "Extra Date 1")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal f2xdt1 { get; set; }

        // 7.0
        /// <summary>
        /// Extra Date 2
        /// </summary>
        [Display(Name = "Extra Date 2")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal f2xdt2 { get; set; }

        // 7.0
        /// <summary>
        /// Extra Date 3
        /// </summary>
        [Display(Name = "Extra Date 3")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal f2xdt3 { get; set; }

        // 4.0
        /// <summary>
        /// Extra Time 1
        /// </summary>
        [Display(Name = "Extra Time 1")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal f2xt1 { get; set; }

        // 4.0
        /// <summary>
        /// Extra Time 2
        /// </summary>
        [Display(Name = "Extra Time 2")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal f2xt2 { get; set; }

        // 4.0
        /// <summary>
        /// Extra Time 3
        /// </summary>
        [Display(Name = "Extra Time 3")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal f2xt3 { get; set; }

        // 6.0
        /// <summary>
        /// Extra Time 4
        /// </summary>
        [Display(Name = "Extra Time 4")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal f2xt4 { get; set; }

        // 6.0
        /// <summary>
        /// Extra Time 5
        /// </summary>
        [Display(Name = "Extra Time 5")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal f2xt5 { get; set; }

        // 6.0
        /// <summary>
        /// Extra Time 6
        /// </summary>
        [Display(Name = "Extra Time 6")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal f2xt6 { get; set; }



        #endregion

    }
}
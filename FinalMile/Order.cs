﻿using System;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace FinalMile
{
    class Order
    {
        //Weight double
        //DeclaredValue double
        //PodRequired int (0 or 1)
        //Auth string(50)
        //OrderAlias string(50)
        //OtherReference string(50)
        //InvoiceReference string(50)
        //CallerPhone string(50)
        //Description string(50)


        /// <summary>
        /// Notes
        /// </summary>
        /// <remarks>
        /// A full text order level note used for special shipping instructions, often used to list the items on the shipment
        /// </remarks>
        [Display(Name = "Notes")]
        [DefaultValue("")] // string(?)
        public string Notes { get; set; }

        /// <summary>
        /// Service
        /// </summary>
        /// <remarks>
        /// A short code that represents the level of service for the shipment
        /// </remarks>
        [Display(Name = "Service")]
        [DefaultValue("")] // string(10)
        [StringLength(10, ErrorMessage = "Service is 10 characters.")]
        public string Service { get; set; }

        /// <summary>
        /// Customer Code
        /// </summary>
        [Display(Name = "Customer Code")]
        [DefaultValue("")] // string(?)
        public string CustomerCode { get; set; }

        // string(50)
        /// <summary>
        /// Caller
        /// </summary>
        /// <remarks>
        /// The contact name/entity that is responsible for the shipment
        /// </remarks>
        [Display(Name = "Caller")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Caller is 50 characters.")]
        public string Caller { get; set; }

        public OrderStops PickUp { get; set; }

        public OrderStops Delivery { get; set; }

        /// <summary>
        /// Job Sequence
        /// </summary>
        [Display(Name = "Job Sequence")]
        [DefaultValue(0)] // int
        public int JobSequence { get; set; }

        /// <summary>
        /// Pieces
        /// </summary>
        [Display(Name = "Pieces")]
        [DefaultValue(0)] // int
        public OrderPieces Pieces { get; set; }



        /// <summary>
        /// Vehicle Type : set using SetVehicleType()
        /// </summary>
        /// <remarks>
        /// 10/03/2018 : rw
        /// property added after call with eCourier
        /// </remarks>
        [Display(Name = "Vehicle Type")]
        [DefaultValue("")] // string(?)
        public string VehicleTypeID { get; set; }


        /// <summary>
        /// VehicleTypeCodes
        /// </summary>
        /// <remarks>
        /// 10/03/2018 : rw
        /// property added after call with eCourier
        /// </remarks>
        public enum VehicleTypeCodes
        {
            None,
            LiftGate
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 10/03/2018 : rw
        /// property added after call with eCourier
        /// </remarks>
        private string[] vehicleTypeCodesArray = { "", "10" };


        public Order()
        {
            PickUp = new OrderStops("P");

            Delivery = new OrderStops("D");

            Pieces = new OrderPieces();

            JobSequence = 1;

            VehicleTypeID = vehicleTypeCodesArray[0];
        }
                

        public void Add(FRP001 frp001)
        {
            Delivery.Add(frp001);

            Pieces.Add(frp001);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frp002"></param>
        /// <remarks>
        /// 10/03/2018 : rw
        /// adding vehicle type requirement after call with eCourier
        /// </remarks>
        public void Add(List<FRP002> frp002)
        {
            PickUp.Add(frp002);

            Delivery.Add(frp002);

            Pieces.Add(frp002);

            // 10/03/2018 : rw

            foreach (FRP002 rec in frp002)
            {
                if (rec.fdcmcl.Trim().Equals("LGATE"))
                {
                    SetVehicleType(VehicleTypeCodes.LiftGate);

                    break;
                }
            }

            // 10/03/2018 : end
        }

        public void Add(AAP030 aap030)
        {
            PickUp.Add(aap030);
        }

        public void Add(List<FRP003> frp003)
        {
            // Delivery.Add(frp003);
        }

        public void Add(FRP001EX frp001ex)
        {
            Delivery.Add(frp001ex);
        }

        /// <summary>
        /// SetVehicleType
        /// </summary>
        /// <remarks>
        /// 10/03/2018 : rw
        /// function added after call with eCourier
        /// </remarks>
        public void SetVehicleType(VehicleTypeCodes typeCode)
        {
            try
            {
                VehicleTypeID = vehicleTypeCodesArray[(int)typeCode];
            }
            catch
            {
                VehicleTypeID = vehicleTypeCodesArray[0];
            }
        }

        /// <summary>
        /// class data to formatted XML string
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// 10/03/2018 : rw
        /// modified after call with eCourier
        /// </remarks>
        public string ToXML()
        {
            StringBuilder xx = new StringBuilder();

            xx.Append(@"<m:SaveOrder xmlns:m = 'http://www.e-courier.com/schemas/'>");

            xx.AppendFormat(Environment.NewLine);

            // xx.AppendFormat(@"<Order Notes = '{0}' Service = '{1}' CustomerCode = '{2}' Caller = '{3}' >", Notes, Service, CustomerCode, Caller);

            xx.AppendFormat(@"<Order Notes = '{0}' Service = '{1}' CustomerCode = '{2}' Caller = '{3}' VehicleTypeID = '{4}' >", Notes, Service, CustomerCode, Caller, VehicleTypeID);

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"<Stops>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(PickUp.ToXML());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(Delivery.ToXML());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Stops>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"<Jobs>");

            xx.AppendFormat(Environment.NewLine);

            xx.AppendFormat(@"<Job Sequence = '{0}' />", JobSequence.ToString());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Jobs>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"<Pieces>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(Pieces.ToXML());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Pieces>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Order>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</m:SaveOrder>");

            return xx.ToString();
        }


    }
}

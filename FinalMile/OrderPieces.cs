﻿using System;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace FinalMile
{
    class OrderPieces
    {
        /// <summary>
        /// Sequence
        /// </summary>
        [Display(Name = "Sequence")]
        [DefaultValue(0)] // int
        public int Sequence { get; set; }

        /// <summary>
        /// Pieces
        /// </summary>
        [Display(Name = "Pieces")]
        [DefaultValue(0)] // int
        public int Pieces { get; set; }

        /// <summary>
        /// Weight
        /// </summary>
        [Display(Name = "Weight")]
        [DefaultValue(0)] // double
        public double Weight { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [Display(Name = "Name")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Name is 50 characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Container Reference
        /// </summary>
        [Display(Name = "Container Reference")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Container Reference is 50 characters.")]
        public string ContainerReference { get; set; }

        /// <summary>
        /// Reference, aka: Pro Number
        /// </summary>
        [Display(Name = "Reference")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Reference is 50 characters.")]
        public string Reference { get; set; }


        public OrderPieces()
        {
            Sequence = 1;

            Pieces = 0;

            Weight = 0;

            Description = string.Empty;

            ContainerReference = string.Empty;

            Reference = string.Empty;


        }

        public void Add(FRP001 frp001)
        {
            Pieces = (int)Convert.ToInt64(frp001.fhtotp);

            Weight = Convert.ToDouble(frp001.fhswgt);
            
            Reference = Convert.ToInt64(frp001.fhpro).ToString(); // remove the decimals
        }

        public void Add(List<FRP002> frp002)
        {
            try
            {
                foreach(FRP002 f2 in frp002)
                {
                    if (f2.fdwgt > 0)
                    {
                        Description = CleanString(f2.fddes.Trim()); // FRP002.FDDES [where FDWGT > 0] ?

                        break;
                    }
                }
                
            }
            catch 
            {
                Description = string.Empty;
            }
        }

        private string CleanString(string value)
        {
            string VALID_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ";

            string newValue = string.Empty;

            try
            {
                foreach (char c in value)
                {
                    if (VALID_CHARS.Contains(c.ToString()))
                    {
                        newValue += c.ToString();
                    }
                }
            }
            catch
            {
                newValue = value;
            }

            return newValue;
        }

        /// <summary>
        /// class data to formatted XML string
        /// </summary>
        /// <returns></returns>
        public string ToXML()
        {
            StringBuilder xx = new StringBuilder();

            xx.AppendFormat(@"<Piece Sequence = '{0}' Pieces = '{1}' Weight = '{2}'  Description = '{3}'  ContainerReference = '{4}'  Reference = '{5}' />",
                            Sequence.ToString(), 
                            Pieces.ToString(), 
                            Weight.ToString(), 
                            Description, 
                            ContainerReference, 
                            Reference);

            return xx.ToString();
        }

    }
}

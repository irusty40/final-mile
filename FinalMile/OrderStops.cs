﻿using System;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace FinalMile
{
    class OrderStops
    {
        
        /// <summary>
        /// Sequence
        /// </summary>
        [Display(Name = "Sequence")]
        [DefaultValue(0)] // int
        public int Sequence { get; set; }

        /// <summary>
        /// Stop Type
        /// </summary>
        [Display(Name = "Stop Type")]
        [DefaultValue("")] // string(1)
        [StringLength(1, ErrorMessage = "Stop Type is 1 character.")]
        public string StopType { get; set; }

        /// <summary>
        /// Notes
        /// </summary>
        [Display(Name = "Notes")]
        [DefaultValue("")] // string(?)
        public string Note { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [Display(Name = "Name")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Name is 50 characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        [Display(Name = "Address")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Address is 50 characters.")]
        public string Address { get; set; }

        /// <summary>
        /// Room
        /// </summary>
        [Display(Name = "Room")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Room is 50 characters.")]
        public string Room { get; set; }


        /// <summary>
        /// City
        /// </summary>
        [Display(Name = "City")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "City is 50 characters.")]
        public string City { get; set; }

        /// <summary>
        /// State
        /// </summary>
        [Display(Name = "State")]
        [DefaultValue("")] // string(2)
        [StringLength(2, ErrorMessage = "State is 2 characters.")]
        public string State { get; set; }

        /// <summary>
        /// Zip
        /// </summary>
        /// <remarks>
        /// format: 99999
        /// or
        /// format: 99999-9999
        /// </remarks>
        [Display(Name = "Zip")]
        [DefaultValue("")] // string(20)
        [StringLength(20, ErrorMessage = "Zip is 20 characters.")]
        public string Zip { get; set; }


        /// <summary>
        /// See - aka: Contact
        /// </summary>
        [Display(Name = "See")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "See is 50 characters.")]
        public string See { get; set; }

        /// <summary>
        /// Phone
        /// </summary>
        /// <remarks>
        /// standard format: 999-999-9999
        /// </remarks>
        [Display(Name = "Phone")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Phone is 50 characters.")]
        public string Phone { get; set; }

        /// <summary>
        /// Early Date Time
        /// </summary>
        /// <remarks>
        /// standard format: mm/dd/yyyy HH:mm
        /// </remarks>
        [Display(Name = "Early Date Time")]
        [DefaultValue("")] // date
        public string EarlyDateTime { get; set; }

        /// <summary>
        /// Scheduled Date Time
        /// </summary>
        /// <remarks>
        /// standard format: mm/dd/yyyy HH:mm
        /// </remarks>
        [Display(Name = "Scheduled Date Time")]
        [DefaultValue("")] // date
        public string ScheduledDateTime { get; set; }

        /// <summary>
        /// Late Date Time
        /// </summary>
        /// <remarks>
        /// standard format: mm/dd/yyyy HH:mm
        /// </remarks>
        [Display(Name = "Late Date Time")]
        [DefaultValue("")] // date
        public string LateDateTime { get; set; }

        /// <summary>
        /// Stop Piece Sequence
        /// </summary>
        [Display(Name = "Stop Piece Sequence")]
        [DefaultValue(0)] // int
        public int StopPieceSequence { get; set; }

        /// <summary>
        /// Stop Piece Action
        /// </summary>
        [Display(Name = "Stop Piece Action")]
        [DefaultValue("")] // string(1)
        [StringLength(1, ErrorMessage = "Stop Piece Action is 1 character.")]
        public string StopPieceAction { get; set; }


        


        public OrderStops(string stopType)
        {
            StopPieceSequence = 1;

            StopType = stopType;

            StopPieceAction = StopType;

            Sequence = 1;

            if (StopType.Equals("D")) Sequence = 2;

            Note = string.Empty;

            

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="frp001"></param>
        /// <remarks>
        /// FRP001 data is only relevant to the Delivery/Consignee
        /// </remarks>
        public void Add(FRP001 frp001)
        {
            if (StopType.Equals("D"))
            {
                Name = CleanString(frp001.fhcnm.Trim());

                Address = CleanString(frp001.fhca1.Trim());

                Room = CleanString(frp001.fhca2.Trim());

                City = CleanString(frp001.fhcct.Trim());

                State = frp001.fhcst.Trim();

                Zip = frp001.fhczip.Trim();

                EarlyDateTime = string.Empty;

                // FRP001EX.F2XN4 or FRP001.FHDADT + FRP001.FHDATM
                // ScheduledDateTime is set here, but will be overwritten if a value exists in FRP001EX.F2XN4

                ScheduledDateTime = string.Empty;

                if (Convert.ToInt64(frp001.fhdadt) > 1180000) ScheduledDateTime = ConvertDate(Convert.ToInt64(frp001.fhdadt).ToString(), Convert.ToInt64(frp001.fhdatm).ToString().PadLeft(4, '0') + "00").ToString("MM/dd/yyyy");

                LateDateTime = string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aap030"></param>
        /// <remarks>
        /// AAP030 data is only relevant to the Pick-up which is the terminal in this context
        /// </remarks>
        public void Add(AAP030 aap030)
        {
            Name = string.Format("Saia - {0}", CleanString(aap030.ct1nam.Trim()));

            Address = CleanString(aap030.ct1ad1.Trim());

            Room = string.Empty;

            City = CleanString(aap030.ct1ct.Trim());

            State = aap030.ct1st.Trim();

            Zip = aap030.ct1zp.Trim();

            Phone = FormatPhone(aap030.ct1lt);

            See = string.Empty;

            // if run date is a weekend, move the pick-up data to Monday

            DateTime procDate = DateTime.Today;

            if (procDate.DayOfWeek == DayOfWeek.Saturday) procDate = procDate.AddDays(1);

            if (procDate.DayOfWeek == DayOfWeek.Sunday) procDate = procDate.AddDays(1);

            // EarlyDateTime = ConvertDate(aap030.ct1wot.ToString(), "").ToString("MM/dd/yyyy");

            EarlyDateTime = ConvertDate("1" + procDate.ToString("yyMMdd"), Convert.ToInt64(aap030.ct1wot).ToString().PadLeft(4, '0') + "00").ToString("MM/dd/yyyy HH:mm");

            ScheduledDateTime = string.Empty;

            // LateDateTime = ConvertDate(aap030.ct1puc.ToString(), "").ToString("MM/dd/yyyy");

            LateDateTime = ConvertDate("1" + procDate.ToString("yyMMdd"), Convert.ToInt64(aap030.ct1puc).ToString().PadLeft(4, '0') + "00").ToString("MM/dd/yyyy HH:mm");

        }

        public void Add(List<FRP002> frp002)
        {
            if (StopType.Equals("P"))
            {
                foreach (FRP002 rec in frp002)
                {
                    if (rec.fdcmcl.Trim().Equals("LGATE"))
                    {
                        Note = "Liftgate Required (FRP002.FDDES FDCMCL = LGATE)"; // FRP002.FDDES[where FDCMCL = LGATE]
                        
                        break;
                    }
                }
            }
            else
            {
                // FDDES may contain:
                // 479 372 2770                       
                // JAMES CHANEY 254 366 5300          
                // 214 797 2057  ERIC TEAGUE        
                // 361-221-0448                       
                // ???

                // dump all to See and try to parse out a valid phone number
                  
                foreach (FRP002 rec in frp002)
                {
                    if (rec.fdcmcl.Trim().Equals("CCPHON"))
                    {
                        See = rec.fddes.Trim(); // set to raw data

                        // Phone = FRP002.FDDES pull out numeric values only

                        Phone = string.Empty;

                        foreach (char c in See)
                        {
                            if (char.IsDigit(c)) Phone += c;
                        }

                        // only try to format phone is value is numeric

                        if ((!string.IsNullOrEmpty(Phone.Trim()) && (Microsoft.VisualBasic.Information.IsNumeric(Phone))))
                        {
                            Phone = FormatPhone(Convert.ToDecimal(Phone));
                        }

                        break;
                    }
                }
                
            }
        }

        public void Add(FRP003 frp003)
        {
            // tbd ? - 10/01/2018 : rw - not needed in this context at this time
        }

        public void Add(FRP001EX frp001ex)
        {
            if (StopType.Equals("D"))
            {
                if (frp001ex.f2xn4 > 0)
                {
                    ScheduledDateTime = ConvertDate(Convert.ToInt64(frp001ex.f2xn4).ToString(), "").ToString("MM/dd/yyyy");
                }
            }
        }


        private DateTime ConvertDate(string cyymmdd, string hhmmss = "")
        {
            DateTime convDate = DateTime.Now;

            string convString = "{0}/{1}/{2} ";

            try
            {
                if (hhmmss.Equals("240000")) hhmmss = "000000";

                if ((string.IsNullOrEmpty(cyymmdd)) || (Convert.ToDecimal(cyymmdd) == 0))
                {
                    convDate = DateTime.Now;
                }
                else
                {
                    cyymmdd = cyymmdd.PadLeft(7, '0');

                    convString = string.Format(convString, cyymmdd.Substring(3, 2), cyymmdd.Substring(5, 2), cyymmdd.Substring(1, 2));

                    if (!string.IsNullOrEmpty(hhmmss.Trim()))
                    {
                        hhmmss = hhmmss.PadLeft(6, '0');

                        convString += string.Format("{0}:{1}:{2}", hhmmss.Substring(0, 2), hhmmss.Substring(2, 2), hhmmss.Substring(4, 2));
                    }

                    convDate = Convert.ToDateTime(convString);
                }
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("Input: cyymmdd = [{0}],  hhmmss = [{1}],  convString = [{2}]", cyymmdd, hhmmss, convString);

                Errors.ReportException(ex);

                convDate = DateTime.Now;

                // convDate = DateTime.MinValue;
            }

            return convDate;
        }

        private string FormatPhone(decimal phone)
        {
            string fmt = string.Empty;

            try
            {
                if (phone > 0)
                {
                    fmt = Convert.ToInt64(phone).ToString().PadLeft(11, '0').Substring(1, 10);

                    fmt = fmt.Insert(3, "-").Insert(7, "-");
                }
            }
            catch
            {
                fmt = phone.ToString();
            }

            return fmt;
        }

        

        private string CleanString(string value)
        {
            string VALID_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 ";

            string newValue = string.Empty;

            try
            {
                foreach(char c in value)
                {
                    if (VALID_CHARS.Contains(c.ToString()))
                    {
                        newValue += c.ToString();
                    }
                }
            }
            catch 
            {
                newValue = value;
            }

            return newValue;
        }

        /// <summary>
        /// class data to formatted XML string
        /// </summary>
        /// <returns></returns>
        public string ToXML()
        {

            StopPieceAction = StopType;

            StringBuilder xx = new StringBuilder();
            xx.AppendFormat(@"<Stop Sequence = '{0}' StopType = '{1}' Note = '{2}' Name = '{3}' Address = '{4}' Room = '{5}' City = '{6}' State = '{7}' Zip = '{8}' Phone = '{9}' See = '{10}' EarlyDateTime = '{11}' ScheduledDateTime = '{12}' LateDateTime = '{13}' >",
                            Sequence.ToString(),
                            StopType,
                            Note,
                            Name,
                            Address,
                            Room,
                            City,
                            State,
                            Zip,
                            Phone,
                            See,
                            EarlyDateTime,
                            ScheduledDateTime,
                            LateDateTime);

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"<OrderStopPieces>");

            xx.AppendFormat(Environment.NewLine);

            xx.AppendFormat(@"<OrderStopPiece Sequence = '{0}' PieceAction = '{1}' />", 
                            StopPieceSequence, 
                            StopPieceAction);

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</OrderStopPieces>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Stop>");

            return xx.ToString();
        }
    }
}

DSR #5502 - Final Mile, September 2018, Russell Williams

Note: this file is embedded in the Properties/Resources.
Edit this inside the source and recompile to keep this display up to date.

All data is handled by FinalMileAPI. App.config "api_base" contains api url

App.config "taskCount" determines how many threads/processes are used.
Development showed 50 pros would process in 3 seconds with a single thread.
Unit testing was done with 1 and 3 threads to ensure function.

COMMAND LINE ARGS: 
FinalMile.exe			<-- run in manual mode for today's date
FinalMile.exe auto		<-- run in auto mode from job scheduler for today's date. auto could be replaced with any non-numeric value
FinalMile.exe 1180926	<-- run in auto or manual for the date specified

Current files used by this application:
FRP001
FRP001EX
FRP002
FRP003
AAP030
FRP022
INPLOG

FRP022 will be inserted + update with eCourier transmission details
FRP001.FHXT and FRP001FHXTB will be updated via FRP022 API with order number and scac code

INPLOG will contain logging data for history, errors and general information

Local log files are stored inside this applications directory in a "logs" directory.
Each run will create a sub-directory inside logs with a date name format: CYYMMDD
App.config "logAgeInDays" determines how long to keep log data for this app.

All API data is stored in JSON format inside that days process directory so we may
have a record of what Saia data was actually retrieved at the time of processing.
Same for XML data transmitted/received to/from eCourier.

App.config "waitBeforeShutdown" determines how long the app will remain open after processing


INPLOG info
GLDATE	= date of entry (CST)
GLTIME	= time of entry (CST)
GLKEY	= primary key [FINALMILE]
GLAK1	= alpha key 1 [API or APP]
GLAK2	= alpha key 2 [INFO or ERROR]
GLAD1	= file or function name
GLAD3	= pronumber
GLND1	= record count return from query
GLNOTE	= process information or exception details
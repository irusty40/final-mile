﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Diagnostics;
using Microsoft.VisualBasic;
using System.Xml;
using System.Net;
using System.Xml.Linq;
using System.Linq;


namespace FinalMile
{
    public partial class frmMain : Form
    {

        List<FRP001> frp001 = null;

        List<AAP030> aap030 = null;

        string terminals = string.Empty;

        string processDate = string.Empty;

        string logDirectory = string.Empty;

        string mainLogPath = "app.txt";

        string errorLogPath = "error.txt";

        const string processLogPath = "process.{0}.txt";

        const string processErrorPath = "errors.{0}.txt";


        Task[] processTasks = null;

        string[] processNotes = null;

        DateTime shutdown;

        public frmMain()
        {
            InitializeComponent();            
        }




        private void Form1_Load(object sender, EventArgs e)
        {



            using (System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage("noreply@saia.com", "camatlga@bellsouth.net"))
            {
                mail.CC.Add("cmckinnish@saia.com");

                mail.Subject = "Testing External Mail Sending";

                mail.IsBodyHtml = true;

                mail.Body = "Test content";

                using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("smtp.saia.com"))
                {
                    smtp.Send(mail);
                }
            }


            lblHeader.Text = "loading... ";

            txtData.Text = string.Empty;

            txtProcess.Text = string.Empty;

            lblConnection.Text = string.Format("Connection: {0}", Properties.Settings.Default.api_base);

            CreateINPLOG("INFO", "", "", 0, string.Format("App start @ {0} server time.", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff")));

            txtInfo.Text = Properties.Resources.ProgramInfo;


            if ((Logging.RUN_MODE != null) && (!string.IsNullOrEmpty(Logging.RUN_MODE.Trim())))
            {
                if (Information.IsNumeric(Logging.RUN_MODE))
                {
                    processDate = Logging.RUN_MODE;

                    AddProcessText("LOAD", string.Format("app start = {0} > {1}", "AUTO", processDate));
                }
                else
                {
                    processDate = "1" + DateTime.Today.ToString("yyMMdd");

                    AddProcessText("LOAD", string.Format("app start = {0} > {1}", Logging.RUN_MODE, processDate));
                }
                
            }
            else
            {
                //processDate = "1" + DateTime.Today.ToString("yyMMdd");

                processDate = "1180901";

                AddProcessText("LOAD", "app start = manual");
            }

            AddProcessText("LOAD", "Process Date: " + processDate);

            logDirectory = Path.Combine(Environment.CurrentDirectory, "logs");

            if (!Directory.Exists(logDirectory)) Directory.CreateDirectory(logDirectory);

            logDirectory = Path.Combine(logDirectory, processDate);

            if (!Directory.Exists(logDirectory)) Directory.CreateDirectory(logDirectory);

            mainLogPath = Path.Combine(logDirectory, mainLogPath);

            AddProcessText("LOAD", "Main Log Path: " + mainLogPath);

            errorLogPath = Path.Combine(logDirectory, errorLogPath);

            AddProcessText("LOAD", "Error Log Path: " + errorLogPath);
                        
            processTasks = new Task[Properties.Settings.Default.taskCount];

            processNotes = new string[Properties.Settings.Default.taskCount];
                        

            AddProcessText("LOAD", "processing tasks = " + Properties.Settings.Default.taskCount.ToString());
            

            tmrMain.Enabled = true;

        }

        private void AddProcessText(string key, string value)
        {
            txtProcess.Text += string.Format("{0}: {1}", DateTime.Now.ToString("HH:mm:ss.fff"), value) + Environment.NewLine;

            Logging.LogEntry(mainLogPath, key, value);

            txtProcess.SelectionStart = txtProcess.Text.Length;

            txtProcess.ScrollToCaret();

            Application.DoEvents();
        }

        private async void tmrMain_Tick(object sender, EventArgs e)
        {

            try
            {
                tmrMain.Enabled = false;

                frp001 = await GetFRP001Data();

                if (frp001 != null)
                {
                    AddProcessText("START", string.Format("FRP001 API returned {0} residential delivery pros", frp001.Count.ToString()));

                    CreateINPLOG("INFO", "", "", 0, string.Format("FRP001 API returned {0} residential delivery pros.", frp001.Count.ToString()));

                    terminals = string.Empty;

                    foreach (FRP001 f in frp001)
                    {
                        if (!terminals.Contains(f.fhdt.Trim()))
                        {
                            if (!string.IsNullOrEmpty(terminals.Trim())) terminals += ",";

                            terminals += f.fhdt.Trim();
                        }
                    }

                    AddProcessText("START", string.Format("RESDEL Terminals: {0}", terminals));

                    dataGridView1.DataSource = frp001;

                    aap030 = await GetAAP030Data(terminals);

                    if (aap030 != null)
                    {
                        MainProcess();
                    }
                    else
                    {
                        throw new Exception("Processing stopped - unable to retreive AAP030 terminal data. "); 
                    }
                }
                else
                {
                    AddProcessText("START", "FRP001 API returned 0 residential delivery pros");

                    CreateINPLOG("INFO", "", "", 0, "FRP001 API returned 0 residential delivery pros");

                    AddProcessText("START", "Processing complete!");

                    tmrUpdateDisplay.Enabled = true;

                    shutdown = DateTime.Now.AddMinutes(Properties.Settings.Default.waitBeforeShutdown);

                    AddProcessText("START", string.Format("Application will exit at {0}.", shutdown.ToString("HH:mm")));

                    tmrShutdown.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                Logging.LogError(errorLogPath, ex);

                Errors.ReportException(ex);

                CreateINPLOG("ERROR", "", "tmrMain", 0, ex.Message);

                AddProcessText("START EX", "Processing stopped due to exception: " + ex.Message);

                AddProcessText("START EX", "");

                shutdown = DateTime.Now.AddMinutes(Properties.Settings.Default.waitBeforeShutdown);

                AddProcessText("START EX", string.Format("Application will exit at {0}.", shutdown.ToString("HH:mm")));

                tmrShutdown.Enabled = true;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 09/25/2018 : rw
        /// </remarks>
        void MainProcess()
        {
            bool assigned = false;

            Stopwatch swt = new Stopwatch();

            AddProcessText("MAIN", "Begin pro assignment to threads. ");

            swt.Start();

            Stopwatch sw = new Stopwatch();

            int[] ctr = { 0, frp001.Count };

            foreach (FRP001 pro in frp001)
            {
                ctr[0] += 1;

                lblHeader.Text = string.Format("[{0}/{1}] processing pro #{2}", ctr[0].ToString(), ctr[1].ToString(), Convert.ToInt64(pro.fhpro).ToString());

                lblHeader.Refresh();

                assigned = false;

                sw.Reset();

                sw.Start();

                while (!assigned)
                {
                    for (int taskIndex = 0; taskIndex < processTasks.Length; taskIndex++)
                    {
                        if (processTasks[taskIndex] == null)
                        {
                            assigned = true;

                            sw.Stop();

                            AddProcessText("MAIN", string.Format("#{0} assigned to task #{1}. Waited {2} seconds. ", Convert.ToInt64(pro.fhpro).ToString().PadLeft(11, '0'),  taskIndex.ToString(), (sw.ElapsedMilliseconds / 1000).ToString()));
                            
                            processTasks[taskIndex] = Task.Factory.StartNew(() => RunProcess(pro, taskIndex));

                            break;
                        }
                    }

                    if (!assigned) // wait for a task to complete
                    {
                        processTasks[Task.WaitAny(processTasks)] = null;
                    }
                }
            }

            AddProcessText("MAIN", "All pros have been assigned. ");

            swt.Stop();

            AddProcessText("MAIN", string.Format("Processing complete! Run time was {0} seconds.", (swt.ElapsedMilliseconds / 1000).ToString()));

            tmrUpdateDisplay.Enabled = true;

            shutdown = DateTime.Now.AddMinutes(Properties.Settings.Default.waitBeforeShutdown);

            AddProcessText("MAIN", string.Format("Application will exit at {0}.", shutdown.ToString("HH:mm")));


            lblHeader.Text = "complete!";

            lblHeader.Refresh();


            tmrShutdown.Enabled = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pro">FRP001</param>
        /// <param name="index">taskIndex</param>
        /// <remarks>
        /// 09/25/2018 : rw
        /// 
        /// </remarks>
        async void RunProcess(FRP001 pro, int index)
        {
            Envelope envelope = null;

            string pronumber = Convert.ToInt64(pro.fhpro).ToString();

            string soapResult = string.Empty;

            string soapResultPath = string.Empty;

            string orderNumber = string.Empty;

            List<FRP022> frp022 = null;

            FRP022 f22 = null;

            List<FRP003> frp003 = null;

            string FFSTS = "NOTICE";

            bool send = true; // send initial NOTICE to eCourier

            try
            {

                CreateINPLOG("INFO", Convert.ToInt64(pro.fhpro).ToString(), "PRO", 0, string.Format("Begin processing pro @ {0}.", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff")));

                processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "") + Environment.NewLine;

                List<FRP002> frp002 = await GetFRP002Data(pronumber, index);

                processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "FRP002") + Environment.NewLine;

                List<FRP001EX> frp001ex = await GetFRP001EXData(pronumber, index);

                processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "FRP001EX") + Environment.NewLine;

                envelope = new Envelope(Properties.Settings.Default.userGUID);



                envelope.SaveOrder.Service = Properties.Settings.Default.service;

                envelope.SaveOrder.CustomerCode = Properties.Settings.Default.customerCode;


                processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "Envelope") + Environment.NewLine;

                envelope.Add(pro);

                processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "Envelope.Add(FRP001)") + Environment.NewLine;

                envelope.Add(aap030);

                processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "Envelope.Add(AAP030)") + Environment.NewLine;

                if ((frp002 != null) && (frp001ex != null))
                {
                    envelope.Add(frp002);

                    processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "Envelope.Add(FRP002)") + Environment.NewLine;

                    envelope.Add(frp001ex[0]);

                    processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "Envelope.Add(FRP001EX)") + Environment.NewLine;
                }

                // save xml

                string xmlDirectory = Path.Combine(logDirectory, "XML");

                if (!Directory.Exists(xmlDirectory)) Directory.CreateDirectory(xmlDirectory);

                using (StreamWriter wtr = new StreamWriter(Path.Combine(xmlDirectory, pronumber + ".xml"), false))
                {
                    wtr.Write(envelope.ToXML());
                }

                processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "Save XML") + Environment.NewLine;


                // check existence in FRP022

                frp022 = await GetFRP022Data(pronumber, index);

                // if found and date changed, 

                if ((frp022 != null) && (frp022.Count > 0))
                {
                    send = false; // because an FRP022 record already exists - at least a NOTICE was sent
                                       

                    DateTime sdd = Convert.ToDateTime(envelope.SaveOrder.Delivery.ScheduledDateTime);

                    DateTime cdd = ConvertDate(Convert.ToInt64(frp022[0].fflrdt).ToString(), "");

                    if (sdd.Subtract(cdd).TotalDays != 0)
                    {
                        // the date has changed

                        send = true; // because an UPDATE exists and we need to transmit a new date to eCourier

                        FFSTS = "UPDATE";

                        f22.fflrdt = Convert.ToDecimal("1" + cdd.ToString("yyMMdd"));

                        CreateINPLOG("INFO", Convert.ToInt64(pro.fhpro).ToString(), "UPDATE", 0, string.Format("Scheduled delivery date changed from {0} to {1}.", cdd.ToString("MM/dd/yyyy"), sdd.ToString("MM/dd/yyyy")));

                    }

                    
                }



                // pull FRP003 where FCCOD = 'DT'

                frp003 = await GetFRP003Data(pronumber, index);

                // if found update FFSTS = "READY"

                if ((frp003 != null) && (frp003.Count > 0))
                {
                    send = true; // always send when READY at destination terminal

                    FFSTS = "READY";

                    CreateINPLOG("INFO", Convert.ToInt64(pro.fhpro).ToString(), "READY", frp003.Count, "FRP003 query for DT returned a record.");
                }



                // 10/03/2018 : rw
                // don't re-transmit to ecourier if status is not update or ready.
                // after sending, other api's will update FRP022 status codes 
                // so we only deal with what we know

                if ((send) && ((frp022 != null) && (frp022.Count > 0)))
                {
                    if (!"NOTICE,UPDATE,READY".Contains(frp022[0].ffsts.Trim()))
                    {
                        send = false;

                        CreateINPLOG("INFO", Convert.ToInt64(pro.fhpro).ToString(), "NOTSENT", 0, string.Format("Not sent due to current FRP022.FFSTS = {0} ", frp022[0].ffsts.Trim()));

                        return;
                    }

                }

                // 10/03/2018 : rw



                if ((send) && (Properties.Settings.Default.sendToeCourier))
                {
                    // send xml

                    HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(Properties.Settings.Default.eCourierEndpoint);

                    webReq.Headers.Add(@"SOAP:Action");

                    webReq.ContentType = "text/xml;charset=\"utf-8\"";

                    webReq.Accept = "text/xml";

                    webReq.Method = "POST";

                    XmlDocument soapEnvelopeXml = new XmlDocument();

                    soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>" + envelope.ToXML());

                    using (Stream stream = webReq.GetRequestStream())
                    {
                        soapEnvelopeXml.Save(stream);
                    }

                    // get and save response

                    soapResultPath = Path.Combine(xmlDirectory, pronumber + ".response.xml");

                    using (WebResponse response = webReq.GetResponse())
                    {
                        using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                        {
                            soapResult = rd.ReadToEnd();

                            using (StreamWriter sw = new StreamWriter(soapResultPath, false, Encoding.UTF8))
                            {
                                sw.Write(soapResult);
                            }


                        }
                    }

                    // pull order from response

                    // 10/03/2018 : rw
                    // occasionally, this code fails to get the OrderNumber from the XML for valid, success responses.
                    // all i can see that might be causing is the above save to disk may not be finished saving before
                    // the access is requested - so, i'm enclosing in a while with a try/catch with a small delay
                    // to check again before accepting as a failure.

                    //XDocument docx = XDocument.Load(soapResultPath);

                    //string name = docx.Descendants("Order").First().Value;

                    //IEnumerable<XElement> responses = docx.Descendants("Order");

                    //foreach (XElement response in responses)
                    //{
                    //    orderNumber = XElement.Parse(response.ToString()).Attribute("OrderNumber").Value;
                    //}

                    int retries = 0;

                    bool excThrown = false;

                    while ((retries < 3) && (string.IsNullOrEmpty(orderNumber.Trim())))
                    {
                        retries++;

                        excThrown = false;

                        // try to read the OrderNumber from the response XML

                        try
                        {
                            XDocument docx = XDocument.Load(soapResultPath);

                            string name = docx.Descendants("Order").First().Value;

                            IEnumerable<XElement> responses = docx.Descendants("Order");

                            foreach (XElement response in responses)
                            {
                                orderNumber = XElement.Parse(response.ToString()).Attribute("OrderNumber").Value;
                            }
                        }
                        catch
                        {                            
                            excThrown = true;
                        }

                        // try to read a faultcode from the response XML
                        // added 10/04/2018 : rw

                        try
                        {
                            // if faultcode is not found, just let it throw an exception and loop again

                            XDocument docx = XDocument.Load(soapResultPath);

                            string faultcode = docx.Descendants("faultcode").First().Value;

                            string faultstring = docx.Descendants("faultstring").First().Value;

                            string faultactor = docx.Descendants("faultactor").First().Value;

                            // arrived here -
                            // eCourier has give and error response
                            // attach all XML documents to email and send details to programmer

                            orderNumber = string.Empty; // <-- ensures an exception is thrown and logged to INPLOG

                            Errors.ReportException(faultcode, faultstring, faultactor, Path.Combine(xmlDirectory, pronumber + ".xml"), soapResultPath);
                            
                            retries += 10; // ensure we kick out of the while loop

                            break;

                        }
                        catch
                        {
                            excThrown = true; 
                        }

                        if (excThrown) System.Threading.Thread.Sleep(450); // wait a moment for the file to complete saving b4 retry

                    } // end while

                    if (string.IsNullOrEmpty(orderNumber.Trim())) throw new Exception("Failed to retrieve OrderNumber from eCourier SOAP Response.");

                    // 10/03/2018 : end

                    processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber, "order#: " + orderNumber) + Environment.NewLine;


                    // orderNumber = "1010101"; // debug

                    // create the FRP022 entry we need to insert/update in our system

                    f22 = new FRP022();

                    f22.ffpro = Convert.ToDecimal(pronumber);

                    f22.ffodr = Convert.ToDecimal(orderNumber);

                    if (!string.IsNullOrEmpty(envelope.SaveOrder.Delivery.ScheduledDateTime))
                    {
                        f22.fflrdt = Convert.ToDecimal("1" + Convert.ToDateTime(envelope.SaveOrder.Delivery.ScheduledDateTime).ToString("yyMMdd"));
                    }

                                        
                    // finish adding data to FRP022

                    f22.ffsts = FFSTS;
                    
                    f22.fflsts = DateAndTime.Now.AddHours(Properties.Settings.Default.timeOffset).ToString("yyyy-MM-dd-HH.mm.ss.fff000");

                    if ((frp022 != null) && (frp022.Count > 0))
                    {
                        f22.ffadd = frp022[0].fflsts;

                        f22.ffimg = frp022[0].fflsts;
                    }
                    else
                    {
                        f22.ffadd = f22.fflsts;

                        f22.ffimg = f22.fflsts;
                    }

                    // post frp022 data, post orderNumber
                    // note: this post inserts/update FRP022
                    // and updates FRP001 FHXT and FHXTB
                    
                    PostFRP022(f22);


                }
                else
                {
                    CreateINPLOG("INFO", Convert.ToInt64(pro.fhpro).ToString(), "NOTSENT", 0, string.Format("App.Config setting sendToeCourier = {0} or send = {1}", Properties.Settings.Default.sendToeCourier.ToString(), send.ToString()));
                }
            }
            catch (Exception ex)
            {
                CreateINPLOG("ERROR", Convert.ToInt64(pro.fhpro).ToString(), "Process", 0, ex.Message);

                processNotes[index] += string.Format("[{0}] pro #{1} {2} ", DateTime.Now.ToString("HH:mm:ss.fff"), pronumber,ex.Message) + Environment.NewLine;

                Errors.ReportException(ex);
            }

        }

        // select * from frp001 where fhdt in ('MIA') and fhpudt = 1180919 and fhpro in (select f2.fdpro from frp002 f2 where f2.fdcmcl = 'RESDEL') 

        /// <summary>
        /// gets the list of resdel pros for today (date) from api
        /// </summary>
        /// <returns></returns>
        /// <remarks>
        /// 09/24/2018 : rw - created
        /// 
        /// </remarks>
        Task<List<FRP001>> GetFRP001Data()
        {
            List<FRP001> f = null;

            string url = string.Empty;

            Stopwatch sw = new Stopwatch();

            try
            {
                url = Properties.Settings.Default.api_base + string.Format(Properties.Settings.Default.api_frp001, processDate);

                AddProcessText("FRP001", "URL = " + url);

                sw.Start();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;

                        f = JsonConvert.DeserializeObject<List<FRP001>>(result);

                        using (StreamWriter file = File.CreateText(Path.Combine(logDirectory, "frp001.json")))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                        
                            serializer.Serialize(file, f);
                        }
                    }
                    else
                    {
                        var errJson = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        Logging.LogError(errorLogPath, errJson);

                        Errors.ReportException(JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("API FRP001: {0}", url);

                CreateINPLOG("ERROR", "", "getFRP001", 0, ex.Message);

                Logging.LogError(errorLogPath, ex);

                Errors.ReportException(ex);
            }
            finally
            {
                sw.Stop();

                AddProcessText("FRP001", "Query duration (ms): " + sw.ElapsedMilliseconds.ToString());

            }

            return Task.FromResult(f);
        }

        Task<List<AAP030>> GetAAP030Data(string terminalList)
        {
            List<AAP030> tids = null;

            string url = string.Empty;

            Stopwatch sw = new Stopwatch();

            try
            {
                url = Properties.Settings.Default.api_base + string.Format(Properties.Settings.Default.api_aap030, terminalList);

                AddProcessText("AAP030", "URL = " + url);

                sw.Start();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;

                        tids = JsonConvert.DeserializeObject<List<AAP030>>(result);

                        using (StreamWriter file = File.CreateText(Path.Combine(logDirectory, "AAP030.json")))
                        {
                            JsonSerializer serializer = new JsonSerializer();

                            serializer.Serialize(file, tids);
                        }
                    }
                    else
                    {
                        var errJson = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        Logging.LogError(errorLogPath, errJson);

                        Errors.ReportException(JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("API AAP030: {0}", url);

                CreateINPLOG("ERROR", "", "getAAP030", 0, ex.Message);

                Logging.LogError(errorLogPath, ex);

                Errors.ReportException(ex);
            }
            finally
            {
                sw.Stop();

                AddProcessText("AAP030", "Query duration (ms): " + sw.ElapsedMilliseconds.ToString());

            }

            return Task.FromResult(tids);
        }

        Task<List<FRP002>> GetFRP002Data(string pro, int index)
        {
            List<FRP002> f = null;

            string url = string.Empty;

            Stopwatch sw = new Stopwatch();

            string[] paths = new string [] { Path.Combine(logDirectory, string.Format(processLogPath, index.ToString())),
                                             Path.Combine(logDirectory, string.Format(processErrorPath, index.ToString())) };

            try
            {
                url = Properties.Settings.Default.api_base + string.Format(Properties.Settings.Default.api_frp002, pro);

                Logging.LogEntry(paths[0], "FRP002", "URL = " + url);

                sw.Start();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;

                        f = JsonConvert.DeserializeObject<List<FRP002>>(result);

                        string jsonDirectory = Path.Combine(logDirectory, "JSON");

                        if (!Directory.Exists(jsonDirectory)) Directory.CreateDirectory(jsonDirectory);

                        using (StreamWriter file = File.CreateText(Path.Combine(jsonDirectory, pro + ".frp002.json")))
                        {
                            JsonSerializer serializer = new JsonSerializer();

                            serializer.Serialize(file, f);
                        }
                    }
                    else
                    {
                        var errJson = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        Logging.LogError(paths[1], JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));

                        Errors.ReportException(JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));
                    }
                }
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("API FRP002: {0}", url);

                CreateINPLOG("ERROR", "", "getFRP002", 0, ex.Message);

                Logging.LogError(paths[1], ex);

                Errors.ReportException(ex);
            }
            finally
            {
                sw.Stop();

                Logging.LogEntry(paths[0], "FRP002", "Query duration (ms): " + sw.ElapsedMilliseconds.ToString());

            }

            return Task.FromResult(f);
        }

        Task<List<FRP003>> GetFRP003Data(string pro, int index)
        {
            List<FRP003> f = null;

            string url = string.Empty;

            Stopwatch sw = new Stopwatch();

            string[] paths = new string[] { Path.Combine(logDirectory, string.Format(processLogPath, index.ToString())),
                                             Path.Combine(logDirectory, string.Format(processErrorPath, index.ToString())) };

            try
            {
                url = Properties.Settings.Default.api_base + string.Format(Properties.Settings.Default.api_frp003, pro);

                Logging.LogEntry(paths[0], "FRP003", "URL = " + url);

                sw.Start();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;

                        f = JsonConvert.DeserializeObject<List<FRP003>>(result);

                        string jsonDirectory = Path.Combine(logDirectory, "JSON");

                        if (!Directory.Exists(jsonDirectory)) Directory.CreateDirectory(jsonDirectory);

                        using (StreamWriter file = File.CreateText(Path.Combine(jsonDirectory, pro + ".frp002.json")))
                        {
                            JsonSerializer serializer = new JsonSerializer();

                            serializer.Serialize(file, f);
                        }
                    }
                    //else // we really don't need to know about this...
                    //{
                    //    var errJson = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                    //    Logging.LogError(paths[1], JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));

                    //    Errors.ReportException(JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("API FRP003: {0}", url);

                CreateINPLOG("ERROR", "", "getFRP003", 0, ex.Message);

                Logging.LogError(paths[1], ex);

                Errors.ReportException(ex);
            }
            finally
            {
                sw.Stop();

                Logging.LogEntry(paths[0], "FRP003", "Query duration (ms): " + sw.ElapsedMilliseconds.ToString());

            }

            return Task.FromResult(f);
        }

        Task<List<FRP001EX>> GetFRP001EXData(string pro, int index)
        {
            List<FRP001EX> f = null;

            string url = string.Empty;

            Stopwatch sw = new Stopwatch();

            string[] paths = new string[] { Path.Combine(logDirectory, string.Format(processLogPath, index.ToString())),
                                            Path.Combine(logDirectory, string.Format(processErrorPath, index.ToString())) };

            try
            {
                url = Properties.Settings.Default.api_base + string.Format(Properties.Settings.Default.api_frp001ex, pro);

                Logging.LogEntry(paths[0], "FRP001EX", "URL = " + url);

                sw.Start();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;

                        f = JsonConvert.DeserializeObject<List<FRP001EX>>(result);

                        string jsonDirectory = Path.Combine(logDirectory, "JSON");

                        if (!Directory.Exists(jsonDirectory)) Directory.CreateDirectory(jsonDirectory);

                        using (StreamWriter file = File.CreateText(Path.Combine(jsonDirectory, pro + ".frp001ex.json")))
                        {
                            JsonSerializer serializer = new JsonSerializer();

                            serializer.Serialize(file, f);
                        }
                    }
                    //else // don't report this as not all pros have an FRP001EX record
                    //{
                    //    var errJson = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                    //    //Logging.LogError(paths[1], JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));

                    //    Errors.ReportException(JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("API FRP001EX: {0}", url);

                CreateINPLOG("ERROR", "", "getFRP001x", 0, ex.Message);

                Logging.LogError(paths[1], ex);

                Errors.ReportException(ex);
            }
            finally
            {
                sw.Stop();

                Logging.LogEntry(paths[0], "FRP001EX", "Query duration (ms): " + sw.ElapsedMilliseconds.ToString());

            }

            return Task.FromResult(f);
        }

        Task<List<FRP022>> GetFRP022Data(string pro, int index)
        {
            List<FRP022> f = null;

            string url = string.Empty;

            Stopwatch sw = new Stopwatch();

            string[] paths = new string[] { Path.Combine(logDirectory, string.Format(processLogPath, index.ToString())),
                                            Path.Combine(logDirectory, string.Format(processErrorPath, index.ToString())) };

            try
            {
                url = Properties.Settings.Default.api_base + string.Format(Properties.Settings.Default.api_frp022, pro);

                Logging.LogEntry(paths[0], "FRP022", "URL = " + url);

                sw.Start();

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(url).Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;

                        f = JsonConvert.DeserializeObject<List<FRP022>>(result);

                        string jsonDirectory = Path.Combine(logDirectory, "JSON");

                        if (!Directory.Exists(jsonDirectory)) Directory.CreateDirectory(jsonDirectory);

                        using (StreamWriter file = File.CreateText(Path.Combine(jsonDirectory, pro + ".frp022.json")))
                        {
                            JsonSerializer serializer = new JsonSerializer();

                            serializer.Serialize(file, f);
                        }
                    }
                    //else
                    //{
                    //    var errJson = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                    //    //Logging.LogError(paths[1], JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));

                    //    Errors.ReportException(JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));
                    //}
                }
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("API FRP022: {0}", url);

                CreateINPLOG("ERROR", "", "getFRP022", 0, ex.Message);

                Logging.LogError(paths[1], ex);

                Errors.ReportException(ex);
            }
            finally
            {
                sw.Stop();

                Logging.LogEntry(paths[0], "FRP022", "Query duration (ms): " + sw.ElapsedMilliseconds.ToString());

            }

            return Task.FromResult(f);
        }

        private void tmrUpdateDisplay_Tick(object sender, EventArgs e)
        {
            try
            {
                tmrUpdateDisplay.Enabled = false;

                foreach(string note in processNotes)
                {
                    txtData.Text += note;

                    txtData.SelectionStart = txtData.Text.Length;

                    txtData.ScrollToCaret();

                    Application.DoEvents();
                }

            }
            catch { }
        }

        private void ClearLogging()
        {
            List<string> fileList = new List<string>();

            List<string> dirList = new List<string>();

            DirectoryInfo di = null;

            string value = string.Empty;

            try
            {
                logDirectory = Path.Combine(Environment.CurrentDirectory, "logs");

                foreach (string dir in Directory.GetDirectories(logDirectory))
                {
                    di = new DirectoryInfo(dir);

                    if ((Information.IsNumeric(di.Name)) && (ConvertDate(di.Name).Subtract(DateTime.Now).TotalDays < 0))
                    {
                        foreach(string file in Directory.GetFiles(dir))
                        {
                            fileList.Add(file);
                        }

                        foreach (string subdir in Directory.GetDirectories(dir))
                        {
                            dirList.Add(subdir);

                            foreach (string file in Directory.GetFiles(subdir))
                            {
                                fileList.Add(file);
                            }

                        }

                        dirList.Add(dir); // add outer directory last
                    }
                }

                // delete all files

                foreach (string file in fileList)
                {
                    File.Delete(file);
                }

                // delete all directories

                foreach(string dir in dirList)
                {
                    Directory.Delete(dir);
                }

            }
            catch
            {

            }
            finally
            {
                fileList = null;

                dirList = null;

                di = null;

            }

        }

        private DateTime ConvertDate(string cyymmdd, string hhmmss = "")
        {
            DateTime convDate = DateTime.Now;

            string convString = "{0}/{1}/{2} ";

            try
            {
                if (hhmmss.Equals("240000")) hhmmss = "000000";

                if ((string.IsNullOrEmpty(cyymmdd)) || (Convert.ToDecimal(cyymmdd) == 0))
                {
                    convDate = DateTime.Now;
                }
                else
                {
                    cyymmdd = cyymmdd.PadLeft(7, '0');

                    convString = string.Format(convString, cyymmdd.Substring(3, 2), cyymmdd.Substring(5, 2), cyymmdd.Substring(1, 2));

                    if (!string.IsNullOrEmpty(hhmmss.Trim()))
                    {
                        hhmmss = hhmmss.PadLeft(6, '0');

                        convString += string.Format("{0}:{1}:{2}", hhmmss.Substring(0, 2), hhmmss.Substring(2, 2), hhmmss.Substring(4, 2));
                    }
                    convDate = Convert.ToDateTime(convString);
                }
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("Input: [{0}, {1}] {2}", cyymmdd, hhmmss, convString);

                CreateINPLOG("ERROR", "", "ConvDate", 0, ex.Message);

                Errors.ReportException(ex);

                convDate = DateTime.Now;

                // convDate = DateTime.MinValue;
            }

            return convDate;
        }

        private void tmrShutdown_Tick(object sender, EventArgs e)
        {
            try
            {
                if (shutdown.Subtract(DateTime.Now).TotalMinutes < 0)
                {
                    CreateINPLOG("INFO", "", "", 0, string.Format("App shutdown @ {0} server time.", DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss.fff")));

                    ClearLogging();

                    Application.Exit();
                }
            }
            catch
            {
                Application.Exit();
            }
        }

        private void PostFRP022(FRP022 frp022)
        {
            string url = string.Empty;

            try
            {
                if (frp022 == null) return;

                url = Properties.Settings.Default.api_base + Properties.Settings.Default.api_frp022;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var content = new StringContent(JsonConvert.SerializeObject(frp022), Encoding.UTF8, "application/json");

                    var response = client.PostAsync(url, content).Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        var errJson = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        Logging.LogError(errorLogPath, errJson);

                        Errors.ReportException(JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));
                    }
                }
            }
            catch { }
        }

        private void PostINPLOG(INPLOG log)
        {
            string url = string.Empty;

            try
            {
                if (log == null) return;

                url = Properties.Settings.Default.api_base + Properties.Settings.Default.api_inplog;

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();

                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var content = new StringContent(JsonConvert.SerializeObject(log), Encoding.UTF8, "application/json");

                    var response = client.PostAsync(url, content).Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        var errJson = JsonConvert.DeserializeObject<dynamic>(response.Content.ReadAsStringAsync().Result);

                        Logging.LogError(errorLogPath, errJson);

                        Errors.ReportException(JsonConvert.SerializeObject(errJson, Newtonsoft.Json.Formatting.Indented));
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="glak2">INFO or ERROR</param>
        /// <param name="glad3">pronumber</param>
        /// <param name="glad1">file or function name</param>
        /// <param name="glnd1">record count</param>
        /// <param name="glnote">information or exception details</param>
        private void CreateINPLOG(string glak2, string glad3, string glad1, decimal glnd1, string glnote)
        {
            INPLOG log = new INPLOG("FINALMILE", "APP", glak2);

            try
            {
                log.AlphaData3 = glad3;

                log.AlphaData1 = glad1;

                log.NumericData1 = glnd1;

                log.Notes = glnote;
                                
                PostINPLOG(log);
            }
            catch { }

        }










    }
}


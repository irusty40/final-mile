﻿using System;
using System.Data;
using IBM.Data.DB2.iSeries;

namespace FinalMile
{
    /// <summary>
    /// iSeries Connections
    /// </summary>
    public static class iSeries
    {

        public static DataTable CallStoredProcedureDataTable(string procedureName, ref string[] values, ref Exception exc, ref string jobNumber)
        {
            string places = string.Empty;

            DataTable data_table = null;

            string cmdText = string.Empty;

            try
            {
                exc = null;

                for (Int16 k = 0; k < values.Length; k++)
                {
                    if (!string.IsNullOrEmpty(places)) places += ",";

                    places += "?";

                }

                using (iDB2Connection conn = new iDB2Connection(Properties.Settings.Default.idb2connectionstring))
                {
                    conn.Open();

                    jobNumber = conn.JobName;

                    using (iDB2Command _command = new iDB2Command())
                    {
                        _command.Connection = conn;

                        cmdText = string.Format("CALL QGPL/{0}({1})", procedureName, places);

                        _command.CommandText = cmdText;

                        iDB2CommandBuilder.DeriveParameters(_command);

                        for (Int16 k = 0; k < _command.Parameters.Count; k++)
                        {
                            _command.Parameters[k].Value = values[k];
                        }

                        using (iDB2DataAdapter _adapter = new iDB2DataAdapter(_command))
                        {
                            data_table = new DataTable(procedureName);

                            _adapter.Fill(data_table);
                        }

                        for (Int16 k = 0; k < _command.Parameters.Count; k++)
                        {
                            if (_command.Parameters[k].Direction != ParameterDirection.Input) values[k] = _command.Parameters[k].Value.ToString();
                        }

                    }
                }
                

            }
            catch (iDB2Exception ix)
            {
                ix.HelpLink = string.Format("[job: {0}] CommandText: {1}", jobNumber, cmdText);

                exc = ix;
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("[job: {0}] CommandText: {1}", jobNumber, cmdText);

                exc = ex;
            }

            return data_table;
        }

        /*
        public iDB2ParameterCollection CallStoredProcedureParameter(string procedureName, ref string[] values, ref Exception exc)
        {
            string places = string.Empty;

            iDB2Command _command = null;

            string cmdText = string.Empty;

            try
            {
                exc = null;

                _procedureName = procedureName;

                _values = values;

                for (Int16 k = 0; k < values.Length; k++)
                {
                    if (!string.IsNullOrEmpty(places)) places += ",";

                    places += "?";

                }

                if ((_connection == null) || (_connection.State != ConnectionState.Open))
                {
                    _connection = new iDB2Connection(string.Format(Properties.Settings.Default.connection_string, _uid, _pwd));

                    _connection.Open();

                    LogConnectionOpen();
                }

                jobNumber = _connection.JobName.Split('/')[0];

                _command = new iDB2Command();

                _command.Connection = _connection;

                _command.CommandTimeout = timeout;

                cmdText = string.Format("CALL QGPL/{0}({1})", _procedureName, places);

                _command.CommandText = cmdText;

                as400Log.Write(string.Format("[{0}]  {1}", jobNumber, _command.CommandText));

                iDB2CommandBuilder.DeriveParameters(_command);

                for (Int16 k = 0; k < _command.Parameters.Count; k++)
                {
                    _command.Parameters[k].Value = _values[k];
                }

                _command.ExecuteNonQuery();

                for (Int16 k = 0; k < _command.Parameters.Count; k++)
                {
                    if (_command.Parameters[k].Direction != ParameterDirection.Input) _values[k] = _command.Parameters[k].Value.ToString();
                }

            }
            catch (iDB2Exception ix)
            {
                ix.HelpLink = string.Format("[job: {0}] CommandText: {1}", jobNumber, cmdText);

                exc = ix;

                _message = ix.StackTrace;

                as400Log.Write(ix);
            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("[job: {0}] CommandText: {1}", jobNumber, cmdText);

                exc = ex;

                _message = ex.StackTrace;

                as400Log.Write(ex);
            }

            return _command.Parameters;
        }
        */

        /// <summary>
        /// Get a table of data via SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable GetData(string sql)
        {
            DataTable dt = new DataTable();

            string job = string.Empty;

            try
            {
                using (iDB2Connection conn = new iDB2Connection(Properties.Settings.Default.idb2connectionstring))
                {
                    conn.Open();

                    job = conn.JobName;

                    using (iDB2DataAdapter da = new iDB2DataAdapter(sql, conn))
                    {
                        da.Fill(dt);
                    }
                }
            }
            catch (iDB2Exception ix)
            {
                ix.HelpLink = sql;

            }
            catch (Exception ex)
            {
                ex.HelpLink = sql;
            }

            return dt;
        }
        

        /// <summary>
        /// Insert or Update via SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="anyEx"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string sql, ref Exception anyEx)
        {
            int rows = 0;

            string job = string.Empty;
            
            try
            {
                using (iDB2Connection conn = new iDB2Connection(Properties.Settings.Default.idb2connectionstring))
                {
                    conn.Open();

                    job = conn.JobName;

                    using (iDB2Command cmd = new iDB2Command(sql, conn))
                    {
                        rows = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (iDB2Exception ix)
            {
                ix.HelpLink = sql;

                anyEx = ix;
            }
            catch (Exception ex)
            {
                ex.HelpLink = sql;

                anyEx = ex;
            }

            return rows;
        }



    }
}
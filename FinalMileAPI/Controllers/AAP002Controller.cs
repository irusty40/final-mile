﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Data;
using FinalMileAPI.Models;


namespace FinalMileAPI.Controllers
{
    public class AAP002Controller : ApiController
    {
        // GET: api/AAP002/TBDKEY
        public IHttpActionResult Get(string id)
        {
            List<AAP002> items = new List<AAP002>();

            string sql = string.Empty;

            try
            {
                sql = string.Format(AAP002.SQL_SELECT_TBDKEY, id.Trim());

                DataTable dt = iSeries.GetData(sql);

                foreach (DataRow row in dt.Rows)
                {
                    items.Add(new AAP002(row));
                }

                if ((items == null) || (items.Count == 0))
                { 
                    Logging.CreateINPLOG("INFO", "AAP002", 0, "input = " + id);

                    return Content(HttpStatusCode.BadRequest, new Exception(string.Format("Key '{0}' was not found or is an valid key field. [AAP002]", id.ToString())));
                }

                Logging.CreateINPLOG("INFO", "AAP002", items.Count, "input = " + id);

                return Json(items);
            }
            catch (Exception ex)
            {
                ex.HelpLink = Request.RequestUri.ToString();

                Logging.CreateINPLOG("ERROR", "AAP002", 0, ex.Message);

                iSeries.ReportError(ex, "", new string[] { id.ToString() });

                return Content(HttpStatusCode.BadRequest, ex);
            }

        }


        // GET: api/AAP002/TBDKEY?=TBDAK1
        public IHttpActionResult Get(string id, string tbdak1)
        {
            List<AAP002> items = new List<AAP002>();

            string sql = string.Empty;

            try
            {
                sql = string.Format(AAP002.SQL_SELECT_TBDKEY_TBDAK1, id.Trim(), tbdak1.Trim());

                DataTable dt = iSeries.GetData(sql);

                foreach (DataRow row in dt.Rows)
                {
                    items.Add(new AAP002(row));
                }

                if ((items == null) || (items.Count == 0))
                {
                    Logging.CreateINPLOG("INFO", "AAP002", 0, "input = " + id);

                    return Content(HttpStatusCode.BadRequest, new Exception(string.Format("Key '{0}' was not found or is an valid key field. [AAP002]", id.ToString())));
                }

                Logging.CreateINPLOG("INFO", "AAP002", items.Count, "input = " + id);

                return Json(items);
            }
            catch (Exception ex)
            {
                Logging.CreateINPLOG("ERROR", "AAP002", 0, ex.Message);

                iSeries.ReportError(ex, "", new string[] { id.ToString() });

                return Content(HttpStatusCode.BadRequest, ex);
            }

        }

        //// POST: api/AAP002
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/AAP002/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/AAP002/5
        //public void Delete(int id)
        //{
        //}
    }
}

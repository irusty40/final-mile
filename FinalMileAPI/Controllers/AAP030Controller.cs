﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Data;
using FinalMileAPI.Models;
using System.Net.Http;

namespace FinalMileAPI.Controllers
{
    public class AAP030Controller : ApiController
    {
        // GET: api/AAP030/TID
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">comma separated list of terminals</param>
        /// <returns></returns>
        public IHttpActionResult Get(string id)
        {
            List<AAP030> items = new List<AAP030>();

            string sql = string.Empty;

            string[] tids = null;

            string value = string.Empty;

            try
            {
                tids = id.Split(',');
                
                foreach (string tid in tids)
                {
                    if (!string.IsNullOrEmpty(value.Trim())) value += ", ";

                    value += string.Format("'{0}'", tid.Trim());
                }

                sql = string.Format(AAP030.SQL_SELECT_CT1TID, value);


                DataTable dt = iSeries.GetData(sql);

                foreach (DataRow row in dt.Rows)
                {
                    items.Add(new AAP030(row));
                }

                if ((items == null) || (items.Count == 0))
                {
                    Logging.CreateINPLOG("INFO", "AAP030", 0, "input = " + id);

                    return Content(HttpStatusCode.BadRequest, new Exception(string.Format("[{0}] were not found in the terminal control file. [AAP030]", value)));
                }

                Logging.CreateINPLOG("INFO", "AAP030", items.Count, "input = " + id);

                return Json(items);
            }
            catch (Exception ex)
            {
                ex.HelpLink = Request.RequestUri.ToString();

                Logging.CreateINPLOG("ERROR", "AAP030", 0, ex.Message);

                iSeries.ReportError(ex, "", new string[] { id.ToString() });

                return new System.Web.Http.Results.ResponseMessageResult(Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex.Message)));
            }

        }

        //// POST: api/AAP030
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/AAP030/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/AAP030/5
        //public void Delete(int id)
        //{
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Data;
using FinalMileAPI.Models;
using System.Configuration;
using System.Net.Http;

namespace FinalMileAPI.Controllers
{
    public class FRP001Controller : ApiController
    {
        
        // GET: api/FRP001/1180924?terminals=MIA,TPA
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">cyyMMdd</param>
        /// <returns></returns>
        public IHttpActionResult Get(string id)
        {
            List<FRP001> items = new List<FRP001>();

            string sql = string.Empty;

            try
            {
                sql = FRP001.SQL_SELECT + string.Format(ConfigurationManager.AppSettings["frp001date"], id.ToString());
                
                DataTable dt = iSeries.GetData(sql);

                foreach (DataRow row in dt.Rows)
                {
                    items.Add(new FRP001(row));
                }

                if ((items == null) || (items.Count == 0))
                {
                    Logging.CreateINPLOG("INFO", "FRP001", 0, "input = " + id);

                    return Content(HttpStatusCode.BadRequest, new Exception(string.Format("No data found for {0}. [FRP001]", id.ToString())));
                }

                Logging.CreateINPLOG("INFO", "FRP001", items.Count, "input = " + id);

                return Json(items);
            }
            catch (Exception ex)
            {
                ex.HelpLink = Request.RequestUri.ToString();

                Logging.CreateINPLOG("ERROR", "FRP001", 0, ex.Message);

                iSeries.ReportError(ex, "", new string[] { id.ToString() });

                return new System.Web.Http.Results.ResponseMessageResult(Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex.Message)));
            }

        }

               


        //// POST: api/FRP001
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/FRP001/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/FRP001/5
        //public void Delete(int id)
        //{
        //}
    }
}

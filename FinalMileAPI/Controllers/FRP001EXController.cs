﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Data;
using FinalMileAPI.Models;

namespace FinalMileAPI.Controllers
{
    public class FRP001EXController : ApiController
    {
        // GET: api/FRP001EX/pro#
        public IHttpActionResult Get(string id)
        {
            List<FRP001EX> items = new List<FRP001EX>();

            string sql = string.Empty;

            try
            {
                sql = string.Format(FRP001EX.SQL_SELECT_F2PRO, id.Trim());

                DataTable dt = iSeries.GetData(sql);

                foreach (DataRow row in dt.Rows)
                {
                    items.Add(new FRP001EX(row));
                }

                if ((items == null) || (items.Count == 0))
                {
                    Logging.CreateINPLOG("INFO", "FRP001EX", 0, "input = " + id, id);

                    return Content(HttpStatusCode.BadRequest, new Exception(string.Format("Pro #{0} was not found or is an valid pro number. [FRP001EX]", id.ToString())));
                }

                Logging.CreateINPLOG("INFO", "FRP001EX", items.Count, "input = " + id, id);

                return Json(items);
            }
            catch (Exception ex)
            {
                ex.HelpLink = Request.RequestUri.ToString();

                Logging.CreateINPLOG("ERROR", "FRP001EX", 0, ex.Message, id);

                iSeries.ReportError(ex, "", new string[] { id.ToString() });

                return Content(HttpStatusCode.BadRequest, ex);
            }

        }

        //// POST: api/FRP001EX
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/FRP001EX/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/FRP001EX/5
        //public void Delete(int id)
        //{
        //}
    }
}

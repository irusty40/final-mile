﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Data;
using FinalMileAPI.Models;

namespace FinalMileAPI.Controllers
{
    public class FRP002Controller : ApiController
    {
        // GET: api/FRP002/pro#
        public IHttpActionResult Get(string id)
        {
            List<FRP002> items = new List<FRP002>();

            string sql = string.Empty;

            try
            {
                sql = string.Format(FRP002.SQL_SELECT_FDPRO, id.Trim());

                DataTable dt = iSeries.GetData(sql);

                foreach (DataRow row in dt.Rows)
                {
                    items.Add(new FRP002(row));
                }

                if ((items == null) || (items.Count == 0))
                {
                    Logging.CreateINPLOG("INFO", "FRP002", 0, "input = " + id, id);

                    return Content(HttpStatusCode.BadRequest, new Exception(string.Format("Pro #{0} was not found or is an valid pro number. [FRP002]", id.ToString())));
                }

                Logging.CreateINPLOG("INFO", "FRP002", items.Count, "input = " + id, id);

                return Json(items);
            }
            catch (Exception ex)
            {
                ex.HelpLink = Request.RequestUri.ToString();

                Logging.CreateINPLOG("ERROR", "FRP002", 0, ex.Message, id);

                iSeries.ReportError(ex, "", new string[] { id.ToString() });

                return Content(HttpStatusCode.BadRequest, ex);
            }

        }

        //// POST: api/FRP002
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT: api/FRP002/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/FRP002/5
        //public void Delete(int id)
        //{
        //}
    }
}

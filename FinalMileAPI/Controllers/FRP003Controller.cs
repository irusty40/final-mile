﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Data;
using FinalMileAPI.Models;

namespace FinalMileAPI.Controllers
{
    public class FRP003Controller : ApiController
    {
        // GET: api/FRP003/pro#
        public IHttpActionResult Get(string id)
        {
            List<FRP003> items = new List<FRP003>();

            string sql = string.Empty;

            try
            {
                sql = string.Format(FRP003.SQL_SELECT_DT, id.Trim());

                DataTable dt = iSeries.GetData(sql);

                foreach (DataRow row in dt.Rows)
                {
                    items.Add(new FRP003(row));
                }

                if ((items == null) || (items.Count == 0))
                {
                    Logging.CreateINPLOG("INFO", "FRP003", 0, "input = " + id, id);

                    return Content(HttpStatusCode.BadRequest, new Exception(string.Format("DT record for pro #{0} was not found or is an valid pro number. [FRP003]", id.ToString())));
                }

                Logging.CreateINPLOG("INFO", "FRP003", items.Count, "input = " + id, id);

                return Json(items);
            }
            catch (Exception ex)
            {
                ex.HelpLink = Request.RequestUri.ToString();

                Logging.CreateINPLOG("ERROR", "FRP003", 0, ex.Message, id);

                iSeries.ReportError(ex, "", new string[] { id.ToString() });

                return Content(HttpStatusCode.BadRequest, ex);
            }

        }

        // GET: api/FRP003/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/FRP003
        //public void Post([FromBody]string value)
        //{
        //}

        // PUT: api/FRP003/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE: api/FRP003/5
        //public void Delete(int id)
        //{
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Data;
using FinalMileAPI.Models;
using System.Configuration;
using System.Net.Http;
using Newtonsoft.Json;

namespace FinalMileAPI.Controllers
{
    public class FRP022Controller : ApiController
    {

        // GET: api/FRP022/pro#
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">pronumber</param>
        /// <returns></returns>
        public IHttpActionResult Get(string id)
        {
            List<FRP022> items = new List<FRP022>();

            string sql = string.Empty;

            try
            {
                sql = string.Format(FRP022.SQL_SELECT, id);

                DataTable dt = iSeries.GetData(sql);

                foreach (DataRow row in dt.Rows)
                {
                    items.Add(new FRP022(row));
                }

                if ((items == null) || (items.Count == 0))
                {
                    Logging.CreateINPLOG("INFO", "FRP022", 0, "input = " + id);

                    return Content(HttpStatusCode.BadRequest, new Exception(string.Format("No data found for {0}. [FRP022]", id.ToString())));
                }

                Logging.CreateINPLOG("INFO", "FRP022", items.Count, "input = " + id);

                return Json(items);
            }
            catch (Exception ex)
            {
                ex.HelpLink = Request.RequestUri.ToString();

                Logging.CreateINPLOG("ERROR", "FRP022", 0, ex.Message);

                iSeries.ReportError(ex, "", new string[] { id.ToString() });

                return new System.Web.Http.Results.ResponseMessageResult(Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError(ex.Message)));
            }

        }

        // POST: api/FRP022/[frp022]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dFrp022"></param>
        /// <remarks>
        /// 
        /// </remarks>
        [HttpPost]
        public void Post(dynamic dFrp022)
        {
            FRP022 frp022 = null;

            string sql = string.Empty;

            string scac = string.Empty;

            Exception exx = null;

            DataTable dt = null;

            DataTable dtFRP001 = null;

            string notes = string.Empty;

            try
            {

                frp022 = JsonConvert.DeserializeObject<FRP022>(JsonConvert.SerializeObject(dFrp022)); // ? this is weird...

                // get current FRP001 values and save to log

                sql = FRP001.SQL_SELECT + string.Format(" where fhpro = {0} ", frp022.ffpro.ToString());

                dtFRP001 = iSeries.GetData(sql);

                if ((dtFRP001 != null) && (dtFRP001.Rows.Count > 0))
                {
                    notes = string.Format("pre-update values: fhxt = [{0}], fhxtb = [{1}]", dtFRP001.Rows[0]["fhxt"].ToString(), dtFRP001.Rows[0]["fhxtb"].ToString());

                    Logging.CreateINPLOG("INFO", "FRP001", 0, notes, frp022.ffpro.ToString());
                }

                // check if a record already exists in FRP022

                sql = string.Format(FRP022.SQL_SELECT, frp022.ffpro.ToString());

                dt = iSeries.GetData(sql);

                if ((dt != null) && (dt.Rows.Count > 0)) // update FRP022
                {
                    sql = string.Format(FRP022.SQL_UPDATE, frp022.ToArray());

                    if (iSeries.ExecuteNonQuery(sql, ref exx) <= 0) throw new Exception("Failed to update to FRP022.", exx);

                }
                else // insert FRP022
                {
                    sql = string.Format(FRP022.SQL_INSERT, frp022.ToArray());

                    if (iSeries.ExecuteNonQuery(sql, ref exx) <= 0) throw new Exception("Failed to insert to FRP022.", exx);

                }

                // update scac and order number in FRP001

                scac = GetScacCode();

                sql = string.Format(FRP001.SQL_UPDATE_ORDERNUMBER, Convert.ToInt64(frp022.ffpro).ToString(), Convert.ToInt64(frp022.ffodr).ToString(), scac);

                if (iSeries.ExecuteNonQuery(sql, ref exx) <= 0) throw new Exception("Failed to update to FRP001.", exx);

                // log frp001 update

                notes = string.Format("post-update values: fhxt = [{0}], fhxtb = [{1}]", scac, frp022.ffodr);

                Logging.CreateINPLOG("INFO", "FRP001", 0, notes, frp022.ffpro.ToString());

            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("SQL: {0}", sql);

                Logging.CreateINPLOG("ERROR", "FRP022", 0, ex.Message);
                
                iSeries.ReportError(ex, "", frp022.ToArray());
            }

        }


        /*
        

        // POST: api/FRP022/[frp022]
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dFrp022"></param>
        /// <remarks>
        /// 
        /// </remarks>
        [HttpPost]
        public void Post(dynamic dFrp022)
        {
            FRP022 frp022 = null;

            string sql = string.Empty;

            string scac = string.Empty;

            Exception exx = null;

            DataTable dt = null;

            DataTable dtFRP001 = null;

            //DataTable dtFRP001EX = null;

            string notes = string.Empty;

            try
            {

                frp022 = JsonConvert.DeserializeObject<INPLOG>(JsonConvert.SerializeObject(dFrp022)); // ? this is weird...

                // get current FRP001 values and save to log

                sql = string.Format(FRP001.SQL_SELECT, string.Format(" where fhpro = {0} ", frp022.ffpro.ToString()));

                dtFRP001 = iSeries.GetData(sql);

                if ((dtFRP001 != null) && (dtFRP001.Rows.Count > 0))
                {
                    notes = string.Format("pre-update values: fhxt = [{0}], fhxtb = [{1}]", dtFRP001.Rows[0]["fhxt"].ToString(), dtFRP001.Rows[0]["fhxtb"].ToString());

                    Logging.CreateINPLOG("INFO", "FRP001", 0, notes, frp022.ffpro.ToString());
                }

                // set default status for insert

                frp022.ffsts = "NOTICE"; 

                // check if pro is at destination terminal to set FRP022.FFSTS

                sql = string.Format(FRP003.SQL_SELECT_DT, frp022.ffpro.ToString()); // select * from frp003 where fccpro  fccod = 'DT'

                dt = iSeries.GetData(sql);

                // if found in FRP003, set ready for delivery

                if ((dt != null) && (dt.Rows.Count > 0)) frp022.ffsts = "READY"; 

                // check if a record already exists in FRP022

                sql = string.Format(FRP022.SQL_SELECT, frp022.ffpro.ToString());

                dt = iSeries.GetData(sql);

                if ((dt != null) && (dt.Rows.Count > 0)) // update 
                {
                    // check if the scheduled delivery date has changed against FRP001 or FRP001EX






                    if (frp022.ffsts.Equals("READY"))
                    {
                        Logging.CreateINPLOG("INFO", "FRP022", 0, "", frp022.ffpro.ToString());
                    }
                    else
                    {
                        frp022.ffsts = "UPDATE";
                    }

                    //sql = string.Format(FRP022.SQL_UPDATE, frp022.ToArray());

                    if (iSeries.ExecuteNonQuery(sql, ref exx) <= 0) throw new Exception("Failed to update to FRP022.", exx);

                }
                else // insert 
                {
                    sql = string.Format(FRP022.SQL_INSERT, frp022.ToArray());

                    if (iSeries.ExecuteNonQuery(sql, ref exx) <= 0) throw new Exception("Failed to insert to FRP022.", exx);

                }
                                
                // update scac and order number in FRP001

                sql = string.Format(FRP001.SQL_UPDATE_ORDERNUMBER, frp022.ffpro, frp022.ffodr, scac);

                if (iSeries.ExecuteNonQuery(sql, ref exx) <= 0) throw new Exception("Failed to update to FRP001.", exx);

                // log frp001 update

                notes = string.Format("post-update values: fhxt = [{0}], fhxtb = [{1}]", scac, frp022.ffodr);

                Logging.CreateINPLOG("INFO", "FRP001", 0, notes, frp022.ffpro.ToString());

            }
            catch (Exception ex)
            {
                ex.HelpLink = string.Format("SQL: {0}", sql);

                Logging.CreateINPLOG("ERROR", "FRP022", 0, ex.Message);
                
                iSeries.ReportError(ex, "", frp022.ToArray());

                // iSeries.ReportError(ex, "", new string[] { id.ToString() });

            }

        }

        */


        /// <summary>
        /// return a preset scac code - alternate between a list
        /// </summary>
        /// <returns>
        /// </returns>
        private string GetScacCode()
        {
            string[] values = null;

            int index = 0;

            int nextIndex = 0;

            string scacPath = string.Empty;

            try
            {
                values = ConfigurationManager.AppSettings["scac_codes"].Split(',');

                scacPath = System.IO.Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath("~/Text/"), "scac.txt");

                if (System.IO.File.Exists(scacPath))
                {
                    using (System.IO.StreamReader rdr = new System.IO.StreamReader(scacPath))
                    {
                        index = Convert.ToInt32(rdr.ReadLine());
                    }
                }

                nextIndex = index + 1;

                if (nextIndex >= values.Length) nextIndex = 0;

                using (System.IO.StreamWriter wtr = new System.IO.StreamWriter(scacPath, false))
                {
                    wtr.WriteLine(nextIndex.ToString());
                }

            }
            catch // (Exception ex)
            {
                index = 0;

                // Logging.CreateINPLOG("ERROR", "GetScacCode", 0, ex.Message);

                // iSeries.ReportError(ex, "", values);

            }

            return values[index];
        }

        // PUT: api/FRP022/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE: api/FRP022/5
        //public void Delete(int id)
        //{
        //}
    }
}

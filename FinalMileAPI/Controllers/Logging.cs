﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinalMileAPI.Models;

namespace FinalMileAPI.Controllers
{
    public static class Logging
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="glak2">INFO or ERROR</param>
        /// <param name="glad1">file or function name</param>
        /// <param name="glnd1">record count</param>
        /// <param name="glnote">information or exception details</param>
        public static void CreateINPLOG(string glak2, string glad1, decimal glnd1, string glnote)
        {
            INPLOG log = new INPLOG("FINALMILE", "API", glak2);

            try
            {
                log.AlphaData1 = glad1;

                log.NumericData1 = glnd1;

                log.Notes = glnote;

                log.Add();
            }
            catch { }

        }

        public static void CreateINPLOG(string glak2, string glad1, decimal glnd1, string glnote, string pro)
        {
            INPLOG log = new INPLOG("FINALMILE", "API", glak2);

            try
            {
                log.AlphaData3 = pro;

                log.AlphaData1 = glad1;

                log.NumericData1 = glnd1;

                log.Notes = glnote;

                log.Add();
            }
            catch (Exception ex)
            {
                string sdfs = ex.Message;

            }
            //catch { }

        }

    }
}
﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;


namespace FinalMileAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class AAP002
    {
        /// <summary>
        /// base select
        /// </summary>
        public const string SQL_SELECT_TBDKEY = "select tbdkey, tbdak1, tbdak2, tbdnk1, tbdnk2, tbddns, tbdoo, tbdad1, tbdad2, tbdad3, tbdrd1, tbdrd2, tbdnd1, tbdnd2, tbdnd3, tbdgl1, tbdgl2, tbdadt, tbdatm, tbdaup, tbdcdt, tbdctm, tbdcup from AAP002 where tbdkey = '{0}' ";

        public const string SQL_SELECT_TBDKEY_TBDAK1 = "select tbdkey, tbdak1, tbdak2, tbdnk1, tbdnk2, tbddns, tbdoo, tbdad1, tbdad2, tbdad3, tbdrd1, tbdrd2, tbdnd1, tbdnd2, tbdnd3, tbdgl1, tbdgl2, tbdadt, tbdatm, tbdaup, tbdcdt, tbdctm, tbdcup from AAP002 where tbdkey = '{0}' and tbdak1 = '{1}' ";

        /// <summary>
        /// base insert
        /// </summary>
        private const string SQL_INSERT = "insert into AAP002 (tbdkey, tbdak1, tbdak2, tbdnk1, tbdnk2, tbddns, tbdoo, tbdad1, tbdad2, tbdad3, tbdrd1, tbdrd2, tbdnd1, tbdnd2, tbdnd3, tbdgl1, tbdgl2, tbdadt, tbdatm, tbdaup, tbdcdt, tbdctm, tbdcup) values ('{0}', '{1}', '{2}', {3}, {4}, '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', {12}, {13}, {14}, {15}, {16}, {17}, {18}, '{19}', {20}, {21}, '{22}') ";

        /// <summary>
        /// base update
        /// </summary>
        private const string SQL_UPDATE = "update AAP002 set tbdkey = '{0}', tbdak1 = '{1}', tbdak2 = '{2}', tbdnk1 = {3}, tbdnk2 = {4}, tbddns = '{5}', tbdoo = '{6}', tbdad1 = '{7}', tbdad2 = '{8}', tbdad3 = '{9}', tbdrd1 = '{10}', tbdrd2 = '{11}', tbdnd1 = {12}, tbdnd2 = {13}, tbdnd3 = {14}, tbdgl1 = {15}, tbdgl2 = {16}, tbdadt = {17}, tbdatm = {18}, tbdaup = '{19}', tbdcdt = {20}, tbdctm = {21}, tbdcup = '{22}' where *** ";

        /// <summary>
        /// base delete
        /// </summary>
        private const string SQL_DELETE = "delete from AAP002 where *** ";

        /// <summary>
        /// default constructor
        /// </summary>
        public AAP002() { }

        /// <summary>
        /// preferred constructor
        /// </summary>
        /// <param name="row"></param>
        public AAP002(DataRow row)
        {

            tbdkey = row["tbdkey"].ToString().Trim();

            tbdak1 = row["tbdak1"].ToString().Trim();

            tbdak2 = row["tbdak2"].ToString().Trim();

            tbdnk1 = Convert.ToDecimal(row["tbdnk1"]);

            tbdnk2 = Convert.ToDecimal(row["tbdnk2"]);

            tbddns = row["tbddns"].ToString().Trim();

            tbdoo = row["tbdoo"].ToString().Trim();

            tbdad1 = row["tbdad1"].ToString().Trim();

            tbdad2 = row["tbdad2"].ToString().Trim();

            tbdad3 = row["tbdad3"].ToString().Trim();

            tbdrd1 = row["tbdrd1"].ToString().Trim();

            tbdrd2 = row["tbdrd2"].ToString().Trim();

            tbdnd1 = Convert.ToDecimal(row["tbdnd1"]);

            tbdnd2 = Convert.ToDecimal(row["tbdnd2"]);

            tbdnd3 = Convert.ToDecimal(row["tbdnd3"]);

            tbdgl1 = Convert.ToDecimal(row["tbdgl1"]);

            tbdgl2 = Convert.ToDecimal(row["tbdgl2"]);

            tbdadt = Convert.ToDecimal(row["tbdadt"]);

            tbdatm = Convert.ToDecimal(row["tbdatm"]);

            tbdaup = row["tbdaup"].ToString().Trim();

            tbdcdt = Convert.ToDecimal(row["tbdcdt"]);

            tbdctm = Convert.ToDecimal(row["tbdctm"]);

            tbdcup = row["tbdcup"].ToString().Trim();

        }


        /// <summary>
        /// array constructor
        /// </summary>
        /// <param name="values">construct from ToArray() of self</param>
        public AAP002(string[] values)
        {

            tbdkey = values[0];

            tbdak1 = values[1];

            tbdak2 = values[2];

            tbdnk1 = Convert.ToDecimal(values[3]);

            tbdnk2 = Convert.ToDecimal(values[4]);

            tbddns = values[5];

            tbdoo = values[6];

            tbdad1 = values[7];

            tbdad2 = values[8];

            tbdad3 = values[9];

            tbdrd1 = values[10];

            tbdrd2 = values[11];

            tbdnd1 = Convert.ToDecimal(values[12]);

            tbdnd2 = Convert.ToDecimal(values[13]);

            tbdnd3 = Convert.ToDecimal(values[14]);

            tbdgl1 = Convert.ToDecimal(values[15]);

            tbdgl2 = Convert.ToDecimal(values[16]);

            tbdadt = Convert.ToDecimal(values[17]);

            tbdatm = Convert.ToDecimal(values[18]);

            tbdaup = values[19];

            tbdcdt = Convert.ToDecimal(values[20]);

            tbdctm = Convert.ToDecimal(values[21]);

            tbdcup = values[22];

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>an array of the class properties</returns>
        public string[] ToArray()
        {
            return new string[] { tbdkey, tbdak1, tbdak2, tbdnk1.ToString(), tbdnk2.ToString(), tbddns, tbdoo, tbdad1, tbdad2, tbdad3, tbdrd1, tbdrd2, tbdnd1.ToString(), tbdnd2.ToString(), tbdnd3.ToString(), tbdgl1.ToString(), tbdgl2.ToString(), tbdadt.ToString(), tbdatm.ToString(), tbdaup, tbdcdt.ToString(), tbdctm.ToString(), tbdcup };
        }


        #region properties


        // 6
        /// <summary>
        /// Detail Key
        /// </summary>
        [Display(Name = "Detail Key")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Detail Key cannot be longer than 6 characters.")]
        public string tbdkey { get; set; }

        // 8
        /// <summary>
        /// Alpha Key 1
        /// </summary>
        [Display(Name = "Alpha Key 1")]
        [DefaultValue("")]
        [StringLength(8, ErrorMessage = "Alpha Key 1 cannot be longer than 8 characters.")]
        public string tbdak1 { get; set; }

        // 8
        /// <summary>
        /// Alpha Key 2
        /// </summary>
        [Display(Name = "Alpha Key 2")]
        [DefaultValue("")]
        [StringLength(8, ErrorMessage = "Alpha Key 2 cannot be longer than 8 characters.")]
        public string tbdak2 { get; set; }

        // 15.6
        /// <summary>
        /// Numeric Key 1
        /// </summary>
        [Display(Name = "Numeric Key 1")]
        [DefaultValue(0)]
        [Range(0, 999999999999999)]
        public decimal tbdnk1 { get; set; }

        // 15.6
        /// <summary>
        /// Numeric Key 2
        /// </summary>
        [Display(Name = "Numeric Key 2")]
        [DefaultValue(0)]
        [Range(0, 999999999999999)]
        public decimal tbdnk2 { get; set; }

        // 1
        /// <summary>
        /// Do Not Show
        /// </summary>
        [Display(Name = "Do Not Show")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Do Not Show cannot be longer than 1 characters.")]
        public string tbddns { get; set; }

        // 1
        /// <summary>
        /// On/Off Value
        /// </summary>
        [Display(Name = "On/Off Value")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "On/Off Value cannot be longer than 1 characters.")]
        public string tbdoo { get; set; }

        // 4
        /// <summary>
        /// Alpha Data 1
        /// </summary>
        [Display(Name = "Alpha Data 1")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Alpha Data 1 cannot be longer than 4 characters.")]
        public string tbdad1 { get; set; }

        // 10
        /// <summary>
        /// Alpha Data 2
        /// </summary>
        [Display(Name = "Alpha Data 2")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Alpha Data 2 cannot be longer than 10 characters.")]
        public string tbdad2 { get; set; }

        // 50
        /// <summary>
        /// Alpha Data 3
        /// </summary>
        [Display(Name = "Alpha Data 3")]
        [DefaultValue("")]
        [StringLength(50, ErrorMessage = "Alpha Data 3 cannot be longer than 50 characters.")]
        public string tbdad3 { get; set; }

        // 4
        /// <summary>
        /// Result Data 1
        /// </summary>
        [Display(Name = "Result Data 1")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Result Data 1 cannot be longer than 4 characters.")]
        public string tbdrd1 { get; set; }

        // 50
        /// <summary>
        /// Result Data 2
        /// </summary>
        [Display(Name = "Result Data 2")]
        [DefaultValue("")]
        [StringLength(50, ErrorMessage = "Result Data 2 cannot be longer than 50 characters.")]
        public string tbdrd2 { get; set; }

        // 3.0
        /// <summary>
        /// Numeric Data 1
        /// </summary>
        [Display(Name = "Numeric Data 1")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal tbdnd1 { get; set; }

        // 9.2
        /// <summary>
        /// Numeric Data 2
        /// </summary>
        [Display(Name = "Numeric Data 2")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal tbdnd2 { get; set; }

        // 15.6
        /// <summary>
        /// Numeric Data 3
        /// </summary>
        [Display(Name = "Numeric Data 3")]
        [DefaultValue(0)]
        [Range(0, 999999999999999)]
        public decimal tbdnd3 { get; set; }

        // 15.0
        /// <summary>
        /// GL Account Number
        /// </summary>
        [Display(Name = "GL Account Number")]
        [DefaultValue(0)]
        [Range(0, 999999999999999)]
        public decimal tbdgl1 { get; set; }

        // 15.0
        /// <summary>
        /// GL Account Number
        /// </summary>
        [Display(Name = "GL Account Number")]
        [DefaultValue(0)]
        [Range(0, 999999999999999)]
        public decimal tbdgl2 { get; set; }

        // 7.0
        /// <summary>
        /// Added On (Date)
        /// </summary>
        [Display(Name = "Added On (Date)")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal tbdadt { get; set; }

        // 6.0
        /// <summary>
        /// Added At (Time)
        /// </summary>
        [Display(Name = "Added At (Time)")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal tbdatm { get; set; }

        // 10
        /// <summary>
        /// Added By (User)
        /// </summary>
        [Display(Name = "Added By (User)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Added By (User) cannot be longer than 10 characters.")]
        public string tbdaup { get; set; }

        // 7.0
        /// <summary>
        /// Changed On (Date)
        /// </summary>
        [Display(Name = "Changed On (Date)")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal tbdcdt { get; set; }

        // 6.0
        /// <summary>
        /// Changed At (Time)
        /// </summary>
        [Display(Name = "Changed At (Time)")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal tbdctm { get; set; }

        // 10
        /// <summary>
        /// Changed By (User)
        /// </summary>
        [Display(Name = "Changed By (User)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Changed By (User) cannot be longer than 10 characters.")]
        public string tbdcup { get; set; }



        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;


namespace FinalMileAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class AAP030
    {
        /// <summary>
        /// base select
        /// </summary>
        public const string SQL_SELECT_CT1TID = "select ct1tid, ct1ab, ct1num, ct1pid, ct1nam, ct1cn, ct1sn, ct1pn, ct1ad1, ct1ad2, ct1cit, ct1st, ct1zp, ct1zp4, ct1zon, ct1lt, ct1rt, ct1at, ct1fax, ct1rmn, ct1rmw, ct1rmc, ct1rmh, ct1tmn, ct1tmw, ct1tmc, ct1tmh, ct1amn, ct1amw, ct1amc, ct1amh, ct1omn, ct1omw, ct1omc, ct1omh, ct1smn, ct1smw, ct1smc, ct1smh, ct1dsn, ct1dsw, ct1dsc, ct1dsh, ct1hsn, ct1hsw, ct1hsc, ct1hsh, ct1tsn, ct1tsw, ct1tsc, ct1tsh, ct1osn, ct1osw, ct1osc, ct1osh, ct1reg, ct1ct, ct1typ, ct1rta, ct1tzc, ct1co, ct1div, ct1et, ct1dat, ct1ibc, ct1eda, ct1puc, ct1wot, ct1wct, ct1aot, ct1act, ct1uot, ct1uct, ct1dd, ct1apu, ct1atl, ct1adl, ct1drd, ct1dcd, ct1ft, ct1gs, ct1fy, ct1fq, ct1fp, ct1fw, ct1mc, ct1lfb, ct1lmn, ct1los, ct1lfn, ct1lsn, ct1p1, ct1p1d, ct1p1o, ct1p2, ct1p2d, ct1p2o, ct1p3, ct1p3d, ct1p3o, ct1p4, ct1p4d, ct1p4o, ct1p5, ct1p5d, ct1p5o, ct1p6, ct1p6d, ct1p6o, ct1p7, ct1p7d, ct1p7o, ct1p8, ct1p8d, ct1p8o, ct1p9, ct1p9d, ct1p9o, ct1adt, ct1atm, ct1aup, ct1udt, ct1utm, ct1uup from AAP030 where ct1tid in ({0}) ";
        //public const string SQL_SELECT_CT1TID = "select ct1tid, ct1ab, ct1num, ct1pid, ct1nam, ct1cn, ct1sn, ct1pn, ct1ad1, ct1ad2, ct1cit, ct1st, ct1zp, ct1zp4, ct1zon, ct1lt, ct1rt, ct1at, ct1fax, ct1rmn, ct1rmw, ct1rmc, ct1rmh, ct1tmn, ct1tmw, ct1tmc, ct1tmh, ct1amn, ct1amw, ct1amc, ct1amh, ct1omn, ct1omw, ct1omc, ct1omh, ct1smn, ct1smw, ct1smc, ct1smh, ct1dsn, ct1dsw, ct1dsc, ct1dsh, ct1hsn, ct1hsw, ct1hsc, ct1hsh, ct1tsn, ct1tsw, ct1tsc, ct1tsh, ct1osn, ct1osw, ct1osc, ct1osh, ct1reg, ct1ct, ct1typ, ct1rta, ct1tzc, ct1co, ct1div, ct1et, ct1dat, ct1ibc, ct1eda, ct1puc, ct1wot, ct1wct, ct1aot, ct1act, ct1uot, ct1uct, ct1dd, ct1apu, ct1atl, ct1adl, ct1drd, ct1dcd, ct1ft, ct1gs, ct1fy, ct1fq, ct1fp, ct1fw, ct1mc, ct1lfb, ct1lmn, ct1los, ct1lfn, ct1lsn, ct1p1, ct1p1d, ct1p1o, ct1p2, ct1p2d, ct1p2o, ct1p3, ct1p3d, ct1p3o, ct1p4, ct1p4d, ct1p4o, ct1p5, ct1p5d, ct1p5o, ct1p6, ct1p6d, ct1p6o, ct1p7, ct1p7d, ct1p7o, ct1p8, ct1p8d, ct1p8o, ct1p9, ct1p9d, ct1p9o, ct1adt, ct1atm, ct1aup, ct1udt, ct1utm, ct1uup from AAP030 where ct1tid = '{0}' ";

        /// <summary>
        /// base insert
        /// </summary>
        //private const string SQL_INSERT = "insert into AAP030 (ct1tid, ct1ab, ct1num, ct1pid, ct1nam, ct1cn, ct1sn, ct1pn, ct1ad1, ct1ad2, ct1cit, ct1st, ct1zp, ct1zp4, ct1zon, ct1lt, ct1rt, ct1at, ct1fax, ct1rmn, ct1rmw, ct1rmc, ct1rmh, ct1tmn, ct1tmw, ct1tmc, ct1tmh, ct1amn, ct1amw, ct1amc, ct1amh, ct1omn, ct1omw, ct1omc, ct1omh, ct1smn, ct1smw, ct1smc, ct1smh, ct1dsn, ct1dsw, ct1dsc, ct1dsh, ct1hsn, ct1hsw, ct1hsc, ct1hsh, ct1tsn, ct1tsw, ct1tsc, ct1tsh, ct1osn, ct1osw, ct1osc, ct1osh, ct1reg, ct1ct, ct1typ, ct1rta, ct1tzc, ct1co, ct1div, ct1et, ct1dat, ct1ibc, ct1eda, ct1puc, ct1wot, ct1wct, ct1aot, ct1act, ct1uot, ct1uct, ct1dd, ct1apu, ct1atl, ct1adl, ct1drd, ct1dcd, ct1ft, ct1gs, ct1fy, ct1fq, ct1fp, ct1fw, ct1mc, ct1lfb, ct1lmn, ct1los, ct1lfn, ct1lsn, ct1p1, ct1p1d, ct1p1o, ct1p2, ct1p2d, ct1p2o, ct1p3, ct1p3d, ct1p3o, ct1p4, ct1p4d, ct1p4o, ct1p5, ct1p5d, ct1p5o, ct1p6, ct1p6d, ct1p6o, ct1p7, ct1p7d, ct1p7o, ct1p8, ct1p8d, ct1p8o, ct1p9, ct1p9d, ct1p9o, ct1adt, ct1atm, ct1aup, ct1udt, ct1utm, ct1uup) values ('{0}', '{1}', {2}, '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', {15}, {16}, {17}, {18}, '{19}', {20}, {21}, {22}, '{23}', {24}, {25}, {26}, '{27}', {28}, {29}, {30}, '{31}', {32}, {33}, {34}, '{35}', {36}, {37}, {38}, '{39}', {40}, {41}, {42}, '{43}', {44}, {45}, {46}, '{47}', {48}, {49}, {50}, '{51}', {52}, {53}, {54}, '{55}', '{56}', '{57}', '{58}', {59}, {60}, {61}, '{62}', {63}, {64}, {65}, {66}, {67}, {68}, {69}, {70}, {71}, {72}, {73}, {74}, {75}, {76}, {77}, {78}, '{79}', '{80}', {81}, {82}, {83}, {84}, '{85}', {86}, {87}, {88}, {89}, {90}, '{91}', '{92}', '{93}', '{94}', '{95}', '{96}', '{97}', '{98}', '{99}', '{100}', '{101}', '{102}', '{103}', '{104}', '{105}', '{106}', '{107}', '{108}', '{109}', '{110}', '{111}', '{112}', '{113}', '{114}', '{115}', '{116}', '{117}', {118}, {119}, '{120}', {121}, {122}, '{123}') ";

        /// <summary>
        /// base update
        /// </summary>
        //private const string SQL_UPDATE = "update AAP030 set ct1tid = '{0}', ct1ab = '{1}', ct1num = {2}, ct1pid = '{3}', ct1nam = '{4}', ct1cn = '{5}', ct1sn = '{6}', ct1pn = '{7}', ct1ad1 = '{8}', ct1ad2 = '{9}', ct1cit = '{10}', ct1st = '{11}', ct1zp = '{12}', ct1zp4 = '{13}', ct1zon = '{14}', ct1lt = {15}, ct1rt = {16}, ct1at = {17}, ct1fax = {18}, ct1rmn = '{19}', ct1rmw = {20}, ct1rmc = {21}, ct1rmh = {22}, ct1tmn = '{23}', ct1tmw = {24}, ct1tmc = {25}, ct1tmh = {26}, ct1amn = '{27}', ct1amw = {28}, ct1amc = {29}, ct1amh = {30}, ct1omn = '{31}', ct1omw = {32}, ct1omc = {33}, ct1omh = {34}, ct1smn = '{35}', ct1smw = {36}, ct1smc = {37}, ct1smh = {38}, ct1dsn = '{39}', ct1dsw = {40}, ct1dsc = {41}, ct1dsh = {42}, ct1hsn = '{43}', ct1hsw = {44}, ct1hsc = {45}, ct1hsh = {46}, ct1tsn = '{47}', ct1tsw = {48}, ct1tsc = {49}, ct1tsh = {50}, ct1osn = '{51}', ct1osw = {52}, ct1osc = {53}, ct1osh = {54}, ct1reg = '{55}', ct1ct = '{56}', ct1typ = '{57}', ct1rta = '{58}', ct1tzc = {59}, ct1co = {60}, ct1div = {61}, ct1et = '{62}', ct1dat = {63}, ct1ibc = {64}, ct1eda = {65}, ct1puc = {66}, ct1wot = {67}, ct1wct = {68}, ct1aot = {69}, ct1act = {70}, ct1uot = {71}, ct1uct = {72}, ct1dd = {73}, ct1apu = {74}, ct1atl = {75}, ct1adl = {76}, ct1drd = {77}, ct1dcd = {78}, ct1ft = '{79}', ct1gs = '{80}', ct1fy = {81}, ct1fq = {82}, ct1fp = {83}, ct1fw = {84}, ct1mc = '{85}', ct1lfb = {86}, ct1lmn = {87}, ct1los = {88}, ct1lfn = {89}, ct1lsn = {90}, ct1p1 = '{91}', ct1p1d = '{92}', ct1p1o = '{93}', ct1p2 = '{94}', ct1p2d = '{95}', ct1p2o = '{96}', ct1p3 = '{97}', ct1p3d = '{98}', ct1p3o = '{99}', ct1p4 = '{100}', ct1p4d = '{101}', ct1p4o = '{102}', ct1p5 = '{103}', ct1p5d = '{104}', ct1p5o = '{105}', ct1p6 = '{106}', ct1p6d = '{107}', ct1p6o = '{108}', ct1p7 = '{109}', ct1p7d = '{110}', ct1p7o = '{111}', ct1p8 = '{112}', ct1p8d = '{113}', ct1p8o = '{114}', ct1p9 = '{115}', ct1p9d = '{116}', ct1p9o = '{117}', ct1adt = {118}, ct1atm = {119}, ct1aup = '{120}', ct1udt = {121}, ct1utm = {122}, ct1uup = '{123}' where *** ";

        /// <summary>
        /// base delete
        /// </summary>
        //private const string SQL_DELETE = "delete from AAP030 where *** ";

        /// <summary>
        /// default contructor
        /// </summary>
        public AAP030() { }

        /// <summary>
        /// preferred contructor
        /// </summary>
        /// <param name="row"></param>
        public AAP030(DataRow row)
        {

            ct1tid = row["ct1tid"].ToString().Trim();

            ct1ab = row["ct1ab"].ToString().Trim();

            ct1num = Convert.ToDecimal(row["ct1num"]);

            ct1pid = row["ct1pid"].ToString().Trim();

            ct1nam = row["ct1nam"].ToString().Trim();

            ct1cn = row["ct1cn"].ToString().Trim();

            ct1sn = row["ct1sn"].ToString().Trim();

            ct1pn = row["ct1pn"].ToString().Trim();

            ct1ad1 = row["ct1ad1"].ToString().Trim();

            ct1ad2 = row["ct1ad2"].ToString().Trim();

            ct1cit = row["ct1cit"].ToString().Trim();

            ct1st = row["ct1st"].ToString().Trim();

            ct1zp = row["ct1zp"].ToString().Trim();

            ct1zp4 = row["ct1zp4"].ToString().Trim();

            ct1zon = row["ct1zon"].ToString().Trim();

            ct1lt = Convert.ToDecimal(row["ct1lt"]);

            ct1rt = Convert.ToDecimal(row["ct1rt"]);

            ct1at = Convert.ToDecimal(row["ct1at"]);

            ct1fax = Convert.ToDecimal(row["ct1fax"]);

            ct1rmn = row["ct1rmn"].ToString().Trim();

            ct1rmw = Convert.ToDecimal(row["ct1rmw"]);

            ct1rmc = Convert.ToDecimal(row["ct1rmc"]);

            ct1rmh = Convert.ToDecimal(row["ct1rmh"]);

            ct1tmn = row["ct1tmn"].ToString().Trim();

            ct1tmw = Convert.ToDecimal(row["ct1tmw"]);

            ct1tmc = Convert.ToDecimal(row["ct1tmc"]);

            ct1tmh = Convert.ToDecimal(row["ct1tmh"]);

            ct1amn = row["ct1amn"].ToString().Trim();

            ct1amw = Convert.ToDecimal(row["ct1amw"]);

            ct1amc = Convert.ToDecimal(row["ct1amc"]);

            ct1amh = Convert.ToDecimal(row["ct1amh"]);

            ct1omn = row["ct1omn"].ToString().Trim();

            ct1omw = Convert.ToDecimal(row["ct1omw"]);

            ct1omc = Convert.ToDecimal(row["ct1omc"]);

            ct1omh = Convert.ToDecimal(row["ct1omh"]);

            ct1smn = row["ct1smn"].ToString().Trim();

            ct1smw = Convert.ToDecimal(row["ct1smw"]);

            ct1smc = Convert.ToDecimal(row["ct1smc"]);

            ct1smh = Convert.ToDecimal(row["ct1smh"]);

            ct1dsn = row["ct1dsn"].ToString().Trim();

            ct1dsw = Convert.ToDecimal(row["ct1dsw"]);

            ct1dsc = Convert.ToDecimal(row["ct1dsc"]);

            ct1dsh = Convert.ToDecimal(row["ct1dsh"]);

            ct1hsn = row["ct1hsn"].ToString().Trim();

            ct1hsw = Convert.ToDecimal(row["ct1hsw"]);

            ct1hsc = Convert.ToDecimal(row["ct1hsc"]);

            ct1hsh = Convert.ToDecimal(row["ct1hsh"]);

            ct1tsn = row["ct1tsn"].ToString().Trim();

            ct1tsw = Convert.ToDecimal(row["ct1tsw"]);

            ct1tsc = Convert.ToDecimal(row["ct1tsc"]);

            ct1tsh = Convert.ToDecimal(row["ct1tsh"]);

            ct1osn = row["ct1osn"].ToString().Trim();

            ct1osw = Convert.ToDecimal(row["ct1osw"]);

            ct1osc = Convert.ToDecimal(row["ct1osc"]);

            ct1osh = Convert.ToDecimal(row["ct1osh"]);

            ct1reg = row["ct1reg"].ToString().Trim();

            ct1ct = row["ct1ct"].ToString().Trim();

            ct1typ = row["ct1typ"].ToString().Trim();

            ct1rta = row["ct1rta"].ToString().Trim();

            ct1tzc = Convert.ToDecimal(row["ct1tzc"]);

            ct1co = Convert.ToDecimal(row["ct1co"]);

            ct1div = Convert.ToDecimal(row["ct1div"]);

            ct1et = row["ct1et"].ToString().Trim();

            ct1dat = Convert.ToDecimal(row["ct1dat"]);

            ct1ibc = Convert.ToDecimal(row["ct1ibc"]);

            ct1eda = Convert.ToDecimal(row["ct1eda"]);

            ct1puc = Convert.ToDecimal(row["ct1puc"]);

            ct1wot = Convert.ToDecimal(row["ct1wot"]);

            ct1wct = Convert.ToDecimal(row["ct1wct"]);

            ct1aot = Convert.ToDecimal(row["ct1aot"]);

            ct1act = Convert.ToDecimal(row["ct1act"]);

            ct1uot = Convert.ToDecimal(row["ct1uot"]);

            ct1uct = Convert.ToDecimal(row["ct1uct"]);

            ct1dd = Convert.ToDecimal(row["ct1dd"]);

            ct1apu = Convert.ToDecimal(row["ct1apu"]);

            ct1atl = Convert.ToDecimal(row["ct1atl"]);

            ct1adl = Convert.ToDecimal(row["ct1adl"]);

            ct1drd = Convert.ToDecimal(row["ct1drd"]);

            ct1dcd = Convert.ToDecimal(row["ct1dcd"]);

            ct1ft = row["ct1ft"].ToString().Trim();

            ct1gs = row["ct1gs"].ToString().Trim();

            ct1fy = Convert.ToDecimal(row["ct1fy"]);

            ct1fq = Convert.ToDecimal(row["ct1fq"]);

            ct1fp = Convert.ToDecimal(row["ct1fp"]);

            ct1fw = Convert.ToDecimal(row["ct1fw"]);

            ct1mc = row["ct1mc"].ToString().Trim();

            ct1lfb = Convert.ToDecimal(row["ct1lfb"]);

            ct1lmn = Convert.ToDecimal(row["ct1lmn"]);

            ct1los = Convert.ToDecimal(row["ct1los"]);

            ct1lfn = Convert.ToDecimal(row["ct1lfn"]);

            ct1lsn = Convert.ToDecimal(row["ct1lsn"]);

            ct1p1 = row["ct1p1"].ToString().Trim();

            ct1p1d = row["ct1p1d"].ToString().Trim();

            ct1p1o = row["ct1p1o"].ToString().Trim();

            ct1p2 = row["ct1p2"].ToString().Trim();

            ct1p2d = row["ct1p2d"].ToString().Trim();

            ct1p2o = row["ct1p2o"].ToString().Trim();

            ct1p3 = row["ct1p3"].ToString().Trim();

            ct1p3d = row["ct1p3d"].ToString().Trim();

            ct1p3o = row["ct1p3o"].ToString().Trim();

            ct1p4 = row["ct1p4"].ToString().Trim();

            ct1p4d = row["ct1p4d"].ToString().Trim();

            ct1p4o = row["ct1p4o"].ToString().Trim();

            ct1p5 = row["ct1p5"].ToString().Trim();

            ct1p5d = row["ct1p5d"].ToString().Trim();

            ct1p5o = row["ct1p5o"].ToString().Trim();

            ct1p6 = row["ct1p6"].ToString().Trim();

            ct1p6d = row["ct1p6d"].ToString().Trim();

            ct1p6o = row["ct1p6o"].ToString().Trim();

            ct1p7 = row["ct1p7"].ToString().Trim();

            ct1p7d = row["ct1p7d"].ToString().Trim();

            ct1p7o = row["ct1p7o"].ToString().Trim();

            ct1p8 = row["ct1p8"].ToString().Trim();

            ct1p8d = row["ct1p8d"].ToString().Trim();

            ct1p8o = row["ct1p8o"].ToString().Trim();

            ct1p9 = row["ct1p9"].ToString().Trim();

            ct1p9d = row["ct1p9d"].ToString().Trim();

            ct1p9o = row["ct1p9o"].ToString().Trim();

            ct1adt = Convert.ToDecimal(row["ct1adt"]);

            ct1atm = Convert.ToDecimal(row["ct1atm"]);

            ct1aup = row["ct1aup"].ToString().Trim();

            ct1udt = Convert.ToDecimal(row["ct1udt"]);

            ct1utm = Convert.ToDecimal(row["ct1utm"]);

            ct1uup = row["ct1uup"].ToString().Trim();

        }


        /// <summary>
        /// array contructor
        /// </summary>
        /// <param name="values">construct from ToArray() of self</param>
        public AAP030(string[] values)
        {

            ct1tid = values[0];

            ct1ab = values[1];

            ct1num = Convert.ToDecimal(values[2]);

            ct1pid = values[3];

            ct1nam = values[4];

            ct1cn = values[5];

            ct1sn = values[6];

            ct1pn = values[7];

            ct1ad1 = values[8];

            ct1ad2 = values[9];

            ct1cit = values[10];

            ct1st = values[11];

            ct1zp = values[12];

            ct1zp4 = values[13];

            ct1zon = values[14];

            ct1lt = Convert.ToDecimal(values[15]);

            ct1rt = Convert.ToDecimal(values[16]);

            ct1at = Convert.ToDecimal(values[17]);

            ct1fax = Convert.ToDecimal(values[18]);

            ct1rmn = values[19];

            ct1rmw = Convert.ToDecimal(values[20]);

            ct1rmc = Convert.ToDecimal(values[21]);

            ct1rmh = Convert.ToDecimal(values[22]);

            ct1tmn = values[23];

            ct1tmw = Convert.ToDecimal(values[24]);

            ct1tmc = Convert.ToDecimal(values[25]);

            ct1tmh = Convert.ToDecimal(values[26]);

            ct1amn = values[27];

            ct1amw = Convert.ToDecimal(values[28]);

            ct1amc = Convert.ToDecimal(values[29]);

            ct1amh = Convert.ToDecimal(values[30]);

            ct1omn = values[31];

            ct1omw = Convert.ToDecimal(values[32]);

            ct1omc = Convert.ToDecimal(values[33]);

            ct1omh = Convert.ToDecimal(values[34]);

            ct1smn = values[35];

            ct1smw = Convert.ToDecimal(values[36]);

            ct1smc = Convert.ToDecimal(values[37]);

            ct1smh = Convert.ToDecimal(values[38]);

            ct1dsn = values[39];

            ct1dsw = Convert.ToDecimal(values[40]);

            ct1dsc = Convert.ToDecimal(values[41]);

            ct1dsh = Convert.ToDecimal(values[42]);

            ct1hsn = values[43];

            ct1hsw = Convert.ToDecimal(values[44]);

            ct1hsc = Convert.ToDecimal(values[45]);

            ct1hsh = Convert.ToDecimal(values[46]);

            ct1tsn = values[47];

            ct1tsw = Convert.ToDecimal(values[48]);

            ct1tsc = Convert.ToDecimal(values[49]);

            ct1tsh = Convert.ToDecimal(values[50]);

            ct1osn = values[51];

            ct1osw = Convert.ToDecimal(values[52]);

            ct1osc = Convert.ToDecimal(values[53]);

            ct1osh = Convert.ToDecimal(values[54]);

            ct1reg = values[55];

            ct1ct = values[56];

            ct1typ = values[57];

            ct1rta = values[58];

            ct1tzc = Convert.ToDecimal(values[59]);

            ct1co = Convert.ToDecimal(values[60]);

            ct1div = Convert.ToDecimal(values[61]);

            ct1et = values[62];

            ct1dat = Convert.ToDecimal(values[63]);

            ct1ibc = Convert.ToDecimal(values[64]);

            ct1eda = Convert.ToDecimal(values[65]);

            ct1puc = Convert.ToDecimal(values[66]);

            ct1wot = Convert.ToDecimal(values[67]);

            ct1wct = Convert.ToDecimal(values[68]);

            ct1aot = Convert.ToDecimal(values[69]);

            ct1act = Convert.ToDecimal(values[70]);

            ct1uot = Convert.ToDecimal(values[71]);

            ct1uct = Convert.ToDecimal(values[72]);

            ct1dd = Convert.ToDecimal(values[73]);

            ct1apu = Convert.ToDecimal(values[74]);

            ct1atl = Convert.ToDecimal(values[75]);

            ct1adl = Convert.ToDecimal(values[76]);

            ct1drd = Convert.ToDecimal(values[77]);

            ct1dcd = Convert.ToDecimal(values[78]);

            ct1ft = values[79];

            ct1gs = values[80];

            ct1fy = Convert.ToDecimal(values[81]);

            ct1fq = Convert.ToDecimal(values[82]);

            ct1fp = Convert.ToDecimal(values[83]);

            ct1fw = Convert.ToDecimal(values[84]);

            ct1mc = values[85];

            ct1lfb = Convert.ToDecimal(values[86]);

            ct1lmn = Convert.ToDecimal(values[87]);

            ct1los = Convert.ToDecimal(values[88]);

            ct1lfn = Convert.ToDecimal(values[89]);

            ct1lsn = Convert.ToDecimal(values[90]);

            ct1p1 = values[91];

            ct1p1d = values[92];

            ct1p1o = values[93];

            ct1p2 = values[94];

            ct1p2d = values[95];

            ct1p2o = values[96];

            ct1p3 = values[97];

            ct1p3d = values[98];

            ct1p3o = values[99];

            ct1p4 = values[100];

            ct1p4d = values[101];

            ct1p4o = values[102];

            ct1p5 = values[103];

            ct1p5d = values[104];

            ct1p5o = values[105];

            ct1p6 = values[106];

            ct1p6d = values[107];

            ct1p6o = values[108];

            ct1p7 = values[109];

            ct1p7d = values[110];

            ct1p7o = values[111];

            ct1p8 = values[112];

            ct1p8d = values[113];

            ct1p8o = values[114];

            ct1p9 = values[115];

            ct1p9d = values[116];

            ct1p9o = values[117];

            ct1adt = Convert.ToDecimal(values[118]);

            ct1atm = Convert.ToDecimal(values[119]);

            ct1aup = values[120];

            ct1udt = Convert.ToDecimal(values[121]);

            ct1utm = Convert.ToDecimal(values[122]);

            ct1uup = values[123];

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>an array of the class properties</returns>
        public string[] ToArray()
        {
            return new string[] { ct1tid, ct1ab, ct1num.ToString(), ct1pid, ct1nam, ct1cn, ct1sn, ct1pn, ct1ad1, ct1ad2, ct1cit, ct1st, ct1zp, ct1zp4, ct1zon, ct1lt.ToString(), ct1rt.ToString(), ct1at.ToString(), ct1fax.ToString(), ct1rmn, ct1rmw.ToString(), ct1rmc.ToString(), ct1rmh.ToString(), ct1tmn, ct1tmw.ToString(), ct1tmc.ToString(), ct1tmh.ToString(), ct1amn, ct1amw.ToString(), ct1amc.ToString(), ct1amh.ToString(), ct1omn, ct1omw.ToString(), ct1omc.ToString(), ct1omh.ToString(), ct1smn, ct1smw.ToString(), ct1smc.ToString(), ct1smh.ToString(), ct1dsn, ct1dsw.ToString(), ct1dsc.ToString(), ct1dsh.ToString(), ct1hsn, ct1hsw.ToString(), ct1hsc.ToString(), ct1hsh.ToString(), ct1tsn, ct1tsw.ToString(), ct1tsc.ToString(), ct1tsh.ToString(), ct1osn, ct1osw.ToString(), ct1osc.ToString(), ct1osh.ToString(), ct1reg, ct1ct, ct1typ, ct1rta, ct1tzc.ToString(), ct1co.ToString(), ct1div.ToString(), ct1et, ct1dat.ToString(), ct1ibc.ToString(), ct1eda.ToString(), ct1puc.ToString(), ct1wot.ToString(), ct1wct.ToString(), ct1aot.ToString(), ct1act.ToString(), ct1uot.ToString(), ct1uct.ToString(), ct1dd.ToString(), ct1apu.ToString(), ct1atl.ToString(), ct1adl.ToString(), ct1drd.ToString(), ct1dcd.ToString(), ct1ft, ct1gs, ct1fy.ToString(), ct1fq.ToString(), ct1fp.ToString(), ct1fw.ToString(), ct1mc, ct1lfb.ToString(), ct1lmn.ToString(), ct1los.ToString(), ct1lfn.ToString(), ct1lsn.ToString(), ct1p1, ct1p1d, ct1p1o, ct1p2, ct1p2d, ct1p2o, ct1p3, ct1p3d, ct1p3o, ct1p4, ct1p4d, ct1p4o, ct1p5, ct1p5d, ct1p5o, ct1p6, ct1p6d, ct1p6o, ct1p7, ct1p7d, ct1p7o, ct1p8, ct1p8d, ct1p8o, ct1p9, ct1p9d, ct1p9o, ct1adt.ToString(), ct1atm.ToString(), ct1aup, ct1udt.ToString(), ct1utm.ToString(), ct1uup };
        }


        #region properties


        // 5
        /// <summary>
        /// Terminal ID
        /// </summary>
        [Display(Name = "Terminal ID")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Terminal ID cannot be longer than 5 characters.")]
        public string ct1tid { get; set; }

        // 5
        /// <summary>
        /// Terminal Abbreviation
        /// </summary>
        [Display(Name = "Terminal Abbreviation")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Terminal Abbreviation cannot be longer than 5 characters.")]
        public string ct1ab { get; set; }

        // 5.0
        /// <summary>
        /// Terminal Number
        /// </summary>
        [Display(Name = "Terminal Number")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal ct1num { get; set; }

        // 5
        /// <summary>
        /// Terminal ID For Printing
        /// </summary>
        [Display(Name = "Terminal ID For Printing")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Terminal ID For Printing cannot be longer than 5 characters.")]
        public string ct1pid { get; set; }

        // 25
        /// <summary>
        /// Terminal Name
        /// </summary>
        [Display(Name = "Terminal Name")]
        [DefaultValue("")]
        [StringLength(25, ErrorMessage = "Terminal Name cannot be longer than 25 characters.")]
        public string ct1nam { get; set; }

        // 50
        /// <summary>
        /// Terminal Centered Name
        /// </summary>
        [Display(Name = "Terminal Centered Name")]
        [DefaultValue("")]
        [StringLength(50, ErrorMessage = "Terminal Centered Name cannot be longer than 50 characters.")]
        public string ct1cn { get; set; }

        // 10
        /// <summary>
        /// Terminal Short Name
        /// </summary>
        [Display(Name = "Terminal Short Name")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Terminal Short Name cannot be longer than 10 characters.")]
        public string ct1sn { get; set; }

        // 7
        /// <summary>
        /// City/State Short Name
        /// </summary>
        [Display(Name = "City/State Short Name")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "City/State Short Name cannot be longer than 7 characters.")]
        public string ct1pn { get; set; }

        // 30
        /// <summary>
        /// Terminal Address Line 1
        /// </summary>
        [Display(Name = "Terminal Address Line 1")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Terminal Address Line 1 cannot be longer than 30 characters.")]
        public string ct1ad1 { get; set; }

        // 30
        /// <summary>
        /// Terminal Address Line 2
        /// </summary>
        [Display(Name = "Terminal Address Line 2")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Terminal Address Line 2 cannot be longer than 30 characters.")]
        public string ct1ad2 { get; set; }

        // 20
        /// <summary>
        /// Terminal City
        /// </summary>
        [Display(Name = "Terminal City")]
        [DefaultValue("")]
        [StringLength(20, ErrorMessage = "Terminal City cannot be longer than 20 characters.")]
        public string ct1cit { get; set; }

        // 2
        /// <summary>
        /// Terminal State
        /// </summary>
        [Display(Name = "Terminal State")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Terminal State cannot be longer than 2 characters.")]
        public string ct1st { get; set; }

        // 6
        /// <summary>
        /// Terminal ZIP/Postal Code
        /// </summary>
        [Display(Name = "Terminal ZIP/Postal Code")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Terminal ZIP/Postal Code cannot be longer than 6 characters.")]
        public string ct1zp { get; set; }

        // 4
        /// <summary>
        /// Terminal ZIP+4 Code
        /// </summary>
        [Display(Name = "Terminal ZIP+4 Code")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Terminal ZIP+4 Code cannot be longer than 4 characters.")]
        public string ct1zp4 { get; set; }

        // 3
        /// <summary>
        /// Terminal Zone
        /// </summary>
        [Display(Name = "Terminal Zone")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Terminal Zone cannot be longer than 3 characters.")]
        public string ct1zon { get; set; }

        // 11.0
        /// <summary>
        /// Local Telephone
        /// </summary>
        [Display(Name = "Local Telephone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1lt { get; set; }

        // 11.0
        /// <summary>
        /// Interstate WATS
        /// </summary>
        [Display(Name = "Interstate WATS")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1rt { get; set; }

        // 11.0
        /// <summary>
        /// Intrastate WATS
        /// </summary>
        [Display(Name = "Intrastate WATS")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1at { get; set; }

        // 11.0
        /// <summary>
        /// Fax Telephone
        /// </summary>
        [Display(Name = "Fax Telephone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1fax { get; set; }

        // 30
        /// <summary>
        /// Regional Manager
        /// </summary>
        [Display(Name = "Regional Manager")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Regional Manager cannot be longer than 30 characters.")]
        public string ct1rmn { get; set; }

        // 11.0
        /// <summary>
        /// Regional Manager Work Phone
        /// </summary>
        [Display(Name = "Regional Manager Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1rmw { get; set; }

        // 11.0
        /// <summary>
        /// Regional Manager Cell Phone
        /// </summary>
        [Display(Name = "Regional Manager Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1rmc { get; set; }

        // 11.0
        /// <summary>
        /// Regional Manager Home Phone
        /// </summary>
        [Display(Name = "Regional Manager Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1rmh { get; set; }

        // 30
        /// <summary>
        /// Terminal Manager
        /// </summary>
        [Display(Name = "Terminal Manager")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Terminal Manager cannot be longer than 30 characters.")]
        public string ct1tmn { get; set; }

        // 11.0
        /// <summary>
        /// Terminal Manager Work Phone
        /// </summary>
        [Display(Name = "Terminal Manager Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1tmw { get; set; }

        // 11.0
        /// <summary>
        /// Terminal Manager Cell Phone
        /// </summary>
        [Display(Name = "Terminal Manager Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1tmc { get; set; }

        // 11.0
        /// <summary>
        /// Terminal Manager Home Phone
        /// </summary>
        [Display(Name = "Terminal Manager Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1tmh { get; set; }

        // 30
        /// <summary>
        /// Assistant Manager
        /// </summary>
        [Display(Name = "Assistant Manager")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Assistant Manager cannot be longer than 30 characters.")]
        public string ct1amn { get; set; }

        // 11.0
        /// <summary>
        /// Assistant Manager Work Phone
        /// </summary>
        [Display(Name = "Assistant Manager Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1amw { get; set; }

        // 11.0
        /// <summary>
        /// Assistant Manager Cell Phone
        /// </summary>
        [Display(Name = "Assistant Manager Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1amc { get; set; }

        // 11.0
        /// <summary>
        /// Assistant Manager Home Phone
        /// </summary>
        [Display(Name = "Assistant Manager Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1amh { get; set; }

        // 30
        /// <summary>
        /// Office Manager
        /// </summary>
        [Display(Name = "Office Manager")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Office Manager cannot be longer than 30 characters.")]
        public string ct1omn { get; set; }

        // 11.0
        /// <summary>
        /// Office Manager Work Phone
        /// </summary>
        [Display(Name = "Office Manager Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1omw { get; set; }

        // 11.0
        /// <summary>
        /// Office Manager Cell Phone
        /// </summary>
        [Display(Name = "Office Manager Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1omc { get; set; }

        // 11.0
        /// <summary>
        /// Office Manager Home Phone
        /// </summary>
        [Display(Name = "Office Manager Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1omh { get; set; }

        // 30
        /// <summary>
        /// Sales Manager
        /// </summary>
        [Display(Name = "Sales Manager")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Sales Manager cannot be longer than 30 characters.")]
        public string ct1smn { get; set; }

        // 11.0
        /// <summary>
        /// Sales Manager Work Phone
        /// </summary>
        [Display(Name = "Sales Manager Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1smw { get; set; }

        // 11.0
        /// <summary>
        /// Sales Manager Cell Phone
        /// </summary>
        [Display(Name = "Sales Manager Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1smc { get; set; }

        // 11.0
        /// <summary>
        /// Sales Manager Home Phone
        /// </summary>
        [Display(Name = "Sales Manager Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1smh { get; set; }

        // 30
        /// <summary>
        /// Dock Supervisor
        /// </summary>
        [Display(Name = "Dock Supervisor")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Dock Supervisor cannot be longer than 30 characters.")]
        public string ct1dsn { get; set; }

        // 11.0
        /// <summary>
        /// Dock Supervisor Work Phone
        /// </summary>
        [Display(Name = "Dock Supervisor Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1dsw { get; set; }

        // 11.0
        /// <summary>
        /// Dock Supervisor Cell Phone
        /// </summary>
        [Display(Name = "Dock Supervisor Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1dsc { get; set; }

        // 11.0
        /// <summary>
        /// Dock Supervisor Home Phone
        /// </summary>
        [Display(Name = "Dock Supervisor Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1dsh { get; set; }

        // 30
        /// <summary>
        /// Linehaul Supervisor
        /// </summary>
        [Display(Name = "Linehaul Supervisor")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Linehaul Supervisor cannot be longer than 30 characters.")]
        public string ct1hsn { get; set; }

        // 11.0
        /// <summary>
        /// Linehaul Supervisor Work Phone
        /// </summary>
        [Display(Name = "Linehaul Supervisor Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1hsw { get; set; }

        // 11.0
        /// <summary>
        /// Linehaul Supervisor Cell Phone
        /// </summary>
        [Display(Name = "Linehaul Supervisor Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1hsc { get; set; }

        // 11.0
        /// <summary>
        /// Linehaul Supervisor Home Phone
        /// </summary>
        [Display(Name = "Linehaul Supervisor Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1hsh { get; set; }

        // 30
        /// <summary>
        /// Safety Supervisor
        /// </summary>
        [Display(Name = "Safety Supervisor")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Safety Supervisor cannot be longer than 30 characters.")]
        public string ct1tsn { get; set; }

        // 11.0
        /// <summary>
        /// Safety Supervisor Work Phone
        /// </summary>
        [Display(Name = "Safety Supervisor Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1tsw { get; set; }

        // 11.0
        /// <summary>
        /// Safety Supervisor Cell Phone
        /// </summary>
        [Display(Name = "Safety Supervisor Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1tsc { get; set; }

        // 11.0
        /// <summary>
        /// Safety Supervisor Home Phone
        /// </summary>
        [Display(Name = "Safety Supervisor Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1tsh { get; set; }

        // 30
        /// <summary>
        /// OS&D Supervisor
        /// </summary>
        [Display(Name = "Osandd Supervisor")]
        [DefaultValue("")]
        [StringLength(30, ErrorMessage = "Osandd Supervisor cannot be longer than 30 characters.")]
        public string ct1osn { get; set; }

        // 11.0
        /// <summary>
        /// OS&D Supervisor Work Phone
        /// </summary>
        [Display(Name = "Osandd Supervisor Work Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1osw { get; set; }

        // 11.0
        /// <summary>
        /// OS&D Supervisor Cell Phone
        /// </summary>
        [Display(Name = "Osandd Supervisor Cell Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1osc { get; set; }

        // 11.0
        /// <summary>
        /// OS&D Supervisor Home Phone
        /// </summary>
        [Display(Name = "Osandd Supervisor Home Phone")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1osh { get; set; }

        // 2
        /// <summary>
        /// Terminal Region
        /// </summary>
        [Display(Name = "Terminal Region")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Terminal Region cannot be longer than 2 characters.")]
        public string ct1reg { get; set; }

        // 5
        /// <summary>
        /// Controlling Terminal ID
        /// </summary>
        [Display(Name = "Controlling Terminal ID")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Controlling Terminal ID cannot be longer than 5 characters.")]
        public string ct1ct { get; set; }

        // 2
        /// <summary>
        /// Terminal Type
        /// </summary>
        [Display(Name = "Terminal Type")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Terminal Type cannot be longer than 2 characters.")]
        public string ct1typ { get; set; }

        // 7
        /// <summary>
        /// Remit-To Account Code
        /// </summary>
        [Display(Name = "Remit-To Account Code")]
        [DefaultValue("")]
        [StringLength(7, ErrorMessage = "Remit-To Account Code cannot be longer than 7 characters.")]
        public string ct1rta { get; set; }

        // 3.2
        /// <summary>
        /// Time Zone Offset
        /// </summary>
        [Display(Name = "Time Zone Offset")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal ct1tzc { get; set; }

        // 3.0
        /// <summary>
        /// Company Number
        /// </summary>
        [Display(Name = "Company Number")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal ct1co { get; set; }

        // 3.0
        /// <summary>
        /// Division Number
        /// </summary>
        [Display(Name = "Division Number")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal ct1div { get; set; }

        // 2
        /// <summary>
        /// Computer Equipment Type
        /// </summary>
        [Display(Name = "Computer Equipment Type")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Computer Equipment Type cannot be longer than 2 characters.")]
        public string ct1et { get; set; }

        // 7.0
        /// <summary>
        /// Operating Date
        /// </summary>
        [Display(Name = "Operating Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal ct1dat { get; set; }

        // 4.0
        /// <summary>
        /// Inbound Cutoff Time
        /// </summary>
        [Display(Name = "Inbound Cutoff Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1ibc { get; set; }

        // 4.0
        /// <summary>
        /// Earliest Delivery Appointment
        /// </summary>
        [Display(Name = "Earliest Delivery Appointment")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1eda { get; set; }

        // 4.0
        /// <summary>
        /// Pickup Cutoff Time
        /// </summary>
        [Display(Name = "Pickup Cutoff Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1puc { get; set; }

        // 4.0
        /// <summary>
        /// M-F Opening Time
        /// </summary>
        [Display(Name = "M-F Opening Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1wot { get; set; }

        // 4.0
        /// <summary>
        /// M-F Closing Time
        /// </summary>
        [Display(Name = "M-F Closing Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1wct { get; set; }

        // 4.0
        /// <summary>
        /// Saturday Opening Time
        /// </summary>
        [Display(Name = "Saturday Opening Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1aot { get; set; }

        // 4.0
        /// <summary>
        /// Saturday Closing Time
        /// </summary>
        [Display(Name = "Saturday Closing Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1act { get; set; }

        // 4.0
        /// <summary>
        /// Sunday Opening Time
        /// </summary>
        [Display(Name = "Sunday Opening Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1uot { get; set; }

        // 4.0
        /// <summary>
        /// Sunday Closing Time
        /// </summary>
        [Display(Name = "Sunday Closing Time")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1uct { get; set; }

        // 3.0
        /// <summary>
        /// Number Of Dock Doors
        /// </summary>
        [Display(Name = "Number Of Dock Doors")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal ct1dd { get; set; }

        // 5.0
        /// <summary>
        /// Assigned Power Units
        /// </summary>
        [Display(Name = "Assigned Power Units")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal ct1apu { get; set; }

        // 5.0
        /// <summary>
        /// Assigned Trailers
        /// </summary>
        [Display(Name = "Assigned Trailers")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal ct1atl { get; set; }

        // 5.0
        /// <summary>
        /// Assigned Dollies
        /// </summary>
        [Display(Name = "Assigned Dollies")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal ct1adl { get; set; }

        // 5.0
        /// <summary>
        /// Domiciled Road Drivers
        /// </summary>
        [Display(Name = "Domiciled Road Drivers")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal ct1drd { get; set; }

        // 5.0
        /// <summary>
        /// Domiciled City Drivers
        /// </summary>
        [Display(Name = "Domiciled City Drivers")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal ct1dcd { get; set; }

        // 10
        /// <summary>
        /// Fuel Service
        /// </summary>
        [Display(Name = "Fuel Service")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Fuel Service cannot be longer than 10 characters.")]
        public string ct1ft { get; set; }

        // 10
        /// <summary>
        /// Guard Service
        /// </summary>
        [Display(Name = "Guard Service")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Guard Service cannot be longer than 10 characters.")]
        public string ct1gs { get; set; }

        // 4.0
        /// <summary>
        /// Fiscal Year
        /// </summary>
        [Display(Name = "Fiscal Year")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal ct1fy { get; set; }

        // 1.0
        /// <summary>
        /// Fiscal Quarter
        /// </summary>
        [Display(Name = "Fiscal Quarter")]
        [DefaultValue(0)]
        [Range(0, 9)]
        public decimal ct1fq { get; set; }

        // 2.0
        /// <summary>
        /// Fiscal Period
        /// </summary>
        [Display(Name = "Fiscal Period")]
        [DefaultValue(0)]
        [Range(0, 99)]
        public decimal ct1fp { get; set; }

        // 2.0
        /// <summary>
        /// Fiscal Week
        /// </summary>
        [Display(Name = "Fiscal Week")]
        [DefaultValue(0)]
        [Range(0, 99)]
        public decimal ct1fw { get; set; }

        // 10
        /// <summary>
        /// Default Message CRT
        /// </summary>
        [Display(Name = "Default Message CRT")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Default Message CRT cannot be longer than 10 characters.")]
        public string ct1mc { get; set; }

        // 11.0
        /// <summary>
        /// Last Freight Bill Number Used
        /// </summary>
        [Display(Name = "Last Freight Bill Number Used")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ct1lfb { get; set; }

        // 9.0
        /// <summary>
        /// Last Manifest Number Used
        /// </summary>
        [Display(Name = "Last Manifest Number Used")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal ct1lmn { get; set; }

        // 9.0
        /// <summary>
        /// Last OS&D Number Used
        /// </summary>
        [Display(Name = "Last Osandd Number Used")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal ct1los { get; set; }

        // 9.0
        /// <summary>
        /// Last Form Number Used
        /// </summary>
        [Display(Name = "Last Form Number Used")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal ct1lfn { get; set; }

        // 9.0
        /// <summary>
        /// Last Sequence Number Used
        /// </summary>
        [Display(Name = "Last Sequence Number Used")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal ct1lsn { get; set; }

        // 10
        /// <summary>
        /// Reports (Printer 1)
        /// </summary>
        [Display(Name = "Reports (Printer 1)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Reports (Printer 1) cannot be longer than 10 characters.")]
        public string ct1p1 { get; set; }

        // 4
        /// <summary>
        /// Reports Printer CPI
        /// </summary>
        [Display(Name = "Reports Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Reports Printer CPI cannot be longer than 4 characters.")]
        public string ct1p1d { get; set; }

        // 10
        /// <summary>
        /// Reports Output Queue
        /// </summary>
        [Display(Name = "Reports Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Reports Output Queue cannot be longer than 10 characters.")]
        public string ct1p1o { get; set; }

        // 10
        /// <summary>
        /// Delivery Receipts (Printer 2)
        /// </summary>
        [Display(Name = "Delivery Receipts (Printer 2)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Delivery Receipts (Printer 2) cannot be longer than 10 characters.")]
        public string ct1p2 { get; set; }

        // 4
        /// <summary>
        /// Delivery Receipts Printer CPI
        /// </summary>
        [Display(Name = "Delivery Receipts Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Delivery Receipts Printer CPI cannot be longer than 4 characters.")]
        public string ct1p2d { get; set; }

        // 10
        /// <summary>
        /// Delivery Receipts Output Queue
        /// </summary>
        [Display(Name = "Delivery Receipts Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Delivery Receipts Output Queue cannot be longer than 10 characters.")]
        public string ct1p2o { get; set; }

        // 10
        /// <summary>
        /// Working Copies (Printer 3)
        /// </summary>
        [Display(Name = "Working Copies (Printer 3)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Working Copies (Printer 3) cannot be longer than 10 characters.")]
        public string ct1p3 { get; set; }

        // 4
        /// <summary>
        /// Working Copies Printer CPI
        /// </summary>
        [Display(Name = "Working Copies Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Working Copies Printer CPI cannot be longer than 4 characters.")]
        public string ct1p3d { get; set; }

        // 10
        /// <summary>
        /// Working Copies Output Queue
        /// </summary>
        [Display(Name = "Working Copies Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Working Copies Output Queue cannot be longer than 10 characters.")]
        public string ct1p3o { get; set; }

        // 10
        /// <summary>
        /// Manifests (Printer 4)
        /// </summary>
        [Display(Name = "Manifests (Printer 4)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Manifests (Printer 4) cannot be longer than 10 characters.")]
        public string ct1p4 { get; set; }

        // 4
        /// <summary>
        /// Manifests Printer CPI
        /// </summary>
        [Display(Name = "Manifests Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Manifests Printer CPI cannot be longer than 4 characters.")]
        public string ct1p4d { get; set; }

        // 10
        /// <summary>
        /// Manifests Output Queue
        /// </summary>
        [Display(Name = "Manifests Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Manifests Output Queue cannot be longer than 10 characters.")]
        public string ct1p4o { get; set; }

        // 10
        /// <summary>
        /// Messages (Printer 5)
        /// </summary>
        [Display(Name = "Messages (Printer 5)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Messages (Printer 5) cannot be longer than 10 characters.")]
        public string ct1p5 { get; set; }

        // 4
        /// <summary>
        /// Messages Printer CPI
        /// </summary>
        [Display(Name = "Messages Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Messages Printer CPI cannot be longer than 4 characters.")]
        public string ct1p5d { get; set; }

        // 10
        /// <summary>
        /// Messages Output Queue
        /// </summary>
        [Display(Name = "Messages Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Messages Output Queue cannot be longer than 10 characters.")]
        public string ct1p5o { get; set; }

        // 10
        /// <summary>
        /// OS&D (Printer 6)
        /// </summary>
        [Display(Name = "Osandd (Printer 6)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Osandd (Printer 6) cannot be longer than 10 characters.")]
        public string ct1p6 { get; set; }

        // 4
        /// <summary>
        /// OS&D Printer CPI
        /// </summary>
        [Display(Name = "Osandd Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Osandd Printer CPI cannot be longer than 4 characters.")]
        public string ct1p6d { get; set; }

        // 10
        /// <summary>
        /// OS&D Output Queue
        /// </summary>
        [Display(Name = "Osandd Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Osandd Output Queue cannot be longer than 10 characters.")]
        public string ct1p6o { get; set; }

        // 10
        /// <summary>
        /// Sales (Printer 7
        /// </summary>
        [Display(Name = "Sales (Printer 7")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Sales (Printer 7 cannot be longer than 10 characters.")]
        public string ct1p7 { get; set; }

        // 4
        /// <summary>
        /// Sales Printer CPI
        /// </summary>
        [Display(Name = "Sales Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Sales Printer CPI cannot be longer than 4 characters.")]
        public string ct1p7d { get; set; }

        // 10
        /// <summary>
        /// Sales Output Queue
        /// </summary>
        [Display(Name = "Sales Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Sales Output Queue cannot be longer than 10 characters.")]
        public string ct1p7o { get; set; }

        // 10
        /// <summary>
        /// Pickup Calls (Printer 8
        /// </summary>
        [Display(Name = "Pickup Calls (Printer 8")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Pickup Calls (Printer 8 cannot be longer than 10 characters.")]
        public string ct1p8 { get; set; }

        // 4
        /// <summary>
        /// Pickup Calls Printer CPI
        /// </summary>
        [Display(Name = "Pickup Calls Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Pickup Calls Printer CPI cannot be longer than 4 characters.")]
        public string ct1p8d { get; set; }

        // 10
        /// <summary>
        /// Pickup Calls Output Queue
        /// </summary>
        [Display(Name = "Pickup Calls Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Pickup Calls Output Queue cannot be longer than 10 characters.")]
        public string ct1p8o { get; set; }

        // 10
        /// <summary>
        /// Spare (Printer 9
        /// </summary>
        [Display(Name = "Spare (Printer 9")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Spare (Printer 9 cannot be longer than 10 characters.")]
        public string ct1p9 { get; set; }

        // 4
        /// <summary>
        /// Spare Printer CPI
        /// </summary>
        [Display(Name = "Spare Printer CPI")]
        [DefaultValue("")]
        [StringLength(4, ErrorMessage = "Spare Printer CPI cannot be longer than 4 characters.")]
        public string ct1p9d { get; set; }

        // 10
        /// <summary>
        /// Spare Output Queue
        /// </summary>
        [Display(Name = "Spare Output Queue")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Spare Output Queue cannot be longer than 10 characters.")]
        public string ct1p9o { get; set; }

        // 7.0
        /// <summary>
        /// Added On (Date)
        /// </summary>
        [Display(Name = "Added On (Date)")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal ct1adt { get; set; }

        // 6.0
        /// <summary>
        /// Added At (Time)
        /// </summary>
        [Display(Name = "Added At (Time)")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal ct1atm { get; set; }

        // 10
        /// <summary>
        /// Added By (User)
        /// </summary>
        [Display(Name = "Added By (User)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Added By (User) cannot be longer than 10 characters.")]
        public string ct1aup { get; set; }

        // 7.0
        /// <summary>
        /// Changed On (Date)
        /// </summary>
        [Display(Name = "Changed On (Date)")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal ct1udt { get; set; }

        // 6.0
        /// <summary>
        /// Changed At (Time)
        /// </summary>
        [Display(Name = "Changed At (Time)")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal ct1utm { get; set; }

        // 10
        /// <summary>
        /// Changed By (User)
        /// </summary>
        [Display(Name = "Changed By (User)")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Changed By (User) cannot be longer than 10 characters.")]
        public string ct1uup { get; set; }



        #endregion

    }
}
﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;


namespace FinalMileAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class FRP002
    {
        /// <summary>
        /// base select on pro
        /// </summary>
        public const string SQL_SELECT_FDPRO = "select fdpro, fdlin, fdar, fdpcs, fdcmcl, fdhaz, fdpkgc, fddes, fdwgt, fdrat, fdptb, fdmin, fdamt, fdpost, fdacls, fdudat, fdutim, fduid from FRP002 where fdpro = {0} ";

        /// <summary>
        /// base insert
        /// </summary>
        //private const string SQL_INSERT = "insert into FRP002 (fdpro, fdlin, fdar, fdpcs, fdcmcl, fdhaz, fdpkgc, fddes, fdwgt, fdrat, fdptb, fdmin, fdamt, fdpost, fdacls, fdudat, fdutim, fduid) values ({0}, {1}, '{2}', {3}, '{4}', '{5}', '{6}', '{7}', {8}, {9}, '{10}', '{11}', {12}, {13}, '{14}', {15}, {16}, '{17}') ";

        /// <summary>
        /// base update on pro
        /// </summary>
        private const string SQL_UPDATE = "update FRP002 set fdlin = {1}, fdar = '{2}', fdpcs = {3}, fdcmcl = '{4}', fdhaz = '{5}', fdpkgc = '{6}', fddes = '{7}', fdwgt = {8}, fdrat = {9}, fdptb = '{10}', fdmin = '{11}', fdamt = {12}, fdpost = {13}, fdacls = '{14}', fdudat = {15}, fdutim = {16}, fduid = '{17}' where fdpro = {0} ";

        /// <summary>
        /// base delete
        /// </summary>
        //private const string SQL_DELETE = "delete from FRP002 where *** ";

        /// <summary>
        /// default constructor
        /// </summary>
        public FRP002() { }

        /// <summary>
        /// preferred constructor
        /// </summary>
        /// <param name="row"></param>
        public FRP002(DataRow row)
        {

            fdpro = Convert.ToDecimal(row["fdpro"]);

            fdlin = Convert.ToDecimal(row["fdlin"]);

            fdar = row["fdar"].ToString().Trim();

            fdpcs = Convert.ToDecimal(row["fdpcs"]);

            fdcmcl = row["fdcmcl"].ToString().Trim();

            fdhaz = row["fdhaz"].ToString().Trim();

            fdpkgc = row["fdpkgc"].ToString().Trim();

            fddes = row["fddes"].ToString().Trim();

            fdwgt = Convert.ToDecimal(row["fdwgt"]);

            fdrat = Convert.ToDecimal(row["fdrat"]);

            fdptb = row["fdptb"].ToString().Trim();

            fdmin = row["fdmin"].ToString().Trim();

            fdamt = Convert.ToDecimal(row["fdamt"]);

            fdpost = Convert.ToDecimal(row["fdpost"]);

            fdacls = row["fdacls"].ToString().Trim();

            fdudat = Convert.ToDecimal(row["fdudat"]);

            fdutim = Convert.ToDecimal(row["fdutim"]);

            fduid = row["fduid"].ToString().Trim();

        }


        /// <summary>
        /// array constructor
        /// </summary>
        /// <param name="values">construct from ToArray() of self</param>
        public FRP002(string[] values)
        {

            fdpro = Convert.ToDecimal(values[0]);

            fdlin = Convert.ToDecimal(values[1]);

            fdar = values[2];

            fdpcs = Convert.ToDecimal(values[3]);

            fdcmcl = values[4];

            fdhaz = values[5];

            fdpkgc = values[6];

            fddes = values[7];

            fdwgt = Convert.ToDecimal(values[8]);

            fdrat = Convert.ToDecimal(values[9]);

            fdptb = values[10];

            fdmin = values[11];

            fdamt = Convert.ToDecimal(values[12]);

            fdpost = Convert.ToDecimal(values[13]);

            fdacls = values[14];

            fdudat = Convert.ToDecimal(values[15]);

            fdutim = Convert.ToDecimal(values[16]);

            fduid = values[17];

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>an array of the class properties</returns>
        public string[] ToArray()
        {
            return new string[] { fdpro.ToString(), fdlin.ToString(), fdar, fdpcs.ToString(), fdcmcl, fdhaz, fdpkgc, fddes, fdwgt.ToString(), fdrat.ToString(), fdptb, fdmin, fdamt.ToString(), fdpost.ToString(), fdacls, fdudat.ToString(), fdutim.ToString(), fduid };
        }


        #region properties


        // 11.0
        /// <summary>
        /// Pro Number
        /// </summary>
        [Display(Name = "Pro Number")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal fdpro { get; set; }

        // 3.0
        /// <summary>
        /// Line Number
        /// </summary>
        [Display(Name = "Line Number")]
        [DefaultValue(0)]
        [Range(0, 999)]
        public decimal fdlin { get; set; }

        // 1
        /// <summary>
        /// Automatic Rating Line
        /// </summary>
        [Display(Name = "Automatic Rating Line")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Automatic Rating Line cannot be longer than 1 characters.")]
        public string fdar { get; set; }

        // 5.0
        /// <summary>
        /// Handling Units
        /// </summary>
        [Display(Name = "Handling Units")]
        [DefaultValue(0)]
        [Range(0, 99999)]
        public decimal fdpcs { get; set; }

        // 6
        /// <summary>
        /// Commodity Code/Work Field
        /// </summary>
        [Display(Name = "Commodity Code/Work Field")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Commodity Code/Work Field cannot be longer than 6 characters.")]
        public string fdcmcl { get; set; }

        // 1
        /// <summary>
        /// Hazardous Materials Code
        /// </summary>
        [Display(Name = "Hazardous Materials Code")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Hazardous Materials Code cannot be longer than 1 characters.")]
        public string fdhaz { get; set; }

        // 2
        /// <summary>
        /// Package Code
        /// </summary>
        [Display(Name = "Package Code")]
        [DefaultValue("")]
        [StringLength(2, ErrorMessage = "Package Code cannot be longer than 2 characters.")]
        public string fdpkgc { get; set; }

        // 35
        /// <summary>
        /// Item Description
        /// </summary>
        [Display(Name = "Item Description")]
        [DefaultValue("")]
        [StringLength(35, ErrorMessage = "Item Description cannot be longer than 35 characters.")]
        public string fddes { get; set; }

        // 6.0
        /// <summary>
        /// Weight
        /// </summary>
        [Display(Name = "Weight")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal fdwgt { get; set; }

        // 7.2
        /// <summary>
        /// Hundredweight Rate
        /// </summary>
        [Display(Name = "Hundredweight Rate")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fdrat { get; set; }

        // 1
        /// <summary>
        /// Party To Bill
        /// </summary>
        [Display(Name = "Party To Bill")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Party To Bill cannot be longer than 1 characters.")]
        public string fdptb { get; set; }

        // 1
        /// <summary>
        /// Special Rate Field
        /// </summary>
        [Display(Name = "Special Rate Field")]
        [DefaultValue("")]
        [StringLength(1, ErrorMessage = "Special Rate Field cannot be longer than 1 characters.")]
        public string fdmin { get; set; }

        // 7.2
        /// <summary>
        /// Extended Amount
        /// </summary>
        [Display(Name = "Extended Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fdamt { get; set; }

        // 7.2
        /// <summary>
        /// Posted Amount
        /// </summary>
        [Display(Name = "Posted Amount")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fdpost { get; set; }

        // 6
        /// <summary>
        /// Actual/Rated-As Class
        /// </summary>
        [Display(Name = "Actual/Rated-As Class")]
        [DefaultValue("")]
        [StringLength(6, ErrorMessage = "Actual/Rated-As Class cannot be longer than 6 characters.")]
        public string fdacls { get; set; }

        // 7.0
        /// <summary>
        /// Last Changed, Date
        /// </summary>
        [Display(Name = "Last Changed, Date")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fdudat { get; set; }

        // 6.0
        /// <summary>
        /// Last Changed, Time
        /// </summary>
        [Display(Name = "Last Changed, Time")]
        [DefaultValue(0)]
        [Range(0, 999999)]
        public decimal fdutim { get; set; }

        // 10
        /// <summary>
        /// Last Changed, User Profile
        /// </summary>
        [Display(Name = "Last Changed, User Profile")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "Last Changed, User Profile cannot be longer than 10 characters.")]
        public string fduid { get; set; }



        #endregion

    }
}
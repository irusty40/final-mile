﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;


namespace FinalMileAPI
{
    /// <summary>
    /// 
    /// </summary>
    public class FRP003
    {
        /// <summary>
        /// select - where fccpro = {0} 
        /// </summary>
        //private const string SQL_SELECT_FCPRO = "select fcpro, fcterm, fcdat, fctim, fccod, fcmseq, fctrip, fccomm, fcuser from FRP003 where fcpro = {0} ";

        /// <summary>
        /// select - where fccpro = {0} and fccod = 'DT'
        /// </summary>
        public const string SQL_SELECT_DT = "select fcpro, fcterm, fcdat, fctim, fccod, fcmseq, fctrip, fccomm, fcuser from FRP003 where fcpro = {0} and fccod = 'DT' ";

        /// <summary>
        /// base insert
        /// </summary>
        //private const string SQL_INSERT = "insert into FRP003 (fcpro, fcterm, fcdat, fctim, fccod, fcmseq, fctrip, fccomm, fcuser) values ({0}, '{1}', {2}, {3}, '{4}', {5}, {6}, '{7}', '{8}') ";

        /// <summary>
        /// base update
        /// </summary>
        //private const string SQL_UPDATE = "update FRP003 set fcpro = {0}, fcterm = '{1}', fcdat = {2}, fctim = {3}, fccod = '{4}', fcmseq = {5}, fctrip = {6}, fccomm = '{7}', fcuser = '{8}' where *** ";

        /// <summary>
        /// base delete
        /// </summary>
        //private const string SQL_DELETE = "delete from FRP003 where *** ";

        /// <summary>
        /// default contructor
        /// </summary>
        public FRP003() { }

        /// <summary>
        /// preferred contructor
        /// </summary>
        /// <param name="row"></param>
        public FRP003(DataRow row)
        {

            fcpro = Convert.ToDecimal(row["fcpro"]);

            fcterm = row["fcterm"].ToString().Trim();

            fcdat = Convert.ToDecimal(row["fcdat"]);

            fctim = Convert.ToDecimal(row["fctim"]);

            fccod = row["fccod"].ToString().Trim();

            fcmseq = Convert.ToDecimal(row["fcmseq"]);

            fctrip = Convert.ToDecimal(row["fctrip"]);

            fccomm = row["fccomm"].ToString().Trim();

            fcuser = row["fcuser"].ToString().Trim();

        }


        /// <summary>
        /// array contructor
        /// </summary>
        /// <param name="values">construct from ToArray() of self</param>
        public FRP003(string[] values)
        {

            fcpro = Convert.ToDecimal(values[0]);

            fcterm = values[1];

            fcdat = Convert.ToDecimal(values[2]);

            fctim = Convert.ToDecimal(values[3]);

            fccod = values[4];

            fcmseq = Convert.ToDecimal(values[5]);

            fctrip = Convert.ToDecimal(values[6]);

            fccomm = values[7];

            fcuser = values[8];

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>an array of the class properties</returns>
        public string[] ToArray()
        {
            return new string[] { fcpro.ToString(), fcterm, fcdat.ToString(), fctim.ToString(), fccod, fcmseq.ToString(), fctrip.ToString(), fccomm, fcuser };
        }


        #region properties


        // 11.0
        /// <summary>
        /// Pro Number
        /// </summary>
        [Display(Name = "Pro Number")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal fcpro { get; set; }

        // 5
        /// <summary>
        /// Terminal For This Activity
        /// </summary>
        [Display(Name = "Terminal For This Activity")]
        [DefaultValue("")]
        [StringLength(5, ErrorMessage = "Terminal For This Activity cannot be longer than 5 characters.")]
        public string fcterm { get; set; }

        // 7.0
        /// <summary>
        /// Date Of Activity
        /// </summary>
        [Display(Name = "Date Of Activity")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fcdat { get; set; }

        // 4.0
        /// <summary>
        /// Time Of Activity, HHMM
        /// </summary>
        [Display(Name = "Time Of Activity, HHMM")]
        [DefaultValue(0)]
        [Range(0, 9999)]
        public decimal fctim { get; set; }

        // 3
        /// <summary>
        /// Transaction Code
        /// </summary>
        [Display(Name = "Transaction Code")]
        [DefaultValue("")]
        [StringLength(3, ErrorMessage = "Transaction Code cannot be longer than 3 characters.")]
        public string fccod { get; set; }

        // 9.0
        /// <summary>
        /// Manifest Number
        /// </summary>
        [Display(Name = "Manifest Number")]
        [DefaultValue(0)]
        [Range(0, 999999999)]
        public decimal fcmseq { get; set; }

        // 7.0
        /// <summary>
        /// Dispatch Number
        /// </summary>
        [Display(Name = "Dispatch Number")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fctrip { get; set; }

        // 40
        /// <summary>
        /// Comment
        /// </summary>
        [Display(Name = "Comment")]
        [DefaultValue("")]
        [StringLength(40, ErrorMessage = "Comment cannot be longer than 40 characters.")]
        public string fccomm { get; set; }

        // 10
        /// <summary>
        /// User Profile
        /// </summary>
        [Display(Name = "User Profile")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "User Profile cannot be longer than 10 characters.")]
        public string fcuser { get; set; }



        #endregion

    }
}
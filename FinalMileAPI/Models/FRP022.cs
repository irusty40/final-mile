﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;


namespace FinalMileAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class FRP022
    {
        /// <summary>
        /// base select
        /// </summary>
        public const string SQL_SELECT = "select ffpro, ffodr, ffsts, fflrdt, fflsts, ffadd, ffimg from FRP022 where ffpro = {0} ";

        /// <summary>
        /// base insert
        /// </summary>
        public const string SQL_INSERT = "insert into FRP022 (ffpro, ffodr, ffsts, fflrdt, fflsts, ffadd, ffimg) values ({0}, {1}, '{2}', {3}, '{4}', '{5}', '{6}') ";

        /// <summary>
        /// base update
        /// </summary>
        public const string SQL_UPDATE = "update FRP022 set ffodr = {1}, ffsts = '{2}', fflrdt = {3}, fflsts = '{4}', ffadd = '{5}', ffimg = '{6}' where ffpro = {0} ";

        /// <summary>
        /// base delete
        /// </summary>
        private const string SQL_DELETE = "delete from FRP022 where *** ";

        /// <summary>
        /// default contructor
        /// </summary>
        public FRP022() { }

        /// <summary>
        /// preferred contructor
        /// </summary>
        /// <param name="row"></param>
        public FRP022(DataRow row)
        {

            ffpro = Convert.ToDecimal(row["ffpro"]);

            ffodr = Convert.ToDecimal(row["ffodr"]);

            ffsts = row["ffsts"].ToString().Trim();

            fflrdt = Convert.ToDecimal(row["fflrdt"]);

            fflsts = row["fflsts"].ToString().Trim();

            if (string.IsNullOrEmpty(fflsts.Trim())) fflsts = DateTime.MinValue.ToString("YYYY-MM-dd-HH.mm.ss.fff000"); // 2018-09-27-14.06.41.648000'

            ffadd = row["ffadd"].ToString().Trim();

            if (string.IsNullOrEmpty(ffadd.Trim())) ffadd = DateTime.MinValue.ToString("YYYY-MM-dd-HH.mm.ss.fff000"); // 2018-09-27-14.06.41.648000'

            ffimg = row["ffimg"].ToString().Trim();

            if (string.IsNullOrEmpty(ffimg.Trim())) ffimg = DateTime.MinValue.ToString("YYYY-MM-dd-HH.mm.ss.fff000"); // 2018-09-27-14.06.41.648000'

        }


        /// <summary>
        /// array contructor
        /// </summary>
        /// <param name="values">construct from ToArray() of self</param>
        public FRP022(string[] values)
        {

            ffpro = Convert.ToDecimal(values[0]);

            ffodr = Convert.ToDecimal(values[1]);

            ffsts = values[2];

            fflrdt = Convert.ToDecimal(values[3]);

            fflsts = values[4];

            if (string.IsNullOrEmpty(fflsts.Trim())) fflsts = DateTime.MinValue.ToString("YYYY-MM-dd-HH.mm.ss.fff000"); // 2018-09-27-14.06.41.648000'

            ffadd = values[5];

            if (string.IsNullOrEmpty(ffadd.Trim())) ffadd = DateTime.MinValue.ToString("YYYY-MM-dd-HH.mm.ss.fff000"); // 2018-09-27-14.06.41.648000'

            ffimg = values[6];

            if (string.IsNullOrEmpty(ffimg.Trim())) ffimg = DateTime.MinValue.ToString("YYYY-MM-dd-HH.mm.ss.fff000"); // 2018-09-27-14.06.41.648000'

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>an array of the class properties</returns>
        public string[] ToArray()
        {
            return new string[] { ffpro.ToString(), ffodr.ToString(), ffsts, fflrdt.ToString(), fflsts, ffadd, ffimg };
        }


        #region properties


        // 11.0
        /// <summary>
        /// SAIA PRO
        /// </summary>
        [Display(Name = "Saia Pro")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ffpro { get; set; }

        // 11.0
        /// <summary>
        /// ECOURIER ORDER
        /// </summary>
        [Display(Name = "eCoueries Order Number")]
        [DefaultValue(0)]
        [Range(0, 99999999999)]
        public decimal ffodr { get; set; }

        // 10
        /// <summary>
        /// SHIPMEMT STATUS
        /// </summary>
        [Display(Name = "Shipment Status")]
        [DefaultValue("")]
        [StringLength(10, ErrorMessage = "SHIPMEMT STATUS cannot be longer than 10 characters.")]
        public string ffsts { get; set; }

        // 7.0
        /// <summary>
        /// LAST READY DATE SENT
        /// </summary>
        [Display(Name = "Last Ready Date Sent")]
        [DefaultValue(0)]
        [Range(0, 9999999)]
        public decimal fflrdt { get; set; }

        // 10.
        /// <summary>
        /// LAST STATUS UPDATE
        /// </summary>
        [Display(Name = "Last Status Update")]
        [DefaultValue("")]
        public string fflsts { get; set; }

        // 10.
        /// <summary>
        /// ADDED TO FILE
        /// </summary>
        [Display(Name = "Added To File")]
        [DefaultValue("")]
        public string ffadd { get; set; }

        // 10.
        /// <summary>
        /// IMAGE RECEIVED
        /// </summary>
        [Display(Name = "Image Recieved")]
        [DefaultValue("")]
        public string ffimg { get; set; }



        #endregion

    }
}
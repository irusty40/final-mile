﻿using System;
using System.ComponentModel.DataAnnotations;
using IBM.Data.DB2.iSeries;
using Microsoft.VisualBasic;
using System.Net;

namespace FinalMileAPI.Models
{
    public class INPLOG
    {
        private const string DEFAULT_DETAILKEY = "FINALMILE";

        private const string DEFAULT_LOG_USER = "FINALMILE";

        private const string SQL_INSERT = "insert into inplog (gldate, gltime, glkey, glak1, glak2, glnk1, glnk2, glad1, glad2, glad3, glad4, glad5, glnd1, glnd2, glnd3, glnd4, glnd5, glipad, glserv, glnote, gluser) values ({0}, {1}, '{2}', '{3}', '{4}', {5}, {6}, '{7}', '{8}', '{9}', '{10}', '{11}', {12}, {13}, {14}, {15}, {16}, '{17}', '{18}', '{19}', '{20}')  ";

        private string[] values = new string[(int)Columns.Count];

        private readonly int[] sizes = { 7, 6, 10, 10, 10, 15, 15, 10, 10, 20, 20, 50, 7, 7, 9, 9, 15, 30, 30, 250, 10 };

        private readonly int[] types = { 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0 };

        public System.Text.StringBuilder ErrorMessage = new System.Text.StringBuilder();

        public enum Columns
        {
            GLDATE,
            GLTIME,
            GLKEY,
            GLAK1,
            GLAK2,
            GLNK1,
            GLNK2,
            GLAD1,
            GLAD2,
            GLAD3,
            GLAD4,
            GLAD5,
            GLND1,
            GLND2,
            GLND3,
            GLND4,
            GLND5,
            GLIPAD,
            GLSERV,
            GLNOTE,
            GLUSER,
            Count
        }

        public INPLOG() { } 

        /// <summary>
        /// default constructor 
        /// </summary>
        /// <param name="_detailkey">FINALMILE</param>
        /// <param name="_alphakey1">Alpha Key 1</param>
        /// <param name="_alphakey2">Alpha Key 2</param>
        /// <remarks>
        /// 09/18/2017 : rw
        /// use properties to set additonal values
        /// </remarks>
        public INPLOG(string _detailkey, string _alphakey1, string _alphakey2)
        {
            Initialize();

            DetailKey = _detailkey;

            AlphaKey1 = _alphakey1;

            AlphaKey2 = _alphakey2;
        }

        /// <summary>
        /// preferred constructor for logging user email address changes
        /// </summary>
        /// <param name="_detailkey">value [LMS]</param>
        /// <param name="_alphakey1">value [EMAIL]</param>
        /// <param name="_alphakey2">value [UPDATE]</param>
        /// <param name="_newEmail">user's new email address</param>
        /// <param name="_oldEmail">user's old email address</param>
        /// <param name="_userId">user's employee number or id</param>
        /// <remarks>
        /// 09/18/2017 : rw
        /// use properties to set additonal values
        /// </remarks>
        public INPLOG(string _detailkey, string _alphakey1, string _alphakey2, string _newEmail, string _oldEmail, string _userId)
        {
            Initialize();

            DetailKey = _detailkey;

            AlphaKey1 = _alphakey1;

            AlphaKey2 = _alphakey2;
        }

        public string ClientIPAddress()
        {
            string ipAddress = string.Empty;

            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());

                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        ipAddress = ip.ToString();
                    }
                }
            }
            catch { }

            return ipAddress;
        }

        public bool Add()
        {
            bool added = false;

            System.Text.StringBuilder sql = new System.Text.StringBuilder();

            Exception exx = null;

            try
            {
                ServerName = Environment.MachineName;

                if (string.IsNullOrEmpty(LogUser.Trim())) LogUser = DEFAULT_LOG_USER;

                if (string.IsNullOrEmpty(IPAddress.Trim())) IPAddress = ClientIPAddress();

                SetLogDate();

                Validate();

                sql.AppendFormat(SQL_INSERT, values);

                added = (iSeries.ExecuteNonQuery(sql.ToString(), ref exx) > 0);

            }
            catch (Exception ex)
            {
                ex.HelpLink = sql.ToString();

                ErrorMessage.AppendFormat("{0}<br />", ex.Message);
            }

            return added;

        }

        private void Validate()
        {
            try
            {
                for (int k = 0; k < values.Length; k++)
                {
                    // remove any double apostrophes so it won't jack the length

                    if (values[k].Contains("''")) values[k] = values[k].Replace("''", "'");

                    if (types[k] > 0)
                    {
                        if (!Information.IsNumeric(values[k]))
                        {
                            if (!string.IsNullOrEmpty(values[k].ToString().Trim())) Notes += string.Format("[{0} NaN, replaced with 0.]", values[k].Trim());

                            values[k] = "0";
                        }                    
                    }

                    if (values[k].Trim().Length > sizes[k]) values[k] = values[k].Substring(0, sizes[k]);

                    if (values[k].Contains("'")) values[k] = values[k].Replace("'", "''");
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.AppendFormat("{0}<br />", ex.Message);
            }
        }
        
        private void Initialize()
        {
            for (int k = 0; k < values.Length; k++)
            {
                values[k] = string.Empty;

                if (types[k] > 0) values[k] = "0";
            }

            ErrorMessage = new System.Text.StringBuilder();
        }

        /// <summary>
        /// Sets the log date and time. Subtracts one hour from server time for CST.
        /// </summary>
        private void SetLogDate()
        {
            LogDate = Convert.ToDecimal("1" + DateTime.Now.AddHours(-1).ToString("yyMMdd"));

            LogTime = Convert.ToDecimal(DateTime.Now.AddHours(-1).ToString("HHmmss"));
        }


        /// <summary>
        /// GLDATE (7.0 D)
        /// </summary>
        [Display(Name = "Log Date")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 9999999)]
        public decimal LogDate
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLDATE]);  } 
            set { values[(int)Columns.GLDATE] = value.ToString(); }
        }

        /// <summary>
        /// GLTIME (6 D)
        /// </summary>
        [Display(Name = "Log Time")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 999999)]
        public decimal LogTime
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLTIME]); }
            set { values[(int)Columns.GLTIME] = value.ToString(); }
        }

        /// <summary>
        /// GLKEY (10 C) - default value is DEFAULT_DETAILKEY
        /// </summary>
        [Display(Name = "Detail Key")]
        [MaxLength(10)]
        public string DetailKey
        {
            get { return values[(int)Columns.GLKEY].ToString(); }
            set { values[(int)Columns.GLKEY] = value; }
        }

        /// <summary>
        /// GLAK1 (10 C)
        /// </summary>
        [Display(Name = "Alpha Key 1")]
        [MaxLength(10)]
        public string AlphaKey1
        {
            get { return values[(int)Columns.GLAK1].ToString(); }
            set { values[(int)Columns.GLAK1] = value; }
        }

        /// <summary>
        /// GLAK2 (10 C)
        /// </summary>
        [Display(Name = "Alpha Key 2")]
        [MaxLength(10)]
        public string AlphaKey2
        {
            get { return values[(int)Columns.GLAK2].ToString(); }
            set { values[(int)Columns.GLAK2] = value; }
        }

        /// <summary>
        /// GLNK1 (15.6 D)
        /// </summary>
        [Display(Name = "Numeric Key 1")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 999999999999999.999999)]
        public decimal NumericKey1
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLNK1]); }
            set { values[(int)Columns.GLNK1] = value.ToString(); }
        }

        /// <summary>
        /// GLNK2 (15.6 D)
        /// </summary>
        [Display(Name = "Numeric Key 2")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 999999999999999.999999)]
        public decimal NumericKey2
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLNK2]); }
            set { values[(int)Columns.GLNK2] = value.ToString(); }
        }

        /// <summary>
        /// GLAD1 (10 C)
        /// </summary>
        [Display(Name = "Alpha Data 1")]
        [MaxLength(10)]
        public string AlphaData1
        {
            get { return values[(int)Columns.GLAD1].ToString(); }
            set { values[(int)Columns.GLAD1] = value; }
        }

        /// <summary>
        /// GLAD2 (10 C)
        /// </summary>
        [Display(Name = "Alpha Data 2")]
        [MaxLength(10)]
        public string AlphaData2
        {
            get { return values[(int)Columns.GLAD2].ToString(); }
            set { values[(int)Columns.GLAD2] = value; }
        }

        /// <summary>
        /// GLAD3 (20 C)
        /// </summary>
        [Display(Name = "Alpha Data 3")]
        [MaxLength(20)]
        public string AlphaData3
        {
            get { return values[(int)Columns.GLAD3].ToString(); }
            set { values[(int)Columns.GLAD3] = value; }
        }

        /// <summary>
        /// GLAD4 (20 C)
        /// </summary>
        [Display(Name = "Alpha Data 4")]
        [MaxLength(20)]
        public string AlphaData4
        {
            get { return values[(int)Columns.GLAD4].ToString(); }
            set { values[(int)Columns.GLAD4] = value; }
        }

        /// <summary>
        /// GLAD5 (50 C)
        /// </summary>
        [Display(Name = "Alpha Data 5")]
        [MaxLength(50)]
        public string AlphaData5
        {
            get { return values[(int)Columns.GLAD5].ToString(); }
            set { values[(int)Columns.GLAD5] = value; }
        }

        /// <summary>
        /// GLND1 (7.2 D)
        /// </summary>
        [Display(Name = "Numeric Data 1")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 9999999.99)]
        public decimal NumericData1
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLND1]); }
            set { values[(int)Columns.GLND1] = value.ToString(); }
        }

        /// <summary>
        /// GLND2 (7.2 D)
        /// </summary>
        [Display(Name = "Numeric Data 2")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 9999999.99)]
        public decimal NumericData2
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLND2]); }
            set { values[(int)Columns.GLND2] = value.ToString(); }
        }

        /// <summary>
        /// GLND3 (9.2 D)
        /// </summary>
        [Display(Name = "Numeric Data 3")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 999999999.99)]
        public decimal NumericData3
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLND3]); }
            set { values[(int)Columns.GLND3] = value.ToString(); }
        }

        /// <summary>
        /// GLND4 (9.2 D)
        /// </summary>
        [Display(Name = "Numeric Data 4")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 999999999.99)]
        public decimal NumericData4
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLND4]); }
            set { values[(int)Columns.GLND4] = value.ToString(); }
        }

        /// <summary>
        /// GLND5 (15.6 D)
        /// </summary>
        [Display(Name = "Numeric Data 5")]
        [RegularExpression(@"^\d+\.\d{0,2}$")]
        [Range(0, 999999999999999.999999)]
        public decimal NumericData5
        {
            get { return Convert.ToDecimal(values[(int)Columns.GLND5]); }
            set { values[(int)Columns.GLND5] = value.ToString(); }
        }

        /// <summary>
        /// GLIPAD (30 C)
        /// </summary>
        [Display(Name = "IP Address")]
        [MaxLength(30)]
        public string IPAddress
        {
            get { return values[(int)Columns.GLIPAD].ToString(); }
            set { values[(int)Columns.GLIPAD] = value; }
        }

        /// <summary>
        /// GLSERV (30 C) - Auto Populated
        /// </summary>
        [Display(Name = "Server Name")]
        [MaxLength(30)]
        public string ServerName
        {
            get { return values[(int)Columns.GLSERV].ToString(); }
            set { values[(int)Columns.GLSERV] = value; }
        }

        /// <summary>
        /// GLNOTE (250 C) 
        /// </summary>
        [Display(Name = "Notes")]
        [MaxLength(250)]
        public string Notes
        {
            get { return values[(int)Columns.GLNOTE].ToString(); }
            set { values[(int)Columns.GLNOTE] = value; }
        }

        /// <summary>
        /// GLUSER (10 C) 
        /// </summary>
        [Display(Name = "Log User")]
        [MaxLength(10)]
        public string LogUser
        {
            get { return values[(int)Columns.GLUSER].ToString(); }
            set { values[(int)Columns.GLUSER] = value; }
        }

    }
}
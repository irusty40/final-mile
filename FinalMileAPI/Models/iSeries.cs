﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using IBM.Data.DB2.iSeries;
using System.Configuration;

namespace FinalMileAPI.Models
{
    /// <summary>
    /// iSeries Connections
    /// </summary>
    public static class iSeries
    {
        /// <summary>
        /// Get a table of data via SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public static DataTable GetData(string sql)
        {
            DataTable dt = new DataTable();

            string job = string.Empty;

            try
            {
                using (iDB2Connection conn = new iDB2Connection(ConfigurationManager.ConnectionStrings["idb2Conn"].ToString()))
                {
                    conn.Open();

                    job = conn.JobName;

                    using (iDB2DataAdapter da = new iDB2DataAdapter(sql, conn))
                    {
                        da.Fill(dt);
                    }
                }
            }
            catch (iDB2Exception ix)
            {
                ix.HelpLink = sql;

                ReportError(ix, job);

            }
            catch (Exception ex)
            {
                ex.HelpLink = sql;

                ReportError(ex, job);
            }

            return dt;
        }

        /// <summary>
        /// Insert or Update via SQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="anyEx"></param>
        /// <returns></returns>
        public static int ExecuteNonQuery(string sql, ref Exception anyEx)
        {
            int rows = 0;

            string job = string.Empty;

            try
            {
                using (iDB2Connection conn = new iDB2Connection(ConfigurationManager.ConnectionStrings["idb2Conn"].ToString()))
                {
                    conn.Open();

                    job = conn.JobName;

                    using (iDB2Command cmd = new iDB2Command(sql, conn))
                    {
                        rows = cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (iDB2Exception ix)
            {
                ix.HelpLink = sql;

                anyEx = ix;

                ReportError(ix, job);

            }
            catch (Exception ex)
            {
                ex.HelpLink = sql;

                anyEx = ex;

                ReportError(ex, job);
            }

            return rows;
        }

        /// <summary>
        /// Execute Stored Procedure
        /// </summary>
        /// <param name="storedProcedureName"></param>
        /// <param name="parameters"></param>
        /// <param name="anyEx"></param>
        /// <returns></returns>
        public static bool ExecuteStoredProcedure(string storedProcedureName, ref string[] parameters, ref Exception anyEx)
        {
            bool executedWithoutError = true;

            string commandText = @"CALL QGPL/{0}({1})";

            string job = string.Empty;

            try
            {
                commandText = string.Format(commandText, storedProcedureName, "".PadRight(parameters.Length, '?').Replace("?", "?,").TrimEnd(','));

                using (iDB2Connection conn = new iDB2Connection(ConfigurationManager.ConnectionStrings["idb2Conn"].ToString()))
                {
                    conn.Open();

                    job = conn.JobName;

                    using (iDB2Command cmd = new iDB2Command(commandText, conn))
                    {
                        iDB2CommandBuilder.DeriveParameters(cmd);

                        for (int k = 0; k < cmd.Parameters.Count; k++)
                        {
                            if (parameters[k] == null) parameters[k] = string.Empty;

                            cmd.Parameters[k].Value = parameters[k];
                        }

                        cmd.ExecuteNonQuery();

                        for (int k = 0; k < cmd.Parameters.Count; k++)
                        {
                            parameters[k] = cmd.Parameters[k].Value.ToString();
                        }

                    }
                }
            }
            catch (iDB2Exception ix)
            {
                ix.HelpLink = commandText;

                executedWithoutError = false;

                anyEx = ix;

                ReportError(ix, job, parameters);

            }
            catch (Exception ex)
            {
                ex.HelpLink = commandText;

                executedWithoutError = false;

                anyEx = ex;

                ReportError(ex, job, parameters);

            }

            return executedWithoutError;
        }


       /// <summary>
       /// 
       /// </summary>
       /// <param name="storedProcedureName"></param>
       /// <param name="parameters"></param>
       /// <param name="anyEx"></param>
       /// <returns></returns>
        public static DataTable ExecuteStoredProcedureResultSet(string storedProcedureName, ref string[] parameters, ref Exception anyEx)
        {
            DataTable dt = new DataTable(storedProcedureName);

            string commandText = @"CALL QGPL/{0}({1})";

            string job = string.Empty;

            try
            {
                commandText = string.Format(commandText, storedProcedureName, "".PadRight(parameters.Length, '?').Replace("?", "?,").TrimEnd(','));

                using (iDB2Connection conn = new iDB2Connection(ConfigurationManager.ConnectionStrings["idb2Conn"].ToString()))
                {
                    conn.Open();

                    job = conn.JobName;

                    using (iDB2Command cmd = new iDB2Command(commandText, conn))
                    {
                        iDB2CommandBuilder.DeriveParameters(cmd);

                        for (int k = 0; k < cmd.Parameters.Count; k++)
                        {
                            cmd.Parameters[k].Value = parameters[k];
                        }

                        using (iDB2DataAdapter ida = new iDB2DataAdapter(cmd))
                        {
                            ida.Fill(dt);
                        }

                        for (int k = 0; k < cmd.Parameters.Count; k++)
                        {
                            parameters[k] = cmd.Parameters[k].Value.ToString();
                        }

                    }
                }
            }
            catch (iDB2Exception ix)
            {
                ix.HelpLink = commandText;

                anyEx = ix;

                ReportError(ix, job, parameters);

            }
            catch (Exception ex)
            {
                ex.HelpLink = commandText;

                anyEx = ex;

                ReportError(ex, job, parameters);

            }

            return dt;
        }

        /// <summary>
        /// Reports Excpetions to Programmer
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="job"></param>
        /// <param name="parameters"></param>
        public static void ReportError(Exception ex, string job = "", string[] parameters = null)
        {
            System.Text.StringBuilder msg = new System.Text.StringBuilder();

            msg.Append(@"<span style=""font-family: Helvetica, Consolas, Tahoma; font-size: 11pt; color: navy;"">");

            try
            {
                msg.Append("<span style='font-size: 14pt; font-weight: bold;'>Final Mile API: Exception</span><br /><br />");

                if ((job != null) && (!string.IsNullOrEmpty(job.Trim())))
                {
                    msg.AppendFormat("<b>AS400 Job:</b> {0}<br /><br />", job);
                }

                msg.AppendFormat("<b>HelpLink:</b> {0}<br /><br />", ex.HelpLink);

                msg.AppendFormat("<b>Message:</b> {0}<br /><br />", ex.Message);

                msg.AppendFormat("<b>StackTrace:</b> {0}<br /><br />", ex.StackTrace);

                if (ex.InnerException != null)
                {
                    if (ex.InnerException.HelpLink != null) msg.AppendFormat("<b>InnerException.HelpLink:</b> {0}<br /><br />", ex.InnerException.HelpLink);

                    if (ex.InnerException.Message != null) msg.AppendFormat("<b>InnerException.Message:</b> {0}<br /><br />", ex.InnerException.Message);

                    if (ex.InnerException.StackTrace != null) msg.AppendFormat("<b>InnerException.StackTrace:</b> {0}<br /><br />", ex.InnerException.StackTrace);
                }

                msg.Append("<br />");

                msg.AppendFormat("<b>Server:</b> {0}<br /><br />", Environment.MachineName.ToString());

                msg.AppendFormat("<b>Connection String:</b> {0}<br /><br />", ConfigurationManager.ConnectionStrings["idb2Conn"].ToString());

                msg.AppendFormat("<b>Mail Server:</b> {0}<br /><br />", ConfigurationManager.AppSettings["mailserver"].ToString());

                msg.Append("<br />");

                if ((parameters != null) && (parameters.Length > 0))
                {
                    msg.Append("<b>Parameters:</b><br />");

                    for (int k = 0; k < parameters.Length; k++)
                    {
                        msg.AppendFormat("{0}) '{1}'<br />", k.ToString(), parameters[k].Trim());
                    }

                    msg.Append("<br />");
                }
            }
            catch { }

            msg.Append("</span>");


            try
            {   
                using (System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage())
                {
                    mail.From = new System.Net.Mail.MailAddress("noreply@saia.com");

                    mail.To.Add(new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["erroremail"].ToString()));

                    mail.Subject = "Final Mile API Error Report";

                    mail.IsBodyHtml = true;

                    mail.Body = msg.ToString();

                    //mail.Attachments.Add(new System.Net.Mail.Attachment(filepath));

                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["mailserver"].ToString()))
                    {
                        smtp.Send(mail);
                    }
                }

            }
            catch { }
        }

        private static void ReportError(iDB2Exception ex, string job = "", string[] parameters = null)
        {
            System.Text.StringBuilder msg = new System.Text.StringBuilder();

            msg.Append(@"<span style=""font-family: Helvetica, Consolas, Tahoma; font-size: 11pt; color: navy;"">");

            try
            {
                msg.Append("<span style='font-size: 14pt; font-weight: bold;'>Russell's API: iDB2Exception</span><br /><br />");

                if ((job != null) && (!string.IsNullOrEmpty(job.Trim())))
                {
                    msg.AppendFormat("<b>AS400 Job:</b> {0}<br /><br />", job);
                }

                msg.AppendFormat("<b>HelpLink:</b> {0}<br /><br />", ex.HelpLink);

                msg.AppendFormat("<b>MessageCode:</b> {0}<br /><br />", ex.MessageCode);

                msg.AppendFormat("<b>MessageDetails:</b> {0}<br /><br />", ex.MessageDetails);

                msg.AppendFormat("<b>ErrorCode:</b> {0}<br /><br />", ex.ErrorCode);

                msg.AppendFormat("<b>Message:</b> {0}<br /><br />", ex.Message);

                msg.AppendFormat("<b>StackTrace:</b> {0}<br /><br />", ex.StackTrace);

                if (ex.InnerException != null)
                {
                    if (ex.InnerException.HelpLink != null) msg.AppendFormat("<b>InnerException.HelpLink:</b> {0}<br /><br />", ex.InnerException.HelpLink);

                    if (ex.InnerException.Message != null) msg.AppendFormat("<b>InnerException.Message:</b> {0}<br /><br />", ex.InnerException.Message);

                    if (ex.InnerException.StackTrace != null) msg.AppendFormat("<b>InnerException.StackTrace:</b> {0}<br /><br />", ex.InnerException.StackTrace);
                }

                msg.Append("<br />");

                msg.AppendFormat("<b>Server:</b> {0}<br /><br />", Environment.MachineName.ToString());

                msg.AppendFormat("<b>Connection String:</b> {0}<br /><br />", ConfigurationManager.ConnectionStrings["idb2Conn"].ToString());

                msg.AppendFormat("<b>Mail Server:</b> {0}<br /><br />", ConfigurationManager.AppSettings["mailserver"].ToString());

                msg.Append("<br />");

                if ((parameters != null) && (parameters.Length > 0))
                {
                    msg.Append("<b>Parameters:</b><br />");

                    for (int k = 0; k < parameters.Length; k++)
                    {
                        msg.AppendFormat("{0}) '{1}'<br />", k.ToString(), parameters[k].Trim());
                    }

                    msg.Append("<br />");
                }
            }
            catch { }

            msg.Append("</span>");


            try
            {
                using (System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage())
                {
                    mail.From = new System.Net.Mail.MailAddress("noreply@saia.com");

                    mail.To.Add(new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings["erroremail"].ToString()));

                    mail.Subject = "Russell's APIError Report";

                    mail.IsBodyHtml = true;

                    mail.Body = msg.ToString();

                    //mail.Attachments.Add(new System.Net.Mail.Attachment(filepath));

                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(ConfigurationManager.AppSettings["mailserver"].ToString()))
                    {
                        smtp.Send(mail);
                    }
                }

            }
            catch { }
        }
    }
}
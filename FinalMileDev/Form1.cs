﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalMileDev
{
    public partial class Form1 : Form
    {
        int[] donkeys = { 18, 15, 11, 9 };

        Task[] processTasks = null;

        string[] starts = { "", "", "", "" };

        string[] ends = { "", "", "", "" };


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            label1.Text = string.Empty;

            processTasks = new Task[donkeys.Length];

            MainProcess();
            

        }


        void MainProcess()
        {
            bool assigned = false;

            for (int trans = 0; trans < donkeys.Length; trans++)
            {
                assigned = false;
                                
                while (!assigned)
                {
                    for (int taskIndex = 0; taskIndex < processTasks.Length; taskIndex++)
                    {
                        if (processTasks[taskIndex] == null)
                        {
                            assigned = true;

                            label1.Text += "task assigned to " + taskIndex.ToString() + Environment.NewLine;

                            processTasks[taskIndex] = Task.Factory.StartNew(() => RunProcess(donkeys[taskIndex], taskIndex));

                            // processTasks[taskIndex] = Task.Factory.StartNew(() => RunProcess(taskIndex));

                            break;
                        }
                    }

                    if (!assigned) // wait for a task to complete
                    {
                        processTasks[Task.WaitAny(processTasks)] = null;
                    }
                }
            }

        }


        void RunProcess(int index, int trans)
        {

            //frmChildx f = new frmChildx();

            ////f.MdiParent = this;

            //f.Russell = index;            

            //f.Text = string.Format("index = {0} ", index.ToString());

            //f.Show();

            starts[trans] = DateTime.Now.ToString();

            for (int k = 0; k < index; k++)
            {
                //this.Text = string.Format("value={0}, count={1}", index.ToString(), k.ToString());

                System.Threading.Thread.Sleep(1000);

                ends[trans] = DateTime.Now.ToString();

            }


        }

        private void button1_Click(object sender, EventArgs e)
        {

            label2.Text = string.Empty;

            for (int k = 0; k < starts.Length; k++)
            {
                label2.Text += string.Format("start: {0}, end: {1} ", starts[k], ends[k]) + Environment.NewLine;
            }

        }
    }
}

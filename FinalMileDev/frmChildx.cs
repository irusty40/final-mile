﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FinalMileDev
{
    public partial class frmChildx : Form
    {
        public int Russell { get; set;} 

        public frmChildx()
        {
            InitializeComponent();
        }

        private void frmChildx_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;

            label1.Text = DateTime.Now.ToString();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;

            for (int k = 0; k < Russell; k++)
            {
                this.Text = string.Format("value={0}, count={1}", Russell.ToString(), k.ToString());

                System.Threading.Thread.Sleep(1000);
            }

            this.Close();
        }
    }
}

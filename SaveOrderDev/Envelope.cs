﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaveOrderDev
{
    class Envelope
    {
        public string UserGUID { get; set; }

        public Order SaveOrder { get; set; }

        public Envelope()
        {
            SaveOrder = new Order();
        } 

        public string ToXML()
        {
            StringBuilder xx = new StringBuilder();

            xx.Append(@"<SOAP:Envelope xmlns:SOAP='http://schemas.xmlsoap.org/soap/envelope/'>");

            xx.AppendFormat(Environment.NewLine);

            xx.AppendFormat(@"<SOAP:Body UserGUID = '{0}'>", UserGUID);

            xx.AppendFormat(Environment.NewLine);

            xx.Append(SaveOrder.ToXML());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</SOAP:Body>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</SOAP:Envelope>");

            return xx.ToString();
        }

    }
}

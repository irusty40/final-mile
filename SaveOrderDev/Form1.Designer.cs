﻿namespace SaveOrderDev
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSequence = new System.Windows.Forms.TextBox();
            this.txtReference = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtContainerReference = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtWeight = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPieces = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtJobSequence = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCaller = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCustomerCode = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtService = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNotes = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUserGUID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtSee1 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtRoom1 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtState1 = new System.Windows.Forms.TextBox();
            this.txtLateDateTime1 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtScheduledDateTime1 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtEarlyDateTime1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtPhone1 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtZip1 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtCity1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtName1 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtNote1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtStopType1 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSequence1 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtSee2 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtRoom2 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtState2 = new System.Windows.Forms.TextBox();
            this.txtLateDateTime2 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtScheduledDateTime2 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtEarlyDateTime2 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtPhone2 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtZip2 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtCity2 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtName2 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtNote2 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtStopType2 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtSequence2 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.btnXML = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtXML = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtSOAP = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.txtURL = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtSequence);
            this.groupBox1.Controls.Add(this.txtReference);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txtContainerReference);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtDescription);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txtWeight);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtPieces);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtJobSequence);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtCaller);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtCustomerCode);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtService);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtNotes);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtUserGUID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 443);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Body + Order Info";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(23, 218);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(106, 23);
            this.label12.TabIndex = 28;
            this.label12.Text = "Sequence:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSequence
            // 
            this.txtSequence.Location = new System.Drawing.Point(135, 215);
            this.txtSequence.Name = "txtSequence";
            this.txtSequence.Size = new System.Drawing.Size(160, 20);
            this.txtSequence.TabIndex = 27;
            // 
            // txtReference
            // 
            this.txtReference.Location = new System.Drawing.Point(135, 345);
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(160, 20);
            this.txtReference.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(23, 348);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 23);
            this.label7.TabIndex = 24;
            this.label7.Text = "Reference: ";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtContainerReference
            // 
            this.txtContainerReference.Location = new System.Drawing.Point(135, 319);
            this.txtContainerReference.Name = "txtContainerReference";
            this.txtContainerReference.Size = new System.Drawing.Size(160, 20);
            this.txtContainerReference.TabIndex = 23;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(6, 322);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(123, 23);
            this.label8.TabIndex = 22;
            this.label8.Text = "Container Reference:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(135, 293);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(160, 20);
            this.txtDescription.TabIndex = 21;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(23, 296);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(106, 23);
            this.label9.TabIndex = 20;
            this.label9.Text = "Description: ";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtWeight
            // 
            this.txtWeight.Location = new System.Drawing.Point(135, 267);
            this.txtWeight.Name = "txtWeight";
            this.txtWeight.Size = new System.Drawing.Size(160, 20);
            this.txtWeight.TabIndex = 19;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(23, 270);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(106, 23);
            this.label10.TabIndex = 18;
            this.label10.Text = "Weight:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPieces
            // 
            this.txtPieces.Location = new System.Drawing.Point(135, 241);
            this.txtPieces.Name = "txtPieces";
            this.txtPieces.Size = new System.Drawing.Size(160, 20);
            this.txtPieces.TabIndex = 17;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(23, 244);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(106, 23);
            this.label11.TabIndex = 16;
            this.label11.Text = "Pieces:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtJobSequence
            // 
            this.txtJobSequence.Location = new System.Drawing.Point(135, 169);
            this.txtJobSequence.Name = "txtJobSequence";
            this.txtJobSequence.Size = new System.Drawing.Size(160, 20);
            this.txtJobSequence.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(23, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 23);
            this.label6.TabIndex = 10;
            this.label6.Text = "Job Sequence: ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCaller
            // 
            this.txtCaller.Location = new System.Drawing.Point(135, 123);
            this.txtCaller.Name = "txtCaller";
            this.txtCaller.Size = new System.Drawing.Size(160, 20);
            this.txtCaller.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(23, 126);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 23);
            this.label5.TabIndex = 8;
            this.label5.Text = "Caller: ";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCustomerCode
            // 
            this.txtCustomerCode.Location = new System.Drawing.Point(135, 97);
            this.txtCustomerCode.Name = "txtCustomerCode";
            this.txtCustomerCode.Size = new System.Drawing.Size(160, 20);
            this.txtCustomerCode.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(23, 100);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 23);
            this.label4.TabIndex = 6;
            this.label4.Text = "CustomerCode:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtService
            // 
            this.txtService.Location = new System.Drawing.Point(135, 71);
            this.txtService.Name = "txtService";
            this.txtService.Size = new System.Drawing.Size(160, 20);
            this.txtService.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(23, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Service: ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNotes
            // 
            this.txtNotes.Location = new System.Drawing.Point(135, 45);
            this.txtNotes.Name = "txtNotes";
            this.txtNotes.Size = new System.Drawing.Size(160, 20);
            this.txtNotes.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(23, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Notes: ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtUserGUID
            // 
            this.txtUserGUID.Location = new System.Drawing.Point(135, 19);
            this.txtUserGUID.Name = "txtUserGUID";
            this.txtUserGUID.Size = new System.Drawing.Size(160, 20);
            this.txtUserGUID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(23, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "UserGUID: ";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtSee1);
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.txtRoom1);
            this.groupBox2.Controls.Add(this.label37);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.txtState1);
            this.groupBox2.Controls.Add(this.txtLateDateTime1);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.txtScheduledDateTime1);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtEarlyDateTime1);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.txtPhone1);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtZip1);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtCity1);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.txtAddress1);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtName1);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.txtNote1);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.txtStopType1);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.txtSequence1);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Location = new System.Drawing.Point(325, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(313, 443);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pick-Up Info";
            // 
            // txtSee1
            // 
            this.txtSee1.Location = new System.Drawing.Point(135, 299);
            this.txtSee1.Name = "txtSee1";
            this.txtSee1.Size = new System.Drawing.Size(160, 20);
            this.txtSee1.TabIndex = 32;
            // 
            // label38
            // 
            this.label38.Location = new System.Drawing.Point(23, 302);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(106, 23);
            this.label38.TabIndex = 31;
            this.label38.Text = "See:";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtRoom1
            // 
            this.txtRoom1.Location = new System.Drawing.Point(135, 169);
            this.txtRoom1.Name = "txtRoom1";
            this.txtRoom1.Size = new System.Drawing.Size(160, 20);
            this.txtRoom1.TabIndex = 30;
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(23, 172);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(106, 23);
            this.label37.TabIndex = 29;
            this.label37.Text = "Room:";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(23, 224);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(106, 23);
            this.label13.TabIndex = 28;
            this.label13.Text = "State:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtState1
            // 
            this.txtState1.Location = new System.Drawing.Point(135, 221);
            this.txtState1.Name = "txtState1";
            this.txtState1.Size = new System.Drawing.Size(160, 20);
            this.txtState1.TabIndex = 27;
            // 
            // txtLateDateTime1
            // 
            this.txtLateDateTime1.Location = new System.Drawing.Point(135, 397);
            this.txtLateDateTime1.Name = "txtLateDateTime1";
            this.txtLateDateTime1.Size = new System.Drawing.Size(160, 20);
            this.txtLateDateTime1.TabIndex = 25;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(23, 400);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 23);
            this.label14.TabIndex = 24;
            this.label14.Text = "Late Date Time:";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtScheduledDateTime1
            // 
            this.txtScheduledDateTime1.Location = new System.Drawing.Point(135, 371);
            this.txtScheduledDateTime1.Name = "txtScheduledDateTime1";
            this.txtScheduledDateTime1.Size = new System.Drawing.Size(160, 20);
            this.txtScheduledDateTime1.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(6, 374);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(123, 23);
            this.label15.TabIndex = 22;
            this.label15.Text = "Scheduled Date Time:";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtEarlyDateTime1
            // 
            this.txtEarlyDateTime1.Location = new System.Drawing.Point(135, 345);
            this.txtEarlyDateTime1.Name = "txtEarlyDateTime1";
            this.txtEarlyDateTime1.Size = new System.Drawing.Size(160, 20);
            this.txtEarlyDateTime1.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(23, 348);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(106, 23);
            this.label16.TabIndex = 20;
            this.label16.Text = "Early Date Time:";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPhone1
            // 
            this.txtPhone1.Location = new System.Drawing.Point(135, 273);
            this.txtPhone1.Name = "txtPhone1";
            this.txtPhone1.Size = new System.Drawing.Size(160, 20);
            this.txtPhone1.TabIndex = 19;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(23, 276);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 23);
            this.label17.TabIndex = 18;
            this.label17.Text = "Phone:";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtZip1
            // 
            this.txtZip1.Location = new System.Drawing.Point(135, 247);
            this.txtZip1.Name = "txtZip1";
            this.txtZip1.Size = new System.Drawing.Size(160, 20);
            this.txtZip1.TabIndex = 17;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(23, 250);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(106, 23);
            this.label18.TabIndex = 16;
            this.label18.Text = "Zip:";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCity1
            // 
            this.txtCity1.Location = new System.Drawing.Point(135, 195);
            this.txtCity1.Name = "txtCity1";
            this.txtCity1.Size = new System.Drawing.Size(160, 20);
            this.txtCity1.TabIndex = 11;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(23, 198);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(106, 23);
            this.label19.TabIndex = 10;
            this.label19.Text = "City:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(135, 143);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(160, 20);
            this.txtAddress1.TabIndex = 9;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(23, 146);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(106, 23);
            this.label20.TabIndex = 8;
            this.label20.Text = "Address:";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtName1
            // 
            this.txtName1.Location = new System.Drawing.Point(135, 117);
            this.txtName1.Name = "txtName1";
            this.txtName1.Size = new System.Drawing.Size(160, 20);
            this.txtName1.TabIndex = 7;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(23, 120);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(106, 23);
            this.label21.TabIndex = 6;
            this.label21.Text = "Name:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNote1
            // 
            this.txtNote1.Location = new System.Drawing.Point(135, 71);
            this.txtNote1.Name = "txtNote1";
            this.txtNote1.Size = new System.Drawing.Size(160, 20);
            this.txtNote1.TabIndex = 5;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(23, 74);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(106, 23);
            this.label22.TabIndex = 4;
            this.label22.Text = "Note:";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtStopType1
            // 
            this.txtStopType1.Location = new System.Drawing.Point(135, 45);
            this.txtStopType1.Name = "txtStopType1";
            this.txtStopType1.Size = new System.Drawing.Size(160, 20);
            this.txtStopType1.TabIndex = 3;
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(23, 48);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(106, 23);
            this.label23.TabIndex = 2;
            this.label23.Text = "Stop Type:";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSequence1
            // 
            this.txtSequence1.Location = new System.Drawing.Point(135, 19);
            this.txtSequence1.Name = "txtSequence1";
            this.txtSequence1.Size = new System.Drawing.Size(160, 20);
            this.txtSequence1.TabIndex = 1;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(23, 22);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(106, 23);
            this.label24.TabIndex = 0;
            this.label24.Text = "Sequence:";
            this.label24.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtSee2);
            this.groupBox3.Controls.Add(this.label40);
            this.groupBox3.Controls.Add(this.txtRoom2);
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.txtState2);
            this.groupBox3.Controls.Add(this.txtLateDateTime2);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.txtScheduledDateTime2);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.txtEarlyDateTime2);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.txtPhone2);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Controls.Add(this.txtZip2);
            this.groupBox3.Controls.Add(this.label30);
            this.groupBox3.Controls.Add(this.txtCity2);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.txtAddress2);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.txtName2);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.txtNote2);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.txtStopType2);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.txtSequence2);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Location = new System.Drawing.Point(644, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(313, 443);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Delivery Info";
            // 
            // txtSee2
            // 
            this.txtSee2.Location = new System.Drawing.Point(135, 299);
            this.txtSee2.Name = "txtSee2";
            this.txtSee2.Size = new System.Drawing.Size(160, 20);
            this.txtSee2.TabIndex = 34;
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(23, 302);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(106, 23);
            this.label40.TabIndex = 33;
            this.label40.Text = "See:";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtRoom2
            // 
            this.txtRoom2.Location = new System.Drawing.Point(135, 169);
            this.txtRoom2.Name = "txtRoom2";
            this.txtRoom2.Size = new System.Drawing.Size(160, 20);
            this.txtRoom2.TabIndex = 32;
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(23, 172);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(106, 23);
            this.label39.TabIndex = 31;
            this.label39.Text = "Room:";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(23, 224);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(106, 23);
            this.label25.TabIndex = 28;
            this.label25.Text = "State:";
            this.label25.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtState2
            // 
            this.txtState2.Location = new System.Drawing.Point(135, 221);
            this.txtState2.Name = "txtState2";
            this.txtState2.Size = new System.Drawing.Size(160, 20);
            this.txtState2.TabIndex = 27;
            // 
            // txtLateDateTime2
            // 
            this.txtLateDateTime2.Location = new System.Drawing.Point(135, 397);
            this.txtLateDateTime2.Name = "txtLateDateTime2";
            this.txtLateDateTime2.Size = new System.Drawing.Size(160, 20);
            this.txtLateDateTime2.TabIndex = 25;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(23, 400);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(106, 23);
            this.label26.TabIndex = 24;
            this.label26.Text = "Late Date Time:";
            this.label26.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtScheduledDateTime2
            // 
            this.txtScheduledDateTime2.Location = new System.Drawing.Point(135, 371);
            this.txtScheduledDateTime2.Name = "txtScheduledDateTime2";
            this.txtScheduledDateTime2.Size = new System.Drawing.Size(160, 20);
            this.txtScheduledDateTime2.TabIndex = 23;
            // 
            // label27
            // 
            this.label27.Location = new System.Drawing.Point(6, 374);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(123, 23);
            this.label27.TabIndex = 22;
            this.label27.Text = "Scheduled Date Time:";
            this.label27.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtEarlyDateTime2
            // 
            this.txtEarlyDateTime2.Location = new System.Drawing.Point(135, 345);
            this.txtEarlyDateTime2.Name = "txtEarlyDateTime2";
            this.txtEarlyDateTime2.Size = new System.Drawing.Size(160, 20);
            this.txtEarlyDateTime2.TabIndex = 21;
            // 
            // label28
            // 
            this.label28.Location = new System.Drawing.Point(23, 348);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(106, 23);
            this.label28.TabIndex = 20;
            this.label28.Text = "Early Date Time:";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtPhone2
            // 
            this.txtPhone2.Location = new System.Drawing.Point(135, 273);
            this.txtPhone2.Name = "txtPhone2";
            this.txtPhone2.Size = new System.Drawing.Size(160, 20);
            this.txtPhone2.TabIndex = 19;
            // 
            // label29
            // 
            this.label29.Location = new System.Drawing.Point(23, 276);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(106, 23);
            this.label29.TabIndex = 18;
            this.label29.Text = "Phone:";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtZip2
            // 
            this.txtZip2.Location = new System.Drawing.Point(135, 247);
            this.txtZip2.Name = "txtZip2";
            this.txtZip2.Size = new System.Drawing.Size(160, 20);
            this.txtZip2.TabIndex = 17;
            // 
            // label30
            // 
            this.label30.Location = new System.Drawing.Point(23, 250);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(106, 23);
            this.label30.TabIndex = 16;
            this.label30.Text = "Zip:";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtCity2
            // 
            this.txtCity2.Location = new System.Drawing.Point(135, 195);
            this.txtCity2.Name = "txtCity2";
            this.txtCity2.Size = new System.Drawing.Size(160, 20);
            this.txtCity2.TabIndex = 11;
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(23, 198);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(106, 23);
            this.label31.TabIndex = 10;
            this.label31.Text = "City:";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(135, 143);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(160, 20);
            this.txtAddress2.TabIndex = 9;
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(23, 146);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(106, 23);
            this.label32.TabIndex = 8;
            this.label32.Text = "Address:";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtName2
            // 
            this.txtName2.Location = new System.Drawing.Point(135, 117);
            this.txtName2.Name = "txtName2";
            this.txtName2.Size = new System.Drawing.Size(160, 20);
            this.txtName2.TabIndex = 7;
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(23, 120);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(106, 23);
            this.label33.TabIndex = 6;
            this.label33.Text = "Name:";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtNote2
            // 
            this.txtNote2.Location = new System.Drawing.Point(135, 71);
            this.txtNote2.Name = "txtNote2";
            this.txtNote2.Size = new System.Drawing.Size(160, 20);
            this.txtNote2.TabIndex = 5;
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(23, 74);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(106, 23);
            this.label34.TabIndex = 4;
            this.label34.Text = "Note:";
            this.label34.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtStopType2
            // 
            this.txtStopType2.Location = new System.Drawing.Point(135, 45);
            this.txtStopType2.Name = "txtStopType2";
            this.txtStopType2.Size = new System.Drawing.Size(160, 20);
            this.txtStopType2.TabIndex = 3;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(23, 48);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(106, 23);
            this.label35.TabIndex = 2;
            this.label35.Text = "Stop Type:";
            this.label35.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtSequence2
            // 
            this.txtSequence2.Location = new System.Drawing.Point(135, 19);
            this.txtSequence2.Name = "txtSequence2";
            this.txtSequence2.Size = new System.Drawing.Size(160, 20);
            this.txtSequence2.TabIndex = 1;
            // 
            // label36
            // 
            this.label36.Location = new System.Drawing.Point(23, 22);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(106, 23);
            this.label36.TabIndex = 0;
            this.label36.Text = "Sequence:";
            this.label36.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnXML
            // 
            this.btnXML.Location = new System.Drawing.Point(69, 502);
            this.btnXML.Name = "btnXML";
            this.btnXML.Size = new System.Drawing.Size(75, 23);
            this.btnXML.TabIndex = 3;
            this.btnXML.Text = "XML";
            this.btnXML.UseVisualStyleBackColor = true;
            this.btnXML.Click += new System.EventHandler(this.btnXML_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(983, 494);
            this.tabControl1.TabIndex = 4;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(975, 468);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Input";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtXML);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(975, 468);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "XML Output";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtXML
            // 
            this.txtXML.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtXML.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXML.Location = new System.Drawing.Point(3, 3);
            this.txtXML.Multiline = true;
            this.txtXML.Name = "txtXML";
            this.txtXML.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtXML.Size = new System.Drawing.Size(969, 462);
            this.txtXML.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.txtSOAP);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(975, 468);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "SOAP Response";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // txtSOAP
            // 
            this.txtSOAP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSOAP.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSOAP.Location = new System.Drawing.Point(3, 3);
            this.txtSOAP.Multiline = true;
            this.txtSOAP.Name = "txtSOAP";
            this.txtSOAP.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSOAP.Size = new System.Drawing.Size(969, 462);
            this.txtSOAP.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(150, 502);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Send XML";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtURL
            // 
            this.txtURL.Location = new System.Drawing.Point(231, 504);
            this.txtURL.Name = "txtURL";
            this.txtURL.Size = new System.Drawing.Size(487, 20);
            this.txtURL.TabIndex = 8;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(852, 504);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(119, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "Save Source";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(727, 504);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(119, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Load Source";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(10, 502);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(37, 23);
            this.button4.TabIndex = 11;
            this.button4.Text = "><";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(6, 414);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(37, 23);
            this.button5.TabIndex = 12;
            this.button5.Text = "[xml]";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(983, 540);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.txtURL);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.btnXML);
            this.Name = "Form1";
            this.Text = "Save Order Dev";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtUserGUID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtService;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNotes;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCaller;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCustomerCode;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtJobSequence;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtReference;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtContainerReference;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtWeight;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPieces;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSequence;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtState1;
        private System.Windows.Forms.TextBox txtLateDateTime1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtScheduledDateTime1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtEarlyDateTime1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtPhone1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtZip1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtCity1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtAddress1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtName1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtNote1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtStopType1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtSequence1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtState2;
        private System.Windows.Forms.TextBox txtLateDateTime2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtScheduledDateTime2;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtEarlyDateTime2;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox txtPhone2;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtZip2;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtCity2;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtAddress2;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtName2;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox txtNote2;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtStopType2;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtSequence2;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Button btnXML;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtXML;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtURL;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtSOAP;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtRoom1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TextBox txtSee1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox txtSee2;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtRoom2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Button button5;
    }
}


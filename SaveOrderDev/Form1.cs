﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Newtonsoft.Json;
using System.Xml.Linq;

namespace SaveOrderDev
{
    public partial class Form1 : Form
    {

        Envelope xml = null;
        
        public Form1()
        {
            InitializeComponent();
        }


        private async void Form1_Load(object sender, EventArgs e)
        {

            //INPLOG log = new INPLOG("TESTKEY", "TEST1", "TEST2");

            //log.LogDate = 1180926;

            //log.LogTime = 1300;

            //log.LogUser = "RUSWIL";

            //log.Notes = "This a test log. ";


            //var client = new HttpClient();

            //client.DefaultRequestHeaders.Accept.Clear();

            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            //var content = new StringContent(JsonConvert.SerializeObject(log), Encoding.UTF8, "application/json");

            //HttpResponseMessage response = await client.PostAsync(@"http://russellwilp70.saia.com/FinalMileAPI/api/INPLOG", content);

            //if (response.IsSuccessStatusCode)
            //{
            //    Console.WriteLine("Data posted");

            //    MessageBox.Show("ok!");

            //}
            //else
            //{
            //    Console.WriteLine($"Failed to poste data. Status code:{response.StatusCode}");

            //    MessageBox.Show("not ok!");
            //}














            button4_Click(null, null);
        }

        private void button4_Click(object sender, EventArgs e)
        {

            button2.Enabled = false;

            txtURL.Text = @"https://demo.e-courier.com/demo/software/xml/xml.asp";



            txtUserGUID.Text = "{22FF0A2A-6848-4039-A7F5-65083A4CA181}";

            txtNotes.Text = "Pickup MIA Door #48, Trailer #530907";

            txtService.Text = "DIRECT";

            txtCustomerCode.Text = "4";

            txtCaller.Text = "";

            txtJobSequence.Text = "1";

            txtSequence.Text = "1";

            txtPieces.Text = "4";

            txtWeight.Text = "1000";

            txtDescription.Text = "1 pallet w 4 lg boxes";

            txtContainerReference.Text = "530907";

            txtReference.Text = "046800223306";




            txtSequence1.Text = "1";

            txtStopType1.Text = "P";

            txtNote1.Text = "Lift gate required";

            txtName1.Text = "MIAMI, FL (85)";

            txtAddress1.Text = "11405 NW 36th Avenue";

            txtCity1.Text = "Miami";

            txtState1.Text = "FL";

            txtZip1.Text = "33167";

            txtPhone1.Text = "305-685-1047";

            txtEarlyDateTime1.Text = "";

            txtScheduledDateTime1.Text = DateTime.Now.ToString("MM/dd/yyyy") + " 05:00";

            txtLateDateTime1.Text = "";




            txtSequence2.Text = "2";

            txtStopType2.Text = "D";

            txtNote2.Text = "";

            txtName2.Text = "ADEX USA LLC";

            txtAddress2.Text = "9450 NW 12TH ST";

            txtCity2.Text = "Doral";

            txtState2.Text = "FL";

            txtZip2.Text = "33172";

            txtPhone2.Text = "";

            txtEarlyDateTime2.Text = DateTime.Now.ToString("MM/dd/yyyy") + " 13:00";

            txtScheduledDateTime2.Text = DateTime.Now.ToString("MM/dd/yyyy") + " 13:00";

            txtLateDateTime2.Text = DateTime.Now.ToString("MM/dd/yyyy") + " 17:00";



        }

        private void btnXML_Click(object sender, EventArgs e)
        {
            txtXML.Text = string.Empty;

            CollectData();

            txtXML.Text = xml.ToXML();

            using (System.IO.StreamWriter wtr = new System.IO.StreamWriter(@"D:\temp\xmlOutput1.xml", false))
            {
                wtr.Write(xml.ToXML());
            }

            tabControl1.SelectedTab = tabPage2;

        }

        private void  CollectData()
        {
            xml = new Envelope();

            xml.UserGUID = txtUserGUID.Text;

            xml.SaveOrder.Notes = txtNotes.Text;

            xml.SaveOrder.Service = txtService.Text;

            xml.SaveOrder.CustomerCode = txtCustomerCode.Text;

            xml.SaveOrder.Caller = txtCaller.Text;

            xml.SaveOrder.JobSequence = (int)Convert.ToInt64(txtJobSequence.Text);

            xml.SaveOrder.Pieces.Sequence = (int)Convert.ToInt64(txtSequence.Text);

            xml.SaveOrder.Pieces.Pieces = (int)Convert.ToInt64(txtPieces.Text);

            xml.SaveOrder.Pieces.Weight = (int)Convert.ToInt64(txtWeight.Text);

            xml.SaveOrder.Pieces.Description = txtDescription.Text;

            xml.SaveOrder.Pieces.ContainerReference = txtContainerReference.Text;

            xml.SaveOrder.Pieces.Reference = txtReference.Text;


            xml.SaveOrder.PickUp.Sequence = (int)Convert.ToInt64(txtSequence1.Text);

            xml.SaveOrder.PickUp.StopType = txtStopType1.Text;

            xml.SaveOrder.PickUp.Note = txtNote1.Text;

            xml.SaveOrder.PickUp.Name = txtName1.Text;

            xml.SaveOrder.PickUp.Address = txtAddress1.Text;

            xml.SaveOrder.PickUp.City = txtCity1.Text;

            xml.SaveOrder.PickUp.State = txtState1.Text;

            xml.SaveOrder.PickUp.Zip = txtZip1.Text;

            xml.SaveOrder.PickUp.Phone = txtPhone1.Text;

            xml.SaveOrder.PickUp.EarlyDateTime = txtEarlyDateTime1.Text;

            xml.SaveOrder.PickUp.ScheduledDateTime = txtScheduledDateTime1.Text;

            xml.SaveOrder.PickUp.LateDateTime = txtLateDateTime1.Text;


            xml.SaveOrder.Delivery.Sequence = (int)Convert.ToInt64(txtSequence2.Text);

            xml.SaveOrder.Delivery.StopType = txtStopType2.Text;

            xml.SaveOrder.Delivery.Note = txtNote2.Text;

            xml.SaveOrder.Delivery.Name = txtName2.Text;

            xml.SaveOrder.Delivery.Address = txtAddress2.Text;

            xml.SaveOrder.Delivery.City = txtCity2.Text;

            xml.SaveOrder.Delivery.State = txtState2.Text;

            xml.SaveOrder.Delivery.Zip = txtZip2.Text;

            xml.SaveOrder.Delivery.Phone = txtPhone2.Text;

            xml.SaveOrder.Delivery.EarlyDateTime = txtEarlyDateTime2.Text;

            xml.SaveOrder.Delivery.ScheduledDateTime = txtScheduledDateTime2.Text;

            xml.SaveOrder.Delivery.LateDateTime = txtLateDateTime2.Text;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // post xml to soap

            txtSOAP.Text = string.Empty;

            try
            {
                HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(txtURL.Text);

                webReq.Headers.Add(@"SOAP:Action");

                webReq.ContentType = "text/xml;charset=\"utf-8\"";

                webReq.Accept = "text/xml";

                webReq.Method = "POST";


                XmlDocument soapEnvelopeXml = new XmlDocument();

                soapEnvelopeXml.LoadXml(@"<?xml version=""1.0"" encoding=""utf-8""?>" + xml.ToXML());

                using (Stream stream = webReq.GetRequestStream())
                {
                    soapEnvelopeXml.Save(stream);
                }

                using (WebResponse response = webReq.GetResponse())
                {
                    using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                    {
                        string soapResult = rd.ReadToEnd();

                        //Console.WriteLine(soapResult);

                        txtSOAP.Text = soapResult;

                        File.WriteAllText(@"D:\temp\soapResponse.xml", soapResult);

                    }
                }
            }
            catch (Exception ex)
            {
                txtSOAP.Text = ex.Message + Environment.NewLine + ex.StackTrace;
            }

            tabControl1.SelectedTab = tabPage3;
                        
        }


        string SAVE_PATH = string.Empty;

        /// <summary>
        /// load source
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {

            button2.Enabled = true;

            GroupBox[] gboxes = { groupBox1, groupBox2, groupBox3 };

            foreach (GroupBox gbox in gboxes)
            {
                foreach (Control cx in gbox.Controls)
                {
                    if (cx.GetType() == typeof(TextBox))
                    {
                        cx.Text = string.Empty;
                    }
                }
            }
            
            SAVE_PATH = Path.Combine(Environment.CurrentDirectory, "sourcedata.txt");

            if (File.Exists(SAVE_PATH))
            {
                TextBox[] tboxes = buildTextBoxArray();

                using (StreamReader rdr = new StreamReader(SAVE_PATH))
                {
                    int o = 0;

                    while(!rdr.EndOfStream)
                    {
                        tboxes[o].Text = rdr.ReadLine().Trim();

                        o += 1;
                    }
                }

            }

            

        }

        /// <summary>
        /// save source
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            TextBox[] tboxes = buildTextBoxArray();

            SAVE_PATH = Path.Combine(Environment.CurrentDirectory, "sourcedata.txt");

            using (StreamWriter wtr = new StreamWriter(SAVE_PATH, false))
            {
                for (int o = 0; o < tboxes.Length; o++)
                {
                    wtr.WriteLine(tboxes[o].Text.Trim());
                }
            }
        }


        TextBox[] buildTextBoxArray()
        {
            TextBox[] tboxes = {
                txtUserGUID,
                txtNotes,
                txtService,
                txtCustomerCode,
                txtCaller,
                txtJobSequence,
                txtSequence,
                txtPieces,
                txtWeight,
                txtDescription,
                txtContainerReference,
                txtReference,
                txtSequence1,
                txtStopType1,
                txtNote1,
                txtName1,
                txtAddress1,
                txtRoom1,
                txtCity1,
                txtState1,
                txtZip1,
                txtPhone1,
                txtSee1,
                txtEarlyDateTime1,
                txtScheduledDateTime1,
                txtLateDateTime1,
                txtSequence2,
                txtStopType2,
                txtNote2,
                txtName2,
                txtAddress2,
                txtRoom2,
                txtCity2,
                txtState2,
                txtZip2,
                txtPhone2,
                txtSee2,
                txtEarlyDateTime2,
                txtScheduledDateTime2,
                txtLateDateTime2
                };

            return tboxes;
        }

        

        private void button5_Click(object sender, EventArgs e)
        {


            XDocument docx = XDocument.Load(@"D:\temp\soapResponse.xml");

            string name = docx.Descendants("Order").First().Value;

            IEnumerable<XElement> responses = docx.Descendants("Order");

            foreach (XElement response in responses)
            {
                var orderNumber = XElement.Parse(response.ToString()).Attribute("OrderNumber").Value;
            }
            

        }
    }
}

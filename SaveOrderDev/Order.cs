﻿using System;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SaveOrderDev
{
    class Order
    {
        //Weight double
        //DeclaredValue double
        //PodRequired int (0 or 1)
        //Auth string(50)
        //OrderAlias string(50)
        //OtherReference string(50)
        //InvoiceReference string(50)
        //CallerPhone string(50)
        //Description string(50)


        /// <summary>
        /// Notes
        /// </summary>
        /// <remarks>
        /// A full text order level note used for special shipping instructions, often used to list the items on the shipment
        /// </remarks>
        [Display(Name = "Notes")]
        [DefaultValue("")] // string(?)
        public string Notes { get; set; }

        /// <summary>
        /// Service
        /// </summary>
        /// <remarks>
        /// A short code that represents the level of service for the shipment
        /// </remarks>
        [Display(Name = "Service")]
        [DefaultValue("")] // string(10)
        [StringLength(10, ErrorMessage = "Service is 10 characters.")]
        public string Service { get; set; }

        /// <summary>
        /// Customer Code
        /// </summary>
        [Display(Name = "Customer Code")]
        [DefaultValue("")] // string(?)
        public string CustomerCode { get; set; }

        // string(50)
        /// <summary>
        /// Caller
        /// </summary>
        /// <remarks>
        /// The contact name/entity that is responsible for the shipment
        /// </remarks>
        [Display(Name = "Caller")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Caller is 50 characters.")]
        public string Caller { get; set; }

        public OrderStops PickUp { get; set; }

        public OrderStops Delivery { get; set; }

        /// <summary>
        /// Job Sequence
        /// </summary>
        [Display(Name = "Job Sequence")]
        [DefaultValue(0)] // int
        public int JobSequence { get; set; }

        /// <summary>
        /// Pieces
        /// </summary>
        [Display(Name = "Pieces")]
        [DefaultValue(0)] // int
        public OrderPieces Pieces { get; set; }

        public Order()
        {
            PickUp = new OrderStops();

            Delivery = new OrderStops();

            Pieces = new OrderPieces();
        }

        /// <summary>
        /// class data to formatted XML string
        /// </summary>
        /// <returns></returns>
        public string ToXML()
        {
            StringBuilder xx = new StringBuilder();

            xx.Append(@"<m:SaveOrder xmlns:m = 'http://www.e-courier.com/schemas/'>");

            xx.AppendFormat(Environment.NewLine);

            xx.AppendFormat(@"<Order Notes = '{0}' Service = '{1}' CustomerCode = '{2}' Caller = '{3}' >", Notes, Service, CustomerCode, Caller);

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"<Stops>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(PickUp.ToXML());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(Delivery.ToXML());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Stops>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"<Jobs>");

            xx.AppendFormat(Environment.NewLine);

            xx.AppendFormat(@"<Job Sequence = '{0}' />", JobSequence.ToString());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Jobs>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"<Pieces>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(Pieces.ToXML());

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Pieces>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Order>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</m:SaveOrder>");

            return xx.ToString();
        }


    }
}

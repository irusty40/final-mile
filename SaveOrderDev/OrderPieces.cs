﻿using System;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SaveOrderDev
{
    class OrderPieces
    {
        /// <summary>
        /// Sequence
        /// </summary>
        [Display(Name = "Sequence")]
        [DefaultValue(0)] // int
        public int Sequence { get; set; }

        /// <summary>
        /// Pieces
        /// </summary>
        [Display(Name = "Pieces")]
        [DefaultValue(0)] // int
        public int Pieces { get; set; }

        /// <summary>
        /// Weight
        /// </summary>
        [Display(Name = "Weight")]
        [DefaultValue(0)] // double
        public int Weight { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [Display(Name = "Name")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Name is 50 characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Container Reference
        /// </summary>
        [Display(Name = "Container Reference")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Container Reference is 50 characters.")]
        public string ContainerReference { get; set; }

        /// <summary>
        /// Reference
        /// </summary>
        [Display(Name = "Reference")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Reference is 50 characters.")]
        public string Reference { get; set; }


        public OrderPieces() { }


        /// <summary>
        /// class data to formatted XML string
        /// </summary>
        /// <returns></returns>
        public string ToXML()
        {
            StringBuilder xx = new StringBuilder();

            xx.AppendFormat(@"<Piece Sequence = '{0}' Pieces = '{1}' Weight = '{2}'  Description = '{3}'  ContainerReference = '{4}'  Reference = '{5}' />",
                            Sequence.ToString(), 
                            Pieces.ToString(), 
                            Weight.ToString(), 
                            Description, 
                            ContainerReference, 
                            Reference);

            return xx.ToString();
        }

    }
}

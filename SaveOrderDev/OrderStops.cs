﻿using System;
using System.Text;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace SaveOrderDev
{
    class OrderStops
    {
        
        /// <summary>
        /// Sequence
        /// </summary>
        [Display(Name = "Sequence")]
        [DefaultValue(0)] // int
        public int Sequence { get; set; }

        /// <summary>
        /// Stop Type
        /// </summary>
        [Display(Name = "Stop Type")]
        [DefaultValue("")] // string(1)
        [StringLength(1, ErrorMessage = "Stop Type is 1 character.")]
        public string StopType { get; set; }

        /// <summary>
        /// Notes
        /// </summary>
        [Display(Name = "Notes")]
        [DefaultValue("")] // string(?)
        public string Note { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [Display(Name = "Name")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Name is 50 characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Address
        /// </summary>
        [Display(Name = "Address")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Address is 50 characters.")]
        public string Address { get; set; }

        // Room string(50) aka:Suite?


        /// <summary>
        /// City
        /// </summary>
        [Display(Name = "City")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "City is 50 characters.")]
        public string City { get; set; }

        /// <summary>
        /// State
        /// </summary>
        [Display(Name = "State")]
        [DefaultValue("")] // string(2)
        [StringLength(2, ErrorMessage = "State is 2 characters.")]
        public string State { get; set; }

        /// <summary>
        /// Zip
        /// </summary>
        /// <remarks>
        /// format: 99999
        /// or
        /// format: 99999-9999
        /// </remarks>
        [Display(Name = "Zip")]
        [DefaultValue("")] // string(20)
        [StringLength(20, ErrorMessage = "Zip is 20 characters.")]
        public string Zip { get; set; }

        // See string(50) aka: contact name

        /// <summary>
        /// Phone
        /// </summary>
        /// <remarks>
        /// standard format: 999-999-9999
        /// </remarks>
        [Display(Name = "Phone")]
        [DefaultValue("")] // string(50)
        [StringLength(50, ErrorMessage = "Phone is 50 characters.")]
        public string Phone { get; set; }

        /// <summary>
        /// Early Date Time
        /// </summary>
        /// <remarks>
        /// standard format: mm/dd/yyyy HH:mm
        /// </remarks>
        [Display(Name = "Early Date Time")]
        [DefaultValue("")] // date
        public string EarlyDateTime { get; set; }

        /// <summary>
        /// Scheduled Date Time
        /// </summary>
        /// <remarks>
        /// standard format: mm/dd/yyyy HH:mm
        /// </remarks>
        [Display(Name = "Scheduled Date Time")]
        [DefaultValue("")] // date
        public string ScheduledDateTime { get; set; }

        /// <summary>
        /// Late Date Time
        /// </summary>
        /// <remarks>
        /// standard format: mm/dd/yyyy HH:mm
        /// </remarks>
        [Display(Name = "Late Date Time")]
        [DefaultValue("")] // date
        public string LateDateTime { get; set; }

        /// <summary>
        /// Stop Piece Sequence
        /// </summary>
        [Display(Name = "Stop Piece Sequence")]
        [DefaultValue(0)] // int
        public int StopPieceSequence { get; set; }

        /// <summary>
        /// Stop Piece Action
        /// </summary>
        [Display(Name = "Stop Piece Action")]
        [DefaultValue("")] // string(1)
        [StringLength(1, ErrorMessage = "Stop Piece Action is 1 character.")]
        public string StopPieceAction { get; set; }

        public OrderStops()
        {
            StopPieceSequence = 1;
        }
        

        /// <summary>
        /// class data to formatted XML string
        /// </summary>
        /// <returns></returns>
        public string ToXML()
        {

            StopPieceAction = StopType;

            StringBuilder xx = new StringBuilder();

            xx.AppendFormat(@"<Stop Sequence = '{0}' StopType = '{1}' Note = '{2}' Name = '{3}' Address = '{4}' City = '{5}' State = '{6}' Zip = '{7}' Phone = '{8}' EarlyDateTime = '{9}' ScheduledDateTime = '{10}' LateDateTime = '{11}' >",
                            Sequence.ToString(),
                            StopType,
                            Note,
                            Name,
                            Address,
                            City,
                            State,
                            Zip,
                            Phone,
                            EarlyDateTime,
                            ScheduledDateTime,
                            LateDateTime);

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"<OrderStopPieces>");

            xx.AppendFormat(Environment.NewLine);

            xx.AppendFormat(@"<OrderStopPiece Sequence = '{0}' PieceAction = '{1}' />", 
                            StopPieceSequence, 
                            StopPieceAction);

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</OrderStopPieces>");

            xx.AppendFormat(Environment.NewLine);

            xx.Append(@"</Stop>");

            return xx.ToString();
        }
    }
}
